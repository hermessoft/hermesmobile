package com.hermessoft.mobiserv;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.hermessoft.mobiserv.MobiservApplication;
import com.hermessoft.mobiserv.module.rpc.domain.RpcResponse;
import com.hermessoft.mobiserv.module.rpc.domain.RpcResponseFailure;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class RpcIntegrationTests {

    private static final String DEV_TEST_ID = "DEV-TEST";

    @Autowired
    protected Environment env;

    @Value("http://localhost:${local.server.port}/")
    private String basePath;

    @Value("http://localhost:${local.server.port}/rpc")
    private String rpcPath;

    @Autowired
    private ObjectMapper objectMapper;

    private JacksonTester<RpcResponse> rpcResponseTester;
    private JacksonTester<RpcResponseFailure> rpcResponseFailureTester;

    private TestRestTemplate restTemplate = new TestRestTemplate(DEV_TEST_ID, DEV_TEST_ID);

    @Before
    public void setup() {
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void badCredentialTest() {
        TestRestTemplate restTmpl = new TestRestTemplate("user", "password");
        ResponseEntity<String> resp = restTmpl.getForEntity(rpcPath, String.class);
        assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void goodCredentialTest() {
        ResponseEntity<String> resp = restTemplate.getForEntity(rpcPath, String.class);
        assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(resp.getBody()).isEqualTo("Hello");
    }

    @Test
    public void queryWithParamsTest() throws IOException {

        String request = env.getProperty("test.rpc-query-with-params.request");
        ResponseEntity<String> resp = restTemplate.exchange(rpcPath, HttpMethod.POST,
                buildRequestEntity(request), String.class);

        assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);

        String content = resp.getBody();

        System.out.println("Actual content: " + content);

        assertThat(rpcResponseTester.write(rpcResponseTester.parseObject(content)))
                .isEqualToJson(env.getProperty("test.rpc-query-with-params.response"));

    }

    @Test
    public void methodNotFoundErrorTest() throws IOException {

        String request = env.getProperty("test.rpc-method-not-found-error.request");
        ResponseEntity<String> resp = restTemplate.exchange(rpcPath, HttpMethod.POST,
                buildRequestEntity(request), String.class);

        assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);

        String content = resp.getBody();

        System.out.println("Actual content: " + content);

        assertThat(rpcResponseFailureTester.write(rpcResponseFailureTester.parseObject(content)))
                .isEqualToJson(env.getProperty("test.rpc-method-not-found-error.response"));

    }

    @Test
    public void emptyOrNullMethodErrorTest() throws IOException {

        String request = env.getProperty("test.rpc-empty-or-null-method-error.request");
        ResponseEntity<String> resp = restTemplate.exchange(rpcPath, HttpMethod.POST,
                buildRequestEntity(request), String.class);

        assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);

        String content = resp.getBody();

        System.out.println("Actual content: " + content);

        assertThat(rpcResponseFailureTester.write(rpcResponseFailureTester.parseObject(content)))
                .isEqualToJson(env.getProperty("test.rpc-empty-or-null-method-error.response"));


    }

    private HttpEntity<String> buildRequestEntity(String jsonText) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<>(jsonText, headers);
    }

}

package com.hermessoft.mobiserv.module.rpc.apk;

import com.hermessoft.mobiserv.module.rpc.apk.impl.ApkLoaderImpl;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import static com.hermessoft.mobiserv.module.rpc.RpcTestUtils.paramsBuilder;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class) // SpringRunner.class)
public class ApkLoaderTests {

    private static String apkStorageLocation;

    private static ApkLoader apkLoader;

    @BeforeClass
    public static void setup() {
        apkStorageLocation = "./target/test-classes/apk-test-storage";
        apkLoader = new ApkLoaderImpl("./target/test-classes/apk-test-storage");
    }

    @Test
    public void loadTest() throws IOException {

        long expectedDataSize = Files.size(Paths.get(apkStorageLocation, "test-ver/test.apk"));

        List<Map<String, Object>> result = apkLoader.exec("Load",
                paramsBuilder().addRow().addParam("app_ver", "test-ver").build());

        int resultDataSize = ((byte[])result.get(0).get("apk")).length;

        System.out.println(String.format("Expected size: %s", expectedDataSize));
        System.out.println(String.format("Actual size: %s", resultDataSize));
        assertEquals("Wrong data size", expectedDataSize, resultDataSize);

    }

}
package com.hermessoft.mobiserv.module.rpc.dao;

import com.hermessoft.mobiserv.module.rpc.RpcConfiguration;
import com.hermessoft.mobiserv.module.rpc.RpcTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.SchemaOutputResolver;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@JdbcTest
@ActiveProfiles("test")
public class RpcDaoTests {


    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    RpcDao dao;

    @Before
    public void setup() {
   /*     System.out.println("************************************************************************");
        for (String name: applicationContext.getBeanDefinitionNames()) {
            System.out.println(name);
        }*/
    }

    @Test
    @Transactional
    public void queryTest() {

        List<Map<String, Object>> result = dao.exec("select-all", null);

        System.out.println(result);
        assertEquals(result.size(), 2);

    }

    @Test
    @Transactional
    public void queryWithParamsTest() {

        List<Map<String, Object>> result = dao.exec("select-row", RpcTestUtils.paramsBuilder()
                .addRow().addParam("id", 2).build());

        System.out.println(result);
        assertEquals(result.get(0).get("name"), "name2");


    }

    @Test
    @Transactional
    public void execWithParamsTest() {

        dao.exec("update", RpcTestUtils.paramsBuilder()
                .addRow().addParam("id", 2).addParam("name", "Hello").build());


        List<Map<String, Object>> result = dao.exec("select-row", RpcTestUtils.paramsBuilder()
                .addRow().addParam("id", 2).build());

        assertEquals(result.get(0).get("name"), "Hello");

    }

    @Test
    @Transactional
    public void execTest() {
        dao.exec("delete-all", null);

        List<Map<String, Object>> result = dao.exec("select-all", null);
        assertEquals(result.size(), 0);
    }

    @Test
    @Transactional
    public void batchExecTest() {
        List<Map<String, Object>> result;

        dao.exec("delete-all", null);

        dao.exec("insert",
                RpcTestUtils.paramsBuilder()
                        .addRow()
                        .addParam("id", "3")
                        .addParam("name", "name1")
                        .addNextRow()
                        .addParam("id", "4")
                        .addParam("name", "name2")
                        .build());

        result = dao.exec("select-all", new ArrayList<Map<String, Object>>());

        System.out.println(result);
        assertEquals(result.size(), 2);

    }



}
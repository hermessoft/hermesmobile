package com.hermessoft.mobiserv.module.rpc;

import java.util.*;

public class RpcTestUtils {

    public static ParamsBuilder paramsBuilder() {
        return new ParamsBuilder();
    }

    public static class ParamsBuilder {

        private List<Map<String, Object>> result;

        ParamsBuilder() {
            result = new ArrayList<>();
        }

        public List<Map<String, Object>> buildEmpty() {
            return new ArrayList<>();
        }

        public Row addRow() {
            return new Row();
        }

        public class Row {

            private final int idx;

            private Row() {
                result.add(new HashMap<>());
                this.idx = result.size() - 1;
            }

            public Row addParam(String key, Object value) {
                result.get(idx).put(key, value);
                return this;
            }

            public Row addNextRow() {
                return ParamsBuilder.this.addRow();
            }

            public List<Map<String, Object>> build() {
                return result;
            }
        }
    }


}

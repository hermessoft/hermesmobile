package com.hermessoft.mobiserv;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@RunWith(SpringRunner.class)
@SpringBootTest //(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SandBoxTests {

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private Bean1 bean1;

    @Before
    public void setup() {

    }

    @Test
    public void test1() throws IOException {

        Resource resource = resourceLoader.getResource("url:http://www.example.com/");
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(resource.getInputStream()))) {
            reader.lines().forEach(stringBuilder::append);
        }
        System.out.println(stringBuilder.toString());

    }


    @Test
    public void test2() {
        System.out.println("************************:");

    }


    @Configuration
    public static class Conf {

        @Bean
        public Bean1 getBean() {
            return new Bean1();
        }

    }

    public static class Bean1 {

        @Value("world")
        public String hello(String msg) {
            return msg;
        }
    }
}

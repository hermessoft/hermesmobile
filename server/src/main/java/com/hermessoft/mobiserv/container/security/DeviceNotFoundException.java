package com.hermessoft.mobiserv.container.security;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class DeviceNotFoundException extends UsernameNotFoundException {

    public DeviceNotFoundException(String devID) {
        super("Device " + devID + " not found");
    }

}

package com.hermessoft.mobiserv.container.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Map;

@EnableWebSecurity()
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);

    @Value("${app.auth.query}")
    private String USER_BY_USERNAME_QUERY; // = "select id from mfw_dev where id = ? and status = 1";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataSource dataSource;

    @Bean
    public UserDetailsService userDetailsService() {
        return devID -> {
            try {
                logger.debug("Find credentials for {}", devID);

                Map<String, Object> devInfo = jdbcTemplate.queryForMap(USER_BY_USERNAME_QUERY, devID);

                return new User((String) devInfo.get("ID"), "{noop}" + devInfo.get("ID"), true, true, true, true,
                        AuthorityUtils.NO_AUTHORITIES);

            } catch (EmptyResultDataAccessException e) {
                logger.warn("Query returned no results for device ID: {}", devID);
                throw new DeviceNotFoundException(devID);
            } catch (Exception e) {
                logger.error(e.toString());
                throw e;
            }
        };
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .headers().frameOptions().sameOrigin().and() //fix h2-console bug!
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/securityNone").permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }

    @Component
    public class LoginAttemptsLogger {

        @EventListener
        public void onAuthenticationFailureEvent(AbstractAuthenticationFailureEvent event) {
            logger.warn("AUTH FAIL: " + event);
        }

    }

}

package com.hermessoft.mobiserv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobiservApplication {

    public static void main(String[] args) {
        SpringApplication.run(MobiservApplication.class, args);
    }
}

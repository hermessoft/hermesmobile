package com.hermessoft.mobiserv.module.rpc.service.impl;

import com.hermessoft.mobiserv.module.rpc.apk.ApkLoader;
import com.hermessoft.mobiserv.module.rpc.dao.RpcDao;
import com.hermessoft.mobiserv.module.rpc.domain.RpcRequest;
import com.hermessoft.mobiserv.module.rpc.domain.RpcResponse;
import com.hermessoft.mobiserv.module.rpc.exception.RpcMethodNotFoundException;
import com.hermessoft.mobiserv.module.rpc.service.RpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class RpcServiceImpl implements RpcService {
    private static final Logger logger = LoggerFactory.getLogger(RpcService.class);

    private static final String DAO_METHOD_PREF = "MFW";
    private static final String APK_METHOD_PREF = "APK";

    RpcDao dao;

    ApkLoader apkLoader;

    public RpcServiceImpl(RpcDao dao, ApkLoader apkLoader) {
        this.dao = dao;
        this.apkLoader = apkLoader;
    }

    @Override
    public RpcResponse exec(RpcRequest req) {

        String[] methodArr = req.getMethod().split("\\.", -1);
        String methodPref = methodArr[0];
        String methodName = methodArr.length > 1 ? methodArr[1] : methodPref;

        switch (methodPref) {
            case DAO_METHOD_PREF:
                return new RpcResponse(dao.exec(methodName, req.getParams()));
            case APK_METHOD_PREF:
                return new RpcResponse(apkLoader.exec(methodName, req.getParams()));
            default:
                throw new RpcMethodNotFoundException();
        }


    }
}


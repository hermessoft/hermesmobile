package com.hermessoft.mobiserv.module.rpc.dao;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

public interface RpcDao {
    List<Map<String, Object>> exec(@NotEmpty String method, List<Map<String, Object>> params);
}

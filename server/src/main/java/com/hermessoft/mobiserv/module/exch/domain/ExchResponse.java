package com.hermessoft.mobiserv.module.exch.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExchResponse {
    final private List<Map<String, Object>> result;

    public ExchResponse(List<Map<String, Object>> result) {
        this.result = result;
    }

    public ExchResponse() {
        this.result = new ArrayList<>();
    }

    public List<Map<String, Object>> getResult() {
        return result;
    }
}

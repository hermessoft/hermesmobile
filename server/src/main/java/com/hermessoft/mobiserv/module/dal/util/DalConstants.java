package com.hermessoft.mobiserv.module.dal.util;

public class DalConstants {
    public static final String ERROR_METHOD_NOT_FOUND = "ERROR_DAL_METHOD_NOT_FOUND";
    public static final String PROP_METAQUERY = "${app.dal.metaquery}";
}

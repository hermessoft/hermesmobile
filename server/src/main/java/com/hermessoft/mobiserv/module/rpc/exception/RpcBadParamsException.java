package com.hermessoft.mobiserv.module.rpc.exception;

import com.hermessoft.mobiserv.module.rpc.util.RpcConstants;

public class RpcBadParamsException extends RpcException {
    public RpcBadParamsException() {
        super(RpcConstants.ERROR_RPC_BAD_PARAMS);
    }
}

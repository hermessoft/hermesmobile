package com.hermessoft.mobiserv.module.rpc.dao.impl;

import com.hermessoft.mobiserv.module.rpc.dao.RpcDao;
import com.hermessoft.mobiserv.module.rpc.exception.RpcMethodNotFoundException;
import com.hermessoft.mobiserv.module.rpc.util.RpcConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class RpcDaoImpl implements RpcDao {
    private static final Logger logger = LoggerFactory.getLogger(RpcDaoImpl.class);

    private final String metaquery;

    private NamedParameterJdbcTemplate jdbcTemplate;

    public RpcDaoImpl(@Value(RpcConstants.PROP_DAO_METAQUERY) String metaquery,
                      NamedParameterJdbcTemplate jdbcTemplate) {
        this.metaquery = metaquery;
        this.jdbcTemplate = jdbcTemplate;
    }

    private String getMethodSqlText(String method) {
        try {
            Map<String, Object> result = jdbcTemplate.getJdbcTemplate().queryForMap(metaquery, method);
            return (String) result.get("SQL");
        } catch (EmptyResultDataAccessException e) {
            logger.error("Query returned no results for method: {}", method);
            throw new RpcMethodNotFoundException();
        }
    }

    private void executeSql(String sqlText, List<Map<String, Object>> params) {
        if (params == null || params.isEmpty()) {
            jdbcTemplate.execute(sqlText, preparedStatement -> {
                preparedStatement.execute();
                return null;
            });
        } else {
            params.stream().forEachOrdered(
                    p -> jdbcTemplate.execute(sqlText, p, preparedStatement -> {
                        preparedStatement.execute();
                        return null;
                    }));
        }
    }

    private List<Map<String, Object>> executeQuerySql(String sqlText, List<Map<String, Object>> params) {
        Map<String, Object> queryParams = (params == null || params.isEmpty()) ? new HashMap<>(): params.get(0);
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sqlText, queryParams);
        //convert null value -> empty string
        result.stream().forEach(
                r -> {
                    r.entrySet().stream().forEach(
                            e -> {
                                if (e.getValue() == null) e.setValue("");
                            });
                });
        return result;
    }

    @Override
    public List<Map<String, Object>> exec(String method, List<Map<String, Object>> params) {

        String sqlText = getMethodSqlText(method);

        if (sqlText.toLowerCase().startsWith("select")) {
            return executeQuerySql(sqlText, params);
        } else {
            executeSql(sqlText, params);
            return new ArrayList<>();
        }

    }
}

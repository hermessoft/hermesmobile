package com.hermessoft.mobiserv.module.rpc.exception;

import com.hermessoft.mobiserv.module.common.exception.BadRequestException;
import com.hermessoft.mobiserv.module.rpc.util.RpcConstants;

public class RpcMethodEmptyOrNullException extends BadRequestException {

    public RpcMethodEmptyOrNullException() {
        super(RpcConstants.ERROR_RPC_METHOD_VALUE_EMPTY_OR_NULL);
    }
}

package com.hermessoft.mobiserv.module.rpc.exception;

public class RpcException extends RuntimeException {
    public RpcException(String message) {
        super(message);
    }

    public RpcException(String message, Throwable e) {
        super(message, e);
    }
}

package com.hermessoft.mobiserv.module.dal.dao;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;

public interface Dao {
    List<Map<String, Object>> exec(@NotEmpty String method, List<Map<String, Object>> params);
}

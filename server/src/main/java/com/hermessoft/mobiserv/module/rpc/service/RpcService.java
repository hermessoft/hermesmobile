package com.hermessoft.mobiserv.module.rpc.service;

import com.hermessoft.mobiserv.module.rpc.domain.RpcRequest;
import com.hermessoft.mobiserv.module.rpc.domain.RpcResponse;

public interface RpcService {
    RpcResponse exec(RpcRequest req);
}

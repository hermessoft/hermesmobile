package com.hermessoft.mobiserv.module.dal.dao.impl;

import com.hermessoft.mobiserv.module.dal.dao.Dao;
import com.hermessoft.mobiserv.module.dal.exception.DalMethodNotFoundException;
import com.hermessoft.mobiserv.module.dal.util.DalConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DaoImpl implements Dao {
    private static final Logger logger = LoggerFactory.getLogger(DaoImpl.class);

    private final String metaquery;

    private NamedParameterJdbcTemplate jdbcTemplate;

    public DaoImpl(@Value(DalConstants.PROP_METAQUERY) String metaquery,
                      NamedParameterJdbcTemplate jdbcTemplate) {
        this.metaquery = metaquery;
        this.jdbcTemplate = jdbcTemplate;
    }

    private String getMethodSqlText(String method) {
        try {
            Map<String, Object> result = jdbcTemplate.getJdbcTemplate().queryForMap(metaquery, method);
            return (String) result.get("SQL");
        } catch (EmptyResultDataAccessException e) {
            logger.error("Query returned no results for method: {}", method);
            throw new DalMethodNotFoundException();
        }
    }

    private void executeSql(String sqlText, List<Map<String, Object>> params) {
        if (params == null || params.isEmpty()) {
            jdbcTemplate.getJdbcTemplate().execute(sqlText);
        } else {
            jdbcTemplate.batchUpdate(sqlText, SqlParameterSourceUtils.createBatch(params));
        }
    }

    private List<Map<String, Object>> executeQuerySql(String sqlText, List<Map<String, Object>> params) {
        Map<String, Object> queryParams = (params == null || params.isEmpty()) ? new HashMap<>(): params.get(0);
        return jdbcTemplate.queryForList(sqlText, queryParams);
    }

    @Override
    public List<Map<String, Object>> exec(String method, List<Map<String, Object>> params) {

        String sqlText = getMethodSqlText(method);

        if (sqlText.toLowerCase().startsWith("select")) {
            return executeQuerySql(sqlText, params);
        } else {
            executeSql(sqlText, params);
            return new ArrayList<>();
        }

    }
}

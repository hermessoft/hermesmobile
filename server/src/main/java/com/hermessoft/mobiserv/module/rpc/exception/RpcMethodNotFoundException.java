package com.hermessoft.mobiserv.module.rpc.exception;

import com.hermessoft.mobiserv.module.rpc.util.RpcConstants;

public class RpcMethodNotFoundException extends RpcException {
    public RpcMethodNotFoundException() {
        super(RpcConstants.ERROR_RPC_METHOD_NOT_FOUND);
    }
}

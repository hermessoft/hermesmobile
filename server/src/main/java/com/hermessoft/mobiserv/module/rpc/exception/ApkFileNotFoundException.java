package com.hermessoft.mobiserv.module.rpc.exception;

import com.hermessoft.mobiserv.module.rpc.util.RpcConstants;

public class ApkFileNotFoundException extends RpcException {
    public ApkFileNotFoundException() {
        super(RpcConstants.ERROR_RPC_APK_FILE_NOT_FOUND);
    }
}

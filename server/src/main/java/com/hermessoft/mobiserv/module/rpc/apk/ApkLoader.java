package com.hermessoft.mobiserv.module.rpc.apk;

import java.util.List;
import java.util.Map;

public interface ApkLoader {
    List<Map<String, Object>> exec(String method, List<Map<String, Object>> params);
}

package com.hermessoft.mobiserv.module.exch.domain;

import java.util.HashMap;
import java.util.Map;

public class ExchResponseFailure {
    private Map<String, Object> error = new HashMap<>();

    public ExchResponseFailure(int code, String message) {
        error.put("code", code);
        error.put("message", message);
    }

    public ExchResponseFailure() {

    }

    public Map<String, Object> getError() {
        return error;
    }

}

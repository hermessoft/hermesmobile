package com.hermessoft.mobiserv.module.exch.domain;

import com.hermessoft.mobiserv.module.rpc.util.RpcConstants;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExchRequest {
    @NotBlank(message = RpcConstants.ERROR_RPC_METHOD_VALUE_EMPTY_OR_NULL)
    private String method;

    private List<Map<String, Object>> params = new ArrayList<>();

    public String getMethod() {
        return method;
    }

    public List<Map<String, Object>> getParams() {
        return params;
    }

}

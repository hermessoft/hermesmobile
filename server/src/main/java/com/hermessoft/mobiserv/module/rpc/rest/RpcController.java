package com.hermessoft.mobiserv.module.rpc.rest;

import com.hermessoft.mobiserv.module.rpc.domain.RpcRequest;
import com.hermessoft.mobiserv.module.rpc.domain.RpcResponse;
import com.hermessoft.mobiserv.module.rpc.domain.RpcResponseFailure;
import com.hermessoft.mobiserv.module.rpc.exception.RpcException;
import com.hermessoft.mobiserv.module.rpc.service.RpcService;
import com.hermessoft.mobiserv.module.rpc.util.RpcConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/rpc")
public class RpcController {
    private static final Logger logger = LoggerFactory.getLogger(RpcController.class);

    private RpcService rpcService;

    public RpcController(RpcService rpcService) {
        this.rpcService = rpcService;
    }

    private RpcResponseFailure handleException(String message, Throwable e) {
        logger.error("", e);
        return new RpcResponseFailure(1, message);
    }

    @PostMapping()
    public RpcResponse exec(@RequestBody @Validated RpcRequest req, Principal principal) {
        logger.info("RPC CALL [dev: {} method: {}]", principal.getName(), req.getMethod());
        logger.debug("PRC CALL DETAILS\n\tMethod: {}\n\tParams: {}", req.getMethod(), req.getParams());

        return rpcService.exec(req);
    }


    @GetMapping()
    public String ping() {
        return "Hello";
    }

    @ExceptionHandler
    public RpcResponseFailure handleException(RpcException e) {
        return handleException(e.getMessage(), e);
    }

    @ExceptionHandler
    public RpcResponseFailure handleException(MethodArgumentNotValidException e) {
        return handleException(e.getBindingResult().getFieldError().getDefaultMessage(), e);
    }

    @ExceptionHandler
    public RpcResponseFailure handleException(HttpMessageNotReadableException e) {
        return handleException(RpcConstants.ERROR_RPC_BAD_REQUEST, e);
    }
}

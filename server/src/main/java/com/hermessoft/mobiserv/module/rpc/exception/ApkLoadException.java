package com.hermessoft.mobiserv.module.rpc.exception;

import com.hermessoft.mobiserv.module.rpc.util.RpcConstants;

public class ApkLoadException extends RpcException {
    public ApkLoadException() {
        super(RpcConstants.ERROR_RPC_APK_CANT_BE_LOADED);
    }

    public ApkLoadException(Throwable e) {
        super(RpcConstants.ERROR_RPC_APK_CANT_BE_LOADED, e);
    }
}

package com.hermessoft.mobiserv.module.rpc.apk.impl;

import com.hermessoft.mobiserv.module.rpc.apk.ApkLoader;
import com.hermessoft.mobiserv.module.rpc.exception.ApkFileNotFoundException;
import com.hermessoft.mobiserv.module.rpc.exception.ApkLoadException;
import com.hermessoft.mobiserv.module.rpc.exception.RpcBadParamsException;
import com.hermessoft.mobiserv.module.rpc.util.RpcConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ApkLoaderImpl implements ApkLoader {
    private static final Logger logger = LoggerFactory.getLogger(ApkLoaderImpl.class);
    public static final String APP_VER_PARAM = "app_ver";

    private String apkStorageLocation;

    public ApkLoaderImpl(@Value(RpcConstants.PROP_APK_LOCATION) String apkStorageLocation) {
        this.apkStorageLocation = apkStorageLocation;
    }

    @Override
    public List<Map<String, Object>> exec(String method, List<Map<String, Object>> params) {
        List<Map<String, Object>> result = new ArrayList<>();
        result.add(new HashMap<>());

        if (params.isEmpty())
            throw new RpcBadParamsException();

        String appVer = String.valueOf(params.get(0).get(APP_VER_PARAM));

        Path path = Paths.get(apkStorageLocation, appVer);

        try (DirectoryStream<Path> stream =
                     Files.newDirectoryStream(path, "*.apk")) {

            Path firstApkFilePath = null;

            for (Path entry : stream) {
                firstApkFilePath = entry;
                break;
            }

            if (firstApkFilePath == null)
                throw new ApkFileNotFoundException();

            result.get(0).put("apk", Files.readAllBytes(firstApkFilePath));

        } catch (IOException e) {
            logger.error("Cant load apk from [{}]", path.toAbsolutePath());
            throw new ApkLoadException(e);
        }

        return result;
    }
}

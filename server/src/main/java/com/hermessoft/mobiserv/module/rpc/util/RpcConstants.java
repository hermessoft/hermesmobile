package com.hermessoft.mobiserv.module.rpc.util;

public final class RpcConstants {
    public static final String ERROR_RPC_BAD_REQUEST = "ERROR_RPC_BAD_REQUEST";
    public static final String ERROR_RPC_METHOD_NOT_FOUND = "ERROR_RPC_METHOD_NOT_FOUND";
    public static final String ERROR_RPC_METHOD_VALUE_EMPTY_OR_NULL = "ERROR_RPC_METHOD_VALUE_EMPTY_OR_NULL";
    public static final String ERROR_RPC_BAD_PARAMS = "ERROR_RPC_BAD_PARAMS";
    public static final String ERROR_RPC_APK_FILE_NOT_FOUND = "ERROR_RPC_APK_FILE_NOT_FOUND";
    public static final String ERROR_RPC_APK_CANT_BE_LOADED = "ERROR_RPC_APK_CANT_BE_LOADED";

    public static final String PROP_APK_LOCATION = "${app.apk.location}";
    public static final String PROP_DAO_METAQUERY = "${app.dao.metaquery}";
}

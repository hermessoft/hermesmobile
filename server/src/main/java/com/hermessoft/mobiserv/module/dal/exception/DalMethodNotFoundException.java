package com.hermessoft.mobiserv.module.dal.exception;

import com.hermessoft.mobiserv.module.dal.util.DalConstants;

public class DalMethodNotFoundException extends DalException {
    public DalMethodNotFoundException() {
        super(DalConstants.ERROR_METHOD_NOT_FOUND);
    }


}

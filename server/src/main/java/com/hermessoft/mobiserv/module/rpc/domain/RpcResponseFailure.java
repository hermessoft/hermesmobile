package com.hermessoft.mobiserv.module.rpc.domain;

import java.util.HashMap;
import java.util.Map;

public class RpcResponseFailure {
    private Map<String, Object> error = new HashMap<>();

    public RpcResponseFailure(int code, String message) {
        error.put("code", code);
        error.put("message", message);
    }

    public RpcResponseFailure() {

    }

    public Map<String, Object> getError() {
        return error;
    }

}

package com.hermessoft.mobiserv.module.exch.exception;

public class ExchException extends RuntimeException {
    public ExchException(String message) {
        super(message);
    }

    public ExchException(String message, Throwable e) {
        super(message, e);
    }
}

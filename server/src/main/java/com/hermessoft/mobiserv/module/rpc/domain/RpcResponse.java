package com.hermessoft.mobiserv.module.rpc.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RpcResponse {
    final private List<Map<String, Object>> result;

    public RpcResponse(List<Map<String, Object>> result) {
        this.result = result;
    }

    public RpcResponse() {
        this.result = new ArrayList<>();
    }

    public List<Map<String, Object>> getResult() {
        return result;
    }

}

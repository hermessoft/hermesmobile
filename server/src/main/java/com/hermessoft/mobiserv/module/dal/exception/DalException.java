package com.hermessoft.mobiserv.module.dal.exception;

public class DalException extends RuntimeException {
    public DalException(String message) {
        super(message);
    }

    public DalException(String message, Throwable e) {
        super(message, e);
    }

}

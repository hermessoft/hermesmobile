package com.hermessoft.mobiserv.module.exch.rest;

import com.hermessoft.mobiserv.module.dal.dao.Dao;
import com.hermessoft.mobiserv.module.exch.domain.ExchRequest;
import com.hermessoft.mobiserv.module.exch.domain.ExchResponse;
import com.hermessoft.mobiserv.module.exch.domain.ExchResponseFailure;
import com.hermessoft.mobiserv.module.exch.exception.ExchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/exch")
public class ExchController {
    private static final Logger logger = LoggerFactory.getLogger(ExchController.class);

    private Dao dao;

    public ExchController(Dao dao) {
        this.dao = dao;
    }

    @PostMapping()
    public ExchResponse exec(@RequestBody @Validated ExchRequest req) {
        logger.info("EXCH CALL [method: {}]", req.getMethod());
        logger.debug("EXCH CALL DETAILS\n\tMethod: {}\n\tParams: {}", req.getMethod(), req.getParams());

        return new ExchResponse(dao.exec(req.getMethod(), req.getParams()));
    }

    @ExceptionHandler
    public ExchResponseFailure handleException(ExchException e) {
        logger.error("", e);
        return new ExchResponseFailure(1, e.getMessage());
    }

}

package com.hermessoft.hermesa.ui;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiField;
import com.hermessoft.hermesa.config.UiField.FieldValueBinder;
import com.hermessoft.hermesa.config.UiStyle;
import com.hermessoft.hermesa.ui.editors.EditorCheckBox;
import com.hermessoft.hermesa.ui.editors.EditorLookup;
import com.hermessoft.hermesa.ui.editors.EditorMoney;
import com.hermessoft.hermesa.ui.editors.EditorNumber;
import com.hermessoft.hermesa.ui.editors.EditorQty;
import com.hermessoft.hermesa.ui.editors.EditorSwitch;
import com.hermessoft.hermesa.ui.editors.EditorText;

import java.util.HashMap;

public class ViewBuilder {
    private App app;
    private LayoutInflater inflater;
    private int defColor;

    public ViewBuilder(App app) {
        this.app = app;
        inflater = (LayoutInflater) app
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        defColor = app.getResources().getColor(
                android.R.color.primary_text_dark);
    }

    public static void getViewValues(ViewGroup viewGroup, HashMap<String, String> values) {

        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View view = viewGroup.getChildAt(i);
            String fieldName = (String) view.getTag();
            if (fieldName != null) {
                if (view instanceof FieldValueBinder) {
                    String val = ((FieldValueBinder) view).getValue(fieldName);
                    if (val != null) {
                        values.put(fieldName, val);
                    } else {

                        HashMap<String, String> map = ((FieldValueBinder) view)
                                .getValues(fieldName);
                        if (map != null) {
                            values.putAll(map);
                        }
                    }
                }

            } else if (view instanceof ViewGroup) {
                getViewValues((ViewGroup) view, values);
            }
        }

    }

    private View createField(Context context, String uiId, UiField fieldInfo, ViewGroup parent, Cursor cursor,
                             Bundle args) throws Exception {

        View field;

        switch (fieldInfo.editor) {
            case UiField.EDITOR_TEXT:
                field = new EditorText(context, fieldInfo);
                break;

            case UiField.EDITOR_CHECKBOX:
                field = new EditorCheckBox(context, fieldInfo);
                break;

            case UiField.EDITOR_SWITCH:
                field = new EditorSwitch(context, fieldInfo);
                break;

            case UiField.EDITOR_NUMBER:
                field = new EditorNumber(context, fieldInfo);
                break;

            case UiField.EDITOR_LOOKUP:
                field = new EditorLookup(context, fieldInfo, args);
                break;

            case UiField.EDITOR_QTY:
                field = new EditorQty(context, fieldInfo, args);
                break;

            case UiField.EDITOR_MONEY:
                field = new EditorMoney(context, fieldInfo);
                break;

            default:
                field = new TextView(context);
                break;
        }
        field.setTag(R.id.TAG_FIELD_NAME, fieldInfo.name);
        field.setTag(fieldInfo.name);

        field.setTag(R.id.TAG_FIELD_STYLE,
                cursor.getColumnIndex("_ui_" + fieldInfo.name + "_style"));

        field.setPadding(1, 0, 1, 0);
        return field;
    }

    private void setFieldSize(Context context, View field, UiStyle style, int def) {
        int size = style.getTextSize();
        if (size == 0)
            size = def;

        if (field instanceof TextView) {
            switch (size) {
                case UiStyle.TEXT_SIZE_SMALL:
                    ((TextView) field).setTextAppearance(context,
                            android.R.style.TextAppearance_Small);
                    break;
                case UiStyle.TEXT_SIZE_MEDIUM:
                    ((TextView) field).setTextAppearance(context,
                            android.R.style.TextAppearance_Medium);
                    break;
                case UiStyle.TEXT_SIZE_LARGE:
                    ((TextView) field).setTextAppearance(context,
                            android.R.style.TextAppearance_Large);
                    break;
                default:
                    ((TextView) field).setTextAppearance(context,
                            android.R.style.TextAppearance_Medium);
            }
        }
    }

    private void setFieldAlign(View field, UiStyle style) {
        int gravity = style.getAlignH();
        if (gravity == 0)
            return; // gravity = Gravity.LEFT;

        if (field instanceof TextView) {
            ((TextView) field).setGravity(gravity);
        }
    }

    private void setFieldTextStyle(View field, UiStyle style) {
        if (field instanceof TextView) {
            ((TextView) field).setTypeface(null, style.getTextStyle());
        }
    }

    private void setFieldTextColor(View field, UiStyle style) {
        if (style.getColor() == 0)
            return;

        if (field instanceof TextView) {
            ((TextView) field).setTextColor(style.getColor());
        }
    }

    public View buildListRow(Context context, ViewGroup parent, String uiId, Cursor cursor)
            throws Exception {
        LinearLayout result = new LinearLayout(context);
        result.setOrientation(LinearLayout.VERTICAL);
        result.setPadding(5, 10, 5, 10);

        LinearLayout.LayoutParams lpRow = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        LinearLayout row = null;
        LinearLayout.LayoutParams lpCol = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1);
        String band = "";
        boolean first_field = true;
        for (int i = 0; i < cursor.getColumnCount(); i++) {
            String colName = cursor.getColumnName(i);
            UiField fieldInfo = app.getUiCatalog().getUiInfo(uiId).getField(colName);
            UiStyle fieldStyle = fieldInfo.getStyle();

            if (colName.startsWith("_") || !fieldInfo.visible) continue;

            View field = createField(context, uiId, fieldInfo, result, cursor, null);
            setFieldSize(context, field, fieldStyle,
                    (first_field) ? UiStyle.TEXT_SIZE_MEDIUM
                            : UiStyle.TEXT_SIZE_SMALL);
            setFieldAlign(field, fieldStyle);
            setFieldTextStyle(field, fieldStyle);
            setFieldTextColor(field, fieldStyle);

            if (row == null || fieldInfo.band.isEmpty()
                    || !band.equals(fieldInfo.band)) {
                row = new LinearLayout(context);
                row.setOrientation(LinearLayout.HORIZONTAL);
                result.addView(row, lpRow);
                band = fieldInfo.band;
            }

            row.addView(field, lpCol);
            first_field = false;
        }
        return result;
    }

    public void bindListRow(View view, Context context, String uiId,
                            Cursor cursor) {

        for (int i = 0; i < cursor.getColumnCount(); i++) {
            String colName = cursor.getColumnName(i);

            View colView = view.findViewWithTag(colName);

            if (colView == null) continue;

            UiField field = null;
            try {
                field = app.getUiCatalog().getUiInfo(uiId).getField(colName);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            UiStyle fieldStyle = null;
            int styleFieldIdx = (Integer) colView.getTag(R.id.TAG_FIELD_STYLE);
            if (styleFieldIdx != -1) {
                try {
                    fieldStyle = app.getUiCatalog().getUiInfo(uiId).
                            getField(cursor.getString(styleFieldIdx)).getStyle();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            String val = cursor.getString(i);
            if (val == null) val = "";

            boolean hideView = (val.isEmpty() || val.equals("0")) && field.hideEmpty;

            if (!hideView) {
                colView.setVisibility(View.VISIBLE);
            } else {
                colView.setVisibility(View.GONE);
            }

            if (colView instanceof TextView) {
                TextView tv = (TextView) colView;
                if (!hideView && val != null && !val.isEmpty()) {
                    tv.setText(UiField.format(val, field.fmt));
                    if (fieldStyle != null) {
                        tv.setTypeface(null, fieldStyle.getTextStyle());
                        if (fieldStyle.getColor() != 0) {
                            tv.setTextColor(fieldStyle.getColor());
                        } else {
                            tv.setTextColor(defColor);
                        }
                    }
                } else {
                    tv.setText("");

                }
            }
        }
    }

    public ViewGroup buildCard(Context context, ViewGroup parent, String uiId, Cursor cursor, Bundle args)
            throws Exception {
        LinearLayout result = new LinearLayout(context);
        result.setOrientation(LinearLayout.VERTICAL);
        result.setPadding(0, 10, 0, 10);

        LinearLayout.LayoutParams lpRow = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams lpRowDivider = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, 1);
        lpRowDivider.bottomMargin = 5;
        lpRowDivider.topMargin = 5;

        LinearLayout.LayoutParams lpCol = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1);

        LinearLayout row = null;

        String band = "";
        for (int i = 0; i < cursor.getColumnCount(); i++) {
            String colName = cursor.getColumnName(i);
            UiField fieldInfo = app.getUiCatalog().getUiInfo(uiId).getField(colName);
            UiStyle fieldStyle = fieldInfo.getStyle();

            if (colName.startsWith("_") || !fieldInfo.visible) continue;

            String val = cursor.getString(i);
            if (val == null)
                val = "";
            if ((val.isEmpty() || val.equals("0")) && fieldInfo.hideEmpty)
                continue;

            View field = createField(context, uiId, fieldInfo, result, cursor, args);
            setFieldSize(context, field, fieldStyle, UiStyle.TEXT_SIZE_MEDIUM);
            setFieldAlign(field, fieldStyle);
            setFieldTextStyle(field, fieldStyle);
            setFieldTextColor(field, fieldStyle);

            if (field instanceof FieldValueBinder) {
                ((FieldValueBinder) field).setValue(fieldInfo.name, cursor);

            } else if (field instanceof TextView) {
                ((TextView) field).setText(UiField.format(val, fieldInfo.fmt));

            }
/*
            if (field.getClass() == CheckBox.class) {
                CheckBox checkBox = (CheckBox) field;
                checkBox.setText(fieldInfo.title);
                checkBox.setChecked(val.equals("1"));
            } else if (field.getClass() == Switch.class) {
                Switch Switch = (Switch) field;
                Switch.setText(fieldInfo.title);
                Switch.setChecked(val.equals("1"));
            } else if (field instanceof EditorNumber) {
                ((EditorNumber) field).setValue(val);

            } else if (field instanceof EditorLookup) {
                ((EditorLookup) field).setValue(val);

            } else if (field instanceof EditorQty) {
                ((EditorQty) field).setValue(val);
                if (cursor.getColumnIndex(fieldInfo.name + "_unt") != -1) {
                    ((EditorQty) field).setValueUnit(cursor.getInt(cursor.getColumnIndex(fieldInfo.name + "_unt")));
                }

            } else if (field instanceof TextView) {
                ((TextView) field).setText(fieldInfo.format(val));
            }
*/
            if (row == null || fieldInfo.band.isEmpty()
                    || !band.equals(fieldInfo.band)) {
                //
                LinearLayout bandLinear = new LinearLayout(context);
                bandLinear.setOrientation(LinearLayout.VERTICAL);
                View rowDivider = new View(context);
                rowDivider.setBackgroundColor(Color.DKGRAY);

                // title
                if (!fieldInfo.band.isEmpty() && !fieldInfo.band.startsWith("-")) {
                    TextView titleView = new TextView(context);
                    titleView.setText(fieldInfo.band);
                    titleView.setTextAppearance(context,
                            android.R.style.TextAppearance_Small);
                    bandLinear.addView(rowDivider, lpRowDivider);
                    bandLinear.addView(titleView, lpRow);
                } else if (fieldInfo.band.startsWith("--")) {
                    bandLinear.addView(rowDivider, lpRowDivider);
                } else if (!fieldInfo.title.isEmpty() && !fieldInfo.title.startsWith("-")
                        && !(field instanceof Switch) && !(field instanceof CheckBox)) {
                    TextView titleView = new TextView(context);
                    titleView.setText(fieldInfo.title);
                    titleView.setTextAppearance(context,
                            android.R.style.TextAppearance_Small);
                    bandLinear.addView(titleView, lpRow);
                }

                row = new LinearLayout(context);
                row.setOrientation(LinearLayout.HORIZONTAL);
                bandLinear.addView(row, lpRow);

                result.addView(bandLinear, lpRow);
                band = fieldInfo.band;
            }

            row.addView(field, lpCol);
        }
        return result;
    }

    public View build(int resource, ViewGroup parent) {
        return inflater.inflate(resource, parent, false);
    }
}

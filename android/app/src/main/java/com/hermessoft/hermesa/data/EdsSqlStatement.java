package com.hermessoft.hermesa.data;

import java.util.ArrayList;
import java.util.List;

public class EdsSqlStatement {

    private static final char[] paramDelimeters = {' ', ',', ';', ')', '\n', '\r'};

    private ArrayList<String> args;
    private String sql_source;
    private String sql_parsed;
    private boolean parsed;

    public EdsSqlStatement() {
        args = new ArrayList<String>();
    }

    private boolean isParamDelimeter(char ch) {
        for (int i = 0; i < paramDelimeters.length; i++) {
            if (ch == paramDelimeters[i]) return true;
        }
        return false;
    }

    private void parseSql(String sql) {
        args.clear();
        sql_parsed = "";
        StringBuilder sqlSB = new StringBuilder();
        StringBuilder nameSB = new StringBuilder();

        int idx = 0;
        while (idx < sql.length()) {
            char ch = sql.charAt(idx);
            while (ch != ':' && idx < sql.length()) {
                sqlSB.append(ch);
                idx++;
                if (idx < sql.length()) {
                    ch = sql.charAt(idx);
                }
            }

            if (idx == sql.length() - 1) {
                break;
            } else if (ch == ':') {
                sqlSB.append('?');
                idx++;
                nameSB.setLength(0);
                while (idx < sql.length()) {
                    ch = sql.charAt(idx);

                    if (isParamDelimeter(ch)) {
                        sqlSB.append(ch);
                        idx++;
                        break;
                    }

                    nameSB.append(ch);
                    idx++;
                }
                args.add(nameSB.toString());
            }
        }

        sql_parsed = sqlSB.toString();
        parsed = true;
    }

    public List<String> getArgs() {
        if (!parsed) parseSql(sql_source);
        return args;
    }

    public String getSql() {
        if (!parsed) parseSql(sql_source);
        return sql_parsed;
    }

    public void setSql(String sql) {
        this.sql_source = sql;
        parsed = false;
    }

    public String getText() {
        return sql_source;
    }
}

package com.hermessoft.hermesa.data;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.hermessoft.hermesa.config.Config;

public class BarScan {
    public static final String SCAN_ACTION = "scan.action";
    private final static String SCAN_RCV = "scan.rcv.message";
    private Context context;
    private Config conf;
    private boolean started;
    private String barcodeStr;
    private BroadcastReceiver mScanReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            byte[] barcode = intent.getByteArrayExtra("barocode");
            int barcodelen = intent.getIntExtra("length", 0);
            byte temp = intent.getByteExtra("barcodeType", (byte) 0);
            android.util.Log.i("debug", "----codetype--" + temp);
            barcodeStr = new String(barcode, 0, barcodelen);

            Intent iBarCode = new Intent(SCAN_ACTION);
            iBarCode.putExtra("barcode", barcodeStr);
            LocalBroadcastManager.getInstance(context).sendBroadcast(iBarCode);
        }

    };

    public BarScan(Context context, Config conf) {
        this.context = context;
        this.conf = conf;
    }

    public void start() {
        if (!started) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(SCAN_RCV);
            context.registerReceiver(mScanReceiver, filter);
            started = true;
        }
    }

    public void stop() {
        if (started) {
            context.unregisterReceiver(mScanReceiver);
        }
        started = false;
    }
}

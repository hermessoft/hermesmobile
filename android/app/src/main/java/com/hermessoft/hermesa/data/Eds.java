package com.hermessoft.hermesa.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import java.util.List;

public class Eds {
    public static final int ACT_QUERY = 1;
    public static final int ACT_EXEC = 2;
    public static final int ACT_INSERT = 3;
    public static final int ACT_REPLACE = 4;

    private String id;
    private EdsSqlStatement stmtSql;
    private int act;

    public Eds(String id, int act, String sql) {
        this.id = id;
        this.act = act;
        stmtSql = new EdsSqlStatement();
        stmtSql.setSql(sql);
    }

    private void checkSqlPresent() throws Exception {
        String sql = stmtSql.getSql();
        if (sql == null || sql.isEmpty())
            throw new Exception("Empty SQL for EDS: " + id);
    }

    private String[] bindSqlArgs(Bundle source) {
        String[] result;
        List<String> sqlArgs = stmtSql.getArgs();
        if (sqlArgs == null) {
            result = null;
        } else {
            result = new String[sqlArgs.size()];
            int idx = 0;
            for (String p : sqlArgs) {
                if (source != null && source.containsKey(p)) {
                    result[idx] = source.getString(p);
                } else {
                    result[idx] = "";
                }
                idx++;
            }
        }
        return result;
    }

    private ContentValues bindInsertArgs(Bundle source) {
        ContentValues result = new ContentValues();
        for (String key : source.keySet()) {
            result.put(key, source.getString(key));
        }
        return result;
    }

    private ContentValues bindReplaceArgs(Bundle source) {
        ContentValues result = new ContentValues();
        //!!! for work replace
        for (String key : source.keySet()) {
            if ((key.equals("_id") || key.equals("id"))
                    && (source.getString(key) == null || source.getString(key)
                    .equals("")))
                continue;
            result.put(key, source.getString(key));
        }
        return result;
    }


    public Cursor query(SQLiteDatabase db, Bundle args) throws Exception {
        checkSqlPresent();
        return db.rawQuery(stmtSql.getSql(), bindSqlArgs(args));
    }

    public Bundle exec(SQLiteDatabase db, Bundle args) throws Exception {
        checkSqlPresent();
        Bundle result = new Bundle();
        long newId;
        switch (act) {
            case ACT_QUERY:
                Cursor cur = query(db, args);
                cur.moveToNext();
                if (!cur.isAfterLast()) {
                    for (int i = 0; i < cur.getColumnCount(); i++) {
                        result.putString(cur.getColumnName(i), cur.getString(i));
                    }
                } else {
                    for (int i = 0; i < cur.getColumnCount(); i++) {
                        result.putString(cur.getColumnName(i), "");
                    }
                }
                break;
            case ACT_EXEC:
                db.execSQL(stmtSql.getSql(), bindSqlArgs(args));
                break;
            case ACT_INSERT:
                newId = db.insertOrThrow(stmtSql.getText(), null, bindInsertArgs(args));
                result.putString("id", String.valueOf(newId));
                break;
            case ACT_REPLACE:
                newId = db.replaceOrThrow(stmtSql.getText(), null, bindReplaceArgs(args));
                result.putString("id", String.valueOf(newId));
                break;
        }

        return result;
    }

}

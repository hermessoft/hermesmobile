package com.hermessoft.hermesa.ui.cls;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Spinner;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiAction;
import com.hermessoft.hermesa.data.EdsAdapter;
import com.hermessoft.hermesa.data.EdsAdapterLookup;
import com.hermessoft.hermesa.data.EdsLoader;
import com.hermessoft.hermesa.util.SwipeHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class UiList extends UiBaseActivity implements
        LoaderManager.LoaderCallbacks<Cursor>, ActionBar.TabListener {

    public static final String UI_CLS = "activity:list";

    private List<String> spnValues = new ArrayList<String>();
    private List<String> spnNames = new ArrayList<String>();

    private ListView lvMain;
    private EdsAdapter adapter;
    private SwipeHelper swipeHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_list);

        lvMain = (ListView) findViewById(R.id.data);
        registerForContextMenu(lvMain);
        lvMain.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                setItemSelectedData(position);
                onContextDefItemClick();
            }
        });

        fillList();
        setupSpinner();
        setupSpinner2();
        //setupTabs();

        // Gesture detection
        swipeHelper = new SwipeHelper(this);
        swipeHelper.setOnSwipeListener(new SwipeHelper.OnSwipeListener() {

            @Override
            public void onRightToLeftSwipe() {
                setSpinner2Next();
            }

            @Override
            public void onLeftToRightSwipe() {
                setSpinner2Prev();
            }
        });

        //  lvMain.setOnTouchListener(swipeHelper.getGestureListener());
    }

    private void setupTabs() {
        UiAction act;
        act = uiInfo.getAction("TABS");
        if (act != null) {

            ActionBar bar = getActionBar();
            bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

            Object data = act.exec(this, getUiValues());
            if (data != null && data instanceof Cursor) {
                Cursor cur = (Cursor) data;
                cur.moveToFirst();
                while (!cur.isAfterLast()) {
                    spnValues.add(cur.getString(cur.getColumnIndex("_id")));
                    spnNames.add(cur.getString(cur.getColumnIndex("name")));
                    cur.moveToNext();
                }
                cur.close();
            }
        }
    }

    private void setupSpinner() {
        UiAction act;
        act = uiInfo.getAction("SPINNER");
        if (act != null) {
            setTitle("");

            EdsAdapterLookup adapter = new EdsAdapterLookup(this, act.handler, (Cursor) act.exec(this, getUiValues()));
            getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
            getActionBar().setListNavigationCallbacks(adapter,
                    new OnNavigationListener() {

                        @Override
                        public boolean onNavigationItemSelected(
                                int position, long itemId) {

                            getUiValues().putString("fspn", String.valueOf(itemId));
                            refreshList();

                            return false;
                        }
                    });
        }
    }

    private void setupSpinner2() {
        UiAction act;
        act = uiInfo.getAction("SPINNER2");
        if (act != null) {
            EdsAdapterLookup adapter = new EdsAdapterLookup(this, act.handler, (Cursor) act.exec(this, getUiValues()));
            Spinner spinner = (Spinner) findViewById(R.id.spn2);
            spinner.setAdapter(adapter);
            spinner.setPrompt(act.title);
            spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {

                    getUiValues().putString("fspn2", String.valueOf(id));
                    refreshList();
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });
            ((Button) findViewById(R.id.spn2Next))
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Spinner spinner = (Spinner) findViewById(R.id.spn2);
                            if (spinner.getSelectedItemPosition() < spinner.getAdapter().getCount() - 1) {
                                spinner.setSelection(spinner.getSelectedItemPosition() + 1);
                                lvMain.setSelection(0);
                            }

                        }
                    });


        } else {
            findViewById(R.id.spn2Panel).setVisibility(View.GONE);

        }
    }

    private void refreshList() {
        getSupportLoaderManager().getLoader(0).forceLoad();
        //getSupportLoaderManager().restartLoader(0, null, this);
    }

    private void resetFilters() {
        getActionBar().setSelectedNavigationItem(0);
        ((Spinner) findViewById(R.id.spn2)).setSelection(0);
        lvMain.clearTextFilter();
        lvMain.setSelection(0);
    }

    private void setSpinner2Next() {
        Spinner spinner = (Spinner) findViewById(R.id.spn2);
        if (spinner.getSelectedItemPosition() < spinner.getAdapter().getCount() - 1) {
            spinner.setSelection(spinner.getSelectedItemPosition() + 1);
            lvMain.setSelection(0);
        }
    }

    private void setSpinner2Prev() {
        Spinner spinner = (Spinner) findViewById(R.id.spn2);
        if (spinner.getSelectedItemPosition() > 0) {
            spinner.setSelection(spinner.getSelectedItemPosition() - 1);
            lvMain.setSelection(0);
        }
    }

    private void fillList() {
        try {
            adapter = new EdsAdapter(this, uiInfo.uiId);
            lvMain.setAdapter(adapter);
            getSupportLoaderManager().initLoader(0, null, this);
        } catch (Exception e) {
            app.catchError(e);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Loader<Cursor> result = null;
        try {
            result = new EdsLoader(this, uiInfo.uiId,
                    this.getUiValues());
        } catch (Exception e) {
            app.catchError(e);
        }
        return result;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
        try {
            adapter.changeCursor(cursor);
        } catch (Exception e) {
            app.catchError(e);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursor) {
        adapter.changeCursor(null);
    }


    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.uilist, menu);

        if (!uiInfo.getOption("UseSearch").equals("0")) {

            menu.findItem(R.id.action_search).setVisible(true);

            SearchView sv = (SearchView) menu.findItem(R.id.action_search).getActionView();

	/*int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
	EditText searchEditText = (EditText) sv.findViewById(searchSrcTextId);
	searchEditText.setTextColor(Color.BLUE); // set the text color
	searchEditText.setHintTextColor(Color.BLUE); // set the hint color
        */
            sv.setOnQueryTextListener(new OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String s) {
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    String s = !TextUtils.isEmpty(newText) ? newText : "";
                    if (s.length() > 1) {
                        s = s.toUpperCase(Locale.getDefault());
                        getUiValues().putString("ftxt", "%" + s + "%");
                    } else
                        getUiValues().putString("ftxt", "");

                    refreshList();
                    return true;
                }

            });
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
	/*if (item.getItemId() == R.id.uilist_action_search) {
	    InputMethodManager inputMgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	    inputMgr.toggleSoftInput(0, 0);
	    return true;
	} else if (item.getItemId() == R.id.uilist_action_refresh) {
	    resetFilters();	   
	    return true; 
	} else {
	    return super.onOptionsItemSelected(item);
	}*/
    }

    private void setItemSelectedData(int position) {
        getUiValues().putString("item_id",
                String.valueOf(adapter.getItemId(position)));
        Cursor cur = adapter.getCursor();
        for (int i = 0; i < cur.getColumnCount(); i++) {
            String col = cur.getColumnName(i);
            if (!col.equals("_id"))
                getUiValues().putString("item_" + col, cur.getString(i));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo acmi = (AdapterContextMenuInfo) item
                .getMenuInfo();
        setItemSelectedData(acmi.position);
        return super.onContextItemSelected(item);
    }


    @Override
    public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDestroy() {
        getSupportLoaderManager().destroyLoader(0); //for binding params
        super.onDestroy();
    }

    @Override
    protected void onViewReload() {
        refreshList();
    }


}

package com.hermessoft.hermesa.ui.cls;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiAction;

import java.util.List;


public class UiPager extends UiBaseActivity {

    public static final String UI_CLS = "activity:pager";

    ViewPager pager;
    UiPagerAdapter adapter;
    List<UiAction> pages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_pager);

        pager = (ViewPager) findViewById(R.id.pager);
        pages = uiInfo.getActions("PAGE");

        adapter = new UiPagerAdapter(this.getSupportFragmentManager());
        pager.setAdapter(adapter);
    }


    private class UiPagerAdapter extends FragmentPagerAdapter {

        public UiPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            UiAction page = pages.get(position);
            Fragment result = null;
            try {
                result = (Fragment) page.exec(UiPager.this, getUiValues());
                if (result == null)
                    throw new Exception(String.format("CRASH ON PAGE %s CREATE", page.act));
            } catch (Exception e) {
                app.catchError(e);
            }
            return result;
        }

        @Override
        public int getCount() {
            return pages.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pages.get(position).title;
        }

    }

}

package com.hermessoft.hermesa.ui.cls;

import android.content.Context;
import android.os.Bundle;

import com.hermessoft.hermesa.App;

public class UiRedirect {

    public static Object startUI(App app, Context context, String ui, Bundle args) {
        Object result = null;
        if (args != null) {
            String targetUi = args.getString("target");
            if (targetUi != null && !targetUi.isEmpty()) {
                app.getUiCatalog().getUiInfo(targetUi).exec(context, args, 0);
            }
        }
        return result;
    }
}

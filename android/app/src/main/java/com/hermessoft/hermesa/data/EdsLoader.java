package com.hermessoft.hermesa.data;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;

import com.hermessoft.hermesa.App;

public class EdsLoader extends CursorLoader {

    private DAL dal;
    private String edsId;
    private Bundle params;

    public EdsLoader(Context context, String edsId, Bundle args) {
        super(context);
        try {
            App app = (App) context.getApplicationContext();
            dal = app.getDAL();
            this.edsId = edsId;
            params = args;

        } catch (Exception e) {
            e.printStackTrace();
            App.sendError(this.getContext(), e.toString());
        }

    }

    @Override
    public Cursor loadInBackground() {
        Cursor cursor = null;
        try {
            cursor = dal.edsQuery(edsId, params);
        } catch (Exception e) {
            e.printStackTrace();
            App.sendError(this.getContext(), e.toString());
        }
        return cursor;
    }

}

package com.hermessoft.hermesa.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.R;

import java.io.IOException;

public class ErrorDumpActivity extends Activity {

    private TextView tvMessage;
    private App app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edump);
        app = (App) getApplication();

        tvMessage = (TextView) findViewById(R.id.tvMessage);
        try {
            tvMessage.setText(app.getErrorDump());
        } catch (IOException e) {
            app.catchError(e);
        }

        ((Button) findViewById(R.id.btnClose))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        ((Button) findViewById(R.id.btnClear))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        app.clearErrorDump();
                        finish();
                    }
                });

        ((Button) findViewById(R.id.btnSend))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent emailIntent = new Intent(
                                android.content.Intent.ACTION_SEND);
                        emailIntent.setType("text/html");
                        emailIntent.putExtra(
                                android.content.Intent.EXTRA_EMAIL,
                                new String[]{app.getConfig()
                                        .getSupportEmail()});
                        emailIntent.putExtra(
                                android.content.Intent.EXTRA_SUBJECT,
                                "Error Dump");
                        emailIntent
                                .putExtra(
                                        android.content.Intent.EXTRA_TEXT,
                                        "Вы также можете описать то событие, " +
                                                "вследствие которого произошла ошибка. ");
                        emailIntent.putExtra(Intent.EXTRA_STREAM,
                                Uri.parse("file://" + app.getErrorDumpZip()));
                        startActivity(Intent.createChooser(emailIntent,
                                "Sending mail..."));
                        app.clearErrorDump();
                        finish();
                    }
                });

    }

}

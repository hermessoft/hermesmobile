package com.hermessoft.hermesa.ui.cls;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.config.UiCatalog;
import com.hermessoft.hermesa.data.Geo;


public class UiGeo {
    public static final String UI_CLS = "geo:query";
    public static final UiCatalog.UiFactory FACTORY = new UiCatalog.UiFactory() {
        @Override
        public Object startUi(String cls, Class uiClass, Context context,
                              String ui, Bundle args, int options) throws Exception {
            return UiGeo.start(cls, context, ui, args);


        }

    };

    public static Object start(String cls, Context context, String ui, Bundle args) throws Exception {

        Bundle result = args;
        result.putString("utc", "0");
        result.putString("lat", "0");
        result.putString("lng", "0");
        result.putString("acrc", "0");

        Geo geo = ((App) context.getApplicationContext()).getGeo();

        int err = 0;

        if (!geo.isEnabled()) {
            err = 1;
        }

        if (err == 0) {
            Location location = geo.getLastLocation();

            if (location != null) {
                result.putString("utc", String.valueOf(location.getTime()));
                result.putString("lat", String.valueOf(location.getLatitude()));
                result.putString("lng", String.valueOf(location.getLongitude()));
                result.putString("acrc", String.valueOf(location.getAccuracy()));
            } else {
                err = 2;
            }

        }

        if (err == 1) {
            ((App) context.getApplicationContext()).showToast("Ошибка! GPS отсутсвует");
        }

        if (err != 0) {
            ((App) context.getApplicationContext()).showToast("Ошибка получения координат");
        }

        result.putString("err", String.valueOf(err));
        return result;
    }
}

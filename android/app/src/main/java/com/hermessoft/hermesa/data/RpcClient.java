package com.hermessoft.hermesa.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public class RpcClient {

    private final RpcService service;
    private final String urlPath;

    public RpcClient(String url, String username, String password) {

        String baseUrl = !url.startsWith("http") ? "http://" + url : url;

        HttpUrl parsedUrl = HttpUrl.parse(baseUrl);
        urlPath = parsedUrl.encodedPath().substring(1);
        baseUrl = parsedUrl.newBuilder().encodedPath("/").toString() ;

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        AuthenticationInterceptor authenticationInterceptor = new AuthenticationInterceptor(Credentials.basic(username, password));
        httpClient.addInterceptor(authenticationInterceptor);
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(
                        //GsonConverterFactory.create())
                        JacksonConverterFactory.create())
                .client(httpClient.build())
                .build();

        service = retrofit.create(RpcService.class);
    }

    public Object[] call(String method, Map<String, Object> params) throws JsonRpcClientException {
        List<Map<String, Object>> listParams = new ArrayList<>();
        listParams.add(params);
        return call(method, listParams);
    }

    public Object[] call(String method, List<Map<String, Object>> params) throws JsonRpcClientException {
        RpcRequest rpcRequest = new RpcRequest(method, params);

        try {
            Response<RpcResponse> resp = service.call(urlPath, rpcRequest).execute();
            if (resp.isSuccessful()) {
                RpcResponse rpcResponse = resp.body();
                if (rpcResponse.getError().isEmpty()) {
                    return rpcResponse.getResult().toArray();
                } else
                    throw new Exception((String) rpcResponse.getError().get("message"));
            } else
                throw new Exception("Error with code: " + resp.code());
        } catch (Exception e) {
            throw new JsonRpcClientException(e.getMessage());
        }
    }


    private static class AuthenticationInterceptor implements Interceptor {

        private String authToken;

        public AuthenticationInterceptor(String token) {
            this.authToken = token;
        }

        public okhttp3.Response intercept(Chain chain) throws IOException {
            okhttp3.Request original = chain.request();

            okhttp3.Request.Builder builder = original.newBuilder()
                    .header("Authorization", authToken);

            okhttp3.Request request = builder.build();
            return chain.proceed(request);
        }
    }

    public interface RpcService {
        @POST("{urlPath}")
        Call<RpcResponse> call(@Path("urlPath") String urlPath, @Body RpcRequest rpcRequest);
    }

    public class RpcRequest {
        private final String method;
        private final List<Map<String, Object>> params;

        public RpcRequest(String method, List<Map<String, Object>> params) {
            this.method = method;
            this.params = params;
        }

        public String getMethod() {
            return method;
        }

        public List<Map<String, Object>> getParams() {
            return params;
        }

    }

    public static class RpcResponse {

        private Map<String, Object> error = new HashMap<>();
        private List<Map<String, Object>> result;

        public Map<String, Object> getError() {
            return error;
        }

        public void setError(Map<String, Object> error) {
            this.error = error;
        }

        public List<Map<String, Object>> getResult() {
            return result;
        }

        public void setResult(List<Map<String, Object>> result) {
            this.result = result;
        }
    }

    public static class JsonRpcClientException extends RuntimeException {

        public JsonRpcClientException(Exception e) {
            super(e);
        }

        public JsonRpcClientException(String message) {
            super(message);
        }

    }
}

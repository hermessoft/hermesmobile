package com.hermessoft.hermesa.data;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.CursorAdapter;
import android.widget.SectionIndexer;

import com.hermessoft.hermesa.App;

public class EdsAdapterSection extends CursorAdapter implements SectionIndexer {

    private App app;
    private String uiId;
    private SectionIndexer sIndexer;

    public EdsAdapterSection(Context context, String uiId) {
        super(context, null, true);
        app = (App) context.getApplicationContext();
        this.uiId = uiId;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        app.getViewBuilder().bindListRow(view, context, uiId, cursor);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        if (sIndexer == null && cursor.getColumnIndex("name") != -1) {
            sIndexer = new AlphabetIndexer(cursor,
                    cursor.getColumnIndex("name"),
                    "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЬЭЮЯ");
        }

        View result = null;
        try {
            result = app.getViewBuilder().buildListRow(context, parent, uiId, cursor);
        } catch (Exception e) {
            app.catchError(e);
        }
        return result;
    }

    @Override
    public void changeCursor(Cursor cursor) {
        super.changeCursor(cursor);
        // indexer = CreateIndexer(cursor);
        if (sIndexer != null)
            ((AlphabetIndexer) sIndexer).setCursor(cursor);
    }


    @Override
    public int getPositionForSection(int sectionIndex) {
        // TODO Auto-generated method stub
        if (sIndexer != null) {
            return sIndexer.getPositionForSection(sectionIndex);
        } else
            return 0;
    }

    @Override
    public int getSectionForPosition(int position) {
        // TODO Auto-generated method stub
        if (sIndexer != null) {
            return sIndexer.getSectionForPosition(position);
        } else
            return 0;
    }

    @Override
    public Object[] getSections() {
        // TODO Auto-generated method stub
        if (sIndexer != null) {
            return sIndexer.getSections();
        } else
            return null;
    }

}

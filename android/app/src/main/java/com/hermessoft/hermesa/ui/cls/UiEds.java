package com.hermessoft.hermesa.ui.cls;

import android.content.Context;
import android.os.Bundle;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.config.UiCatalog.UiFactory;
import com.hermessoft.hermesa.data.DAL;

public class UiEds {
    public static final String UI_CLS_EXEC = "eds:exec";
    public static final String UI_CLS_QUERY = "eds:query";
    public static final UiFactory FACTORY = new UiFactory() {
        @Override
        public Object startUi(String cls, Class uiClass, Context context,
                              String ui, Bundle args, int options) throws Exception {
            return UiEds.start(cls, context, ui, args);


        }

    };

    public static Object start(String cls, Context context, String ui, Bundle args) throws Exception {
        DAL dal = ((App) context.getApplicationContext()).getDAL();
        if (cls.equals(UI_CLS_QUERY)) {
            return dal.edsQuery(ui, args);
        } else {
            return dal.edsExec(ui, args);
        }
    }

}

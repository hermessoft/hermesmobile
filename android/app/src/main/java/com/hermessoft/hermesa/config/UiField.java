package com.hermessoft.hermesa.config;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class UiField {
    public static final int EDITOR_TEXTVIEW = 0;
    public static final int EDITOR_TEXT = 1;
    public static final int EDITOR_CHECKBOX = 2;
    public static final int EDITOR_SWITCH = 3;
    public static final int EDITOR_NUMBER = 4;
    public static final int EDITOR_LOOKUP = 5;
    public static final int EDITOR_QTY = 6;
    public static final int EDITOR_MONEY = 7;

    public final String uiId;
    public final String name;
    public String title;
    public boolean visible = true;
    public String band = "";
    public int editor = EDITOR_TEXTVIEW;
    public String fmt = "";
    public boolean hideEmpty;
    private UiStyle style = new UiStyle();


    public UiField(String uiId, String name) {
        this.uiId = uiId;
        this.name = name;
        title = name;
    }

    public static String format(String value, String fmt) {
        if (fmt == null || fmt.isEmpty()) {
            return value;
        } else {
            try {
                return String.format(fmt, value);
            } catch (Exception e) {
                return "FMT_ERROR";
            }
        }
    }

    public UiStyle getStyle() {
        return style;
    }

    public void setOptions(String options) {
        if (options == null) options = "";
        ArrayList<String> arrOptions = new ArrayList<String>(Arrays.asList(options.split(";")));
        hideEmpty = arrOptions.indexOf("HideEmpty") != -1;
    }

    public interface FieldValueBinder {
        public void setValue(String fieldName, Cursor cursor);

        public String getValue(String fieldName);

        public HashMap<String, String> getValues(String fieldName);
    }
}

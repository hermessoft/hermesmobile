package com.hermessoft.hermesa.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.Config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SyncRunner {
    public static final String MSG_START = "MSG_SYNC_START";
    public static final String MSG_FINISH = "MSG_SYNC_FINISH";
    public static final String MSG_ERROR = "MSG_SYNC_ERROR";
    public static final String MSG_PROGRESS = "MSG_SYNC_PROGRESS";
    public static final String MSG_APP_UPDATE = "MSG_APP_UPDATE";
    public static final String MSG_PULL_START = "MSG_PULL_START";
    public static final String MSG_PULL_FINISH = "MSG_PULL_FINISH";

    public static final String MSG_EXTRA_MESSAGE = "MESSAGE";
    public static final String SYNC_PARAM_SYNC_SCRIPT = "sync_uri";
    private static final String SYNC_PARAM_APP_VER = "app_ver";
    private static final String SYNC_PARAM_DB_ID = "db_id";
    private static final String SYNC_PARAM_DB_VER = "db_ver";
    private static final String SYNC_PARAM_DEV_ID = "dev_id";
    private static final String SYNC_PARAM_SYNC_ID = "sync_id";

    private static final String RPC_ACCESS_CHECK = "MFW.AccessCheck";
    private static final String RPC_APP_UP_CHECK = "MFW.AppUpCheck";
    private static final String RPC_APK_LOAD = "APK.ApkLoad";
    private static final String RPC_DATABASE_UP_CHECK = "MFW.DatabaseUpCheck";
    private static final String RPC_DATABASE_UP_DDL = "MFW.DatabaseUpDDL";
    private static final String RPC_DATABASE_UP_SAVE = "MFW.DatabaseUpSave";
    private static final String RPC_PULL = "MFW.Pull";
    private static final String RPC_PULL_FINISH = "MFW.PullFinish";
    private static final String RPC_PUSH = "MFW.Push";
    private static final String RPC_PUSH_FINISH = "MFW.PushFinish";
    private Context context;

    ;
    private Config conf;
    private DAL dal;
    private String log;
    private PullTask pullTask;
    private PushTask pushTask;
    private PullTask syncTask;
    public SyncRunner(Context context, Config conf, DAL dal) {
        this.context = context;
        this.conf = conf;
        this.dal = dal;
    }

    private void savePref(int resId, String val) {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        pref.edit().putString(context.getString(resId), val).commit();
    }

    private void sendBroadcast(String action) {
        LocalBroadcastManager.getInstance(context).sendBroadcast(
                new Intent(action));
    }

    private void publishStart(SyncKind sk) {
        log = "";
        sendBroadcast(MSG_START);
        String dt = DateFormat.getDateTimeInstance().format(
                Calendar.getInstance().getTime());
        progressLn(dt);
        switch (sk) {
            case PULL:
                sendBroadcast(MSG_PULL_START);
                savePref(R.string.sync_pull_last_time, dt);
                progressLn(context.getString(R.string.sync_log_pull_start));
                break;
            case PUSH:
                savePref(R.string.sync_push_last_time, dt);
                progressLn(context.getString(R.string.sync_log_push_start));
                break;
            default:
                break;
        }
        progressLn("Сервер №  " + String.valueOf(conf.getSyncServerCurrent()));

    }

    private void publishFinish(SyncKind sk) {
        switch (sk) {
            case PULL:
                savePref(R.string.sync_pull_last_error, "");
                progressLn(context.getString(R.string.sync_log_pull_finish));
                sendBroadcast(MSG_PULL_FINISH);
                break;
            case PUSH:
                savePref(R.string.sync_push_last_error, "");
                progressLn(context.getString(R.string.sync_log_push_finish));
                break;
            default:
                break;
        }
        sendBroadcast(MSG_FINISH);
    }

    private void publishError(SyncKind sk, Exception e) {
        e.printStackTrace();
        progressLn(e.toString());
        sendBroadcast(MSG_ERROR);
        switch (sk) {
            case PULL:
                savePref(R.string.sync_pull_last_error, e.toString());
                break;
            case PUSH:
                savePref(R.string.sync_push_last_error, e.toString());
                break;
            default:
                break;
        }
    }

    private void progressLn(String msg) {
        progress("\n" + msg);
    }

    private void progressLn(int resId) {
        progressLn(context.getString(resId));
    }

    private void progress(int resId) {
        progress(context.getString(resId));
    }

    private void progress(String msg) {
        log = log + msg;
        Intent intent = new Intent(MSG_PROGRESS);
        intent.putExtra(MSG_EXTRA_MESSAGE, msg);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public boolean startPull() {
        if (!isRunning()) {
            pullTask = new PullTask();
            pullTask.execute();
            return true;
        }
        return false;
    }

    public boolean startPush() {
        if (!isRunning()) {
            pushTask = new PushTask();
            pushTask.execute();
            return true;
        }
        return false;
    }

    public void syncCancel() {

    }

    public boolean isRunning() {
        return (pullTask != null && pullTask.getStatus().equals(
                AsyncTask.Status.RUNNING))
                || (pushTask != null && pushTask.getStatus().equals(
                AsyncTask.Status.RUNNING))
                || (syncTask != null && syncTask.getStatus().equals(
                AsyncTask.Status.RUNNING));
    }

    public String getLog() {
        return log;
    }

    public String getInfo() {
        StringBuilder result = new StringBuilder();
        String last_time;
        String last_error;
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);

        last_time = pref.getString(
                context.getString(R.string.sync_pull_last_time), "");
        last_error = "OK";
        if (!last_time.isEmpty()) {
            result.append("\n" + context.getString(R.string.sync_pull_title));
            result.append("\n----------------------");
            if (!pref.getString(
                    context.getString(R.string.sync_pull_last_error), "")
                    .isEmpty())
                last_error = "ERROR";
            result.append("\n" + last_time + " - " + last_error);
        }

        result.append("\n");

        last_time = pref.getString(
                context.getString(R.string.sync_push_last_time), "");
        last_error = "OK";
        if (!last_time.isEmpty()) {
            result.append("\n" + context.getString(R.string.sync_push_title));
            result.append("\n----------------------");
            if (!pref.getString(
                    context.getString(R.string.sync_push_last_error), "")
                    .isEmpty())
                last_error = "ERROR";
            result.append("\n" + last_time + " - " + last_error);
        }

        return result.toString();
    }

    private Map<String, Object> getPullRpcParams() {
        final HashMap<String, Object> result = new HashMap<String, Object>();
        result.put(SYNC_PARAM_DEV_ID, conf.getDeviceID());
        result.put(SYNC_PARAM_APP_VER, App.APP_VERSION);
        result.put(SYNC_PARAM_DB_ID, conf.getDatabaseID());
        result.put(SYNC_PARAM_DB_VER, conf.getDatabaseVer());
        result.put(SYNC_PARAM_SYNC_ID, 0);
        return result;
    }

    private Map<String, Object> getPushRpcParams() {
        final HashMap<String, Object> result = new HashMap<String, Object>();
        result.put(SYNC_PARAM_DB_ID, conf.getDatabaseID());
        return result;
    }

    private boolean accessCheck(RpcClient client) {
        boolean result = false;

        final Object[] data = (Object[]) client.call(RPC_ACCESS_CHECK,
                getPullRpcParams());
        if (data.length > 0) {
            @SuppressWarnings("unchecked")
            Map<String, Object> row = (Map<String, Object>) data[0];

            Object val = row.get("STATUS");
            if (val != null)
                result = Integer.valueOf(val.toString()) != 0;

            val = row.get("ACCESS_LEVEL");
            if (val != null) {
                conf.setAccessLevel(Integer.valueOf(val.toString()));
            } else {
                conf.setAccessLevel(0);
            }
        }

        return result;
    }

    private int appUpCheck(RpcClient client) {
        int result = 0;

        final Object[] data = (Object[]) client.call(RPC_APP_UP_CHECK,
                getPullRpcParams());

        if (data.length > 0) {
            @SuppressWarnings("unchecked")
            Map<String, Object> row = (Map<String, Object>) data[0];
            Object val = row.get("APP_VER");
            if (val != null)
                result = Integer.valueOf(val.toString());
        }

        return result;
    }

    private String loadApk(RpcClient client, int app_ver) throws IOException {
        String result = null;

        final HashMap<String, Object> rpc_param = new HashMap<String, Object>();
        rpc_param.put(SYNC_PARAM_DEV_ID, conf.getDeviceID());
        rpc_param.put(SYNC_PARAM_APP_VER, app_ver);

        Object[] resp = client.call(RPC_APK_LOAD, rpc_param);
        if (resp.length == 0)
            return null;

        String encodedData = (String) ((Map<String, Object>) resp[0]).get("apk");
        byte[] data = (byte[]) Base64.decode(encodedData, Base64.DEFAULT);
        if (data.length > 1) {
            result = Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + "/" + App.NEW_APK_FILE;
            File apkFile = new File(result);
            apkFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(apkFile);
            fOut.write(data);
            fOut.flush();
            fOut.close();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private boolean databaseUp(RpcClient client)
            throws IOException, Exception {
        final HashMap<String, Object> rpc_params = new HashMap<String, Object>();

        rpc_params.put(SYNC_PARAM_DB_ID, conf.getDatabaseID());
        rpc_params.put(SYNC_PARAM_DB_VER, conf.getDatabaseVer());
        rpc_params.put(SYNC_PARAM_APP_VER, App.APP_VERSION);

        final Object[] arr = (Object[]) client.call(RPC_DATABASE_UP_CHECK,
                rpc_params);
        for (int row = 0; row < arr.length; row++) {
            Map<String, Object> map = (Map<String, Object>) arr[row];
            int db_ver = Integer.valueOf(map.get("DB_VER").toString());
            progressLn("-> " + map.get("DB_VER").toString());

            rpc_params.put(SYNC_PARAM_DB_VER, db_ver);
            final Object[] ddl = (Object[]) client.call(RPC_DATABASE_UP_DDL,
                    rpc_params);
            final String[] ddl_arr = new String[ddl.length];
            for (int idx = 0; idx < ddl.length; idx++) {
                ddl_arr[idx] = ((Map<String, Object>) ddl[idx]).get("DDL")
                        .toString();
            }

            dal.upgradeDB(db_ver, ddl_arr);

            rpc_params.put(SYNC_PARAM_DB_VER, conf.getDatabaseVer());
            client.call(RPC_DATABASE_UP_SAVE, rpc_params);
        }

        return arr.length > 0;
    }

    private boolean doPull(RpcClient client) throws Exception {

        final Object[] arr = (Object[]) client.call(RPC_PULL,
                getPullRpcParams());
        for (int row = 0; row < arr.length; row++) {
            @SuppressWarnings("unchecked")
            Map<String, Object> stmt = (Map<String, Object>) arr[row];
            doPullStmt(client, getPullRpcParams(), stmt);
        }

        client.call(RPC_PULL_FINISH, getPullRpcParams());

        return arr.length > 0;
    }

    private int getSyncId(String tabl) throws Exception {
        int result = 0;
        if (dal.getTableFields(tabl).contains("sync_id")) {
            Cursor cur = dal.sqlQuery("select max(sync_id) from " + tabl);
            if (cur.moveToFirst()) {
                result = cur.getInt(0);
            }
            cur.close();
        }
        return result;
    }

    private void doPullStmt(RpcClient client, Map<String, Object> params,
                            Map<String, Object> stmt) throws Exception {

        String method = stmt.get("RPC").toString();
        boolean preErase = stmt.get("ERASE").toString().equals("1");
        boolean isErase = stmt.get("ERASE").toString().equals("2");
        String tabl = stmt.get("TABL").toString();
        String hint = stmt.get("HINT").toString();
        String prevSql = stmt.get("PREV_SQL").toString();
        String postSql = stmt.get("POST_SQL").toString();
        int recall = Integer.valueOf(stmt.get("RECALL").toString());

        if (!hint.equals("-")) progressLn(hint);
        if (!tabl.equals("-")) {

            if (!prevSql.isEmpty()) {
                dal.sqlExec(prevSql);
            }

            if (preErase) {
                dal.eraseAll(tabl);
            }

            int call_count = 0;
            Object[] arr;
            do {
                call_count++;
                params.put(SYNC_PARAM_SYNC_ID, getSyncId(tabl));
                arr = (Object[]) client.call(method, params);
                if (arr.length > 0) {
                    progress(" " + String.valueOf(arr.length));
                    processPullResult(arr, tabl, isErase);
                } else {
                    progress("  0");
                }
            } while (arr.length > 0 && recall > 0 && call_count < (recall + 1));

            if (!postSql.isEmpty()) {
                dal.sqlExec(postSql);
            }

        } else {
            client.call(method, params);
            progress(" OK");
        }

    }

    public void processPullResult(Object[] result, String tabl, boolean isErase)
            throws Exception {
        final ContentValues values = new ContentValues();
        dal.beginTransaction();
        try {
            for (int idx = 0; idx < result.length; idx++) {
                values.clear();
                @SuppressWarnings("unchecked")
                Map<String, Object> row = (Map<String, Object>) result[idx];
                for (String key : row.keySet()) {
                    String keyLowerCase = key.toLowerCase();

                    String vKey = (keyLowerCase.equals("id")) ? "_id" : key;
                    values.put(vKey, row.get(key).toString());

                    if (keyLowerCase.endsWith("_upper")) {
                        String keyVal = key.substring(0, key.length()
                                - "_upper".length());
                        if (row.containsKey(keyVal)) {
                            values.put(key, row.get(keyVal).toString()
                                    .toUpperCase());
                        }
                    }
                }

                if (!isErase) {
                    dal.replace(tabl, values);
                } else
                    dal.erase(tabl, values);
            }
            dal.setTransactionSuccessful();
        } finally {
            dal.endTransaction();
        }
    }

    private boolean doPush(RpcClient client) throws Exception {

        final Object[] arr = (Object[]) client.call(RPC_PUSH, getPushRpcParams());
        for (int row = 0; row < arr.length; row++) {
            @SuppressWarnings("unchecked")
            Map<String, Object> stmt = (Map<String, Object>) arr[row];
            doPushStmt(client, stmt);
        }
        client.call(RPC_PUSH_FINISH, getPushRpcParams());
        return arr.length > 0;
    }

    private void doPushStmt(RpcClient client, Map<String, Object> stmt)
            throws Exception {

        String method = stmt.get("RPC").toString();
        String sql = stmt.get("SQL").toString();
        String hint = stmt.get("HINT").toString();
        int db_id = conf.getDatabaseID();

        if (!hint.equals("-")) progressLn(hint);

        if (!sql.isEmpty()) {

            if (!method.equals("-")) {
                List<Map<String, Object>> rows = new ArrayList<>();
                Cursor cur = dal.sqlQuery(sql);
                cur.moveToFirst();
                while (!cur.isAfterLast()) {
                    Map<String, Object> row = new HashMap<String, Object>();
                    for (int i = 0; i < cur.getColumnCount(); i++) {
                        String val = cur.getString(i);
                        if (val == null) val = "";
                        row.put(cur.getColumnName(i).equals("_id") ? "id" : cur.getColumnName(i), val);
                    }
                    row.put(SYNC_PARAM_DB_ID, db_id);
                    rows.add(row);
                    cur.moveToNext();
                }
                cur.close();

                if (rows.size() > 0) {
                    client.call(method, rows);
                    progress(" " + String.valueOf(rows.size()));
                } else {
                    progress("  0");
                }

            } else {
                dal.sqlExec(sql);
            }

        } else {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put(SYNC_PARAM_DB_ID, db_id);
            client.call(method, data);
            progress(" OK");
        }

    }

    private static enum SyncKind {
        PULL, PUSH, SYNC
    }

    class PullTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            publishStart(SyncKind.PULL);

            RpcClient rpcClient = new RpcClient(conf.getSyncServer(),
                    conf.getSyncUser(), conf.getSyncPassword());

            try {
                if (!accessCheck(rpcClient)) {
                    progressLn(R.string.sync_log_access_ban);
                    sendBroadcast(MSG_ERROR);
                    return null;
                }

                progressLn(R.string.sync_log_app_update);
                int newAppVer = appUpCheck(rpcClient);
                if (newAppVer > App.APP_VERSION) {
                    progressLn(R.string.sync_log_apk_load);
                    String newApkFile = loadApk(rpcClient, newAppVer);
                    if (newApkFile != null) {
                        publishFinish(SyncKind.PULL);
                        Intent intent = new Intent(MSG_APP_UPDATE);
                        intent.putExtra(MSG_EXTRA_MESSAGE, newApkFile);
                        LocalBroadcastManager.getInstance(context)
                                .sendBroadcast(intent);
                        return null;
                    }
                } else
                    progress(R.string.sync_log_dont_require);

                progressLn(R.string.sync_log_db_upgrade);
                if (!databaseUp(rpcClient)) {
                    progress(R.string.sync_log_dont_require);
                }

                progressLn(R.string.sync_log_data_update);
                if (!doPull(rpcClient)) {
                    progress(R.string.sync_log_dont_require);
                }

                publishFinish(SyncKind.PULL);
            } catch (Exception e) {
                publishError(SyncKind.PULL, e);
            }
            return null;
        }
    }

    class PushTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            log = "";
            publishStart(SyncKind.PUSH);

            RpcClient rpcClient = new RpcClient(conf.getSyncServer(),
                    conf.getSyncUser(), conf.getSyncPassword());

            try {
                if (!accessCheck(rpcClient)) {
                    progressLn(R.string.sync_log_access_ban);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(
                            new Intent(MSG_ERROR));
                    return null;
                }

                if (!doPush(rpcClient)) {
                    progress(R.string.sync_log_dont_require);
                }

                publishFinish(SyncKind.PUSH);
            } catch (Exception e) {
                publishError(SyncKind.PUSH, e);
            }
            return null;
        }
    }
}

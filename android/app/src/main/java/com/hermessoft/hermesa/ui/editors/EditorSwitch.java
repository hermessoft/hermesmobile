package com.hermessoft.hermesa.ui.editors;

import android.content.Context;
import android.database.Cursor;
import android.widget.Switch;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiField;
import com.hermessoft.hermesa.config.UiField.FieldValueBinder;

import java.util.HashMap;

public class EditorSwitch extends Switch implements FieldValueBinder {

    public EditorSwitch(Context context, UiField fieldInfo) {
        super(context);
        setText(fieldInfo.title);
        setTextOn(context.getString(R.string.yes));
        setTextOff(context.getString(R.string.no));
    }

    @Override
    public void setValue(String fieldName, Cursor cursor) {
        String val = cursor.getString(cursor.getColumnIndex(fieldName));
        setChecked(val.equals("1"));
    }

    @Override
    public String getValue(String fieldName) {
        return isChecked() ? "1" : "0";
    }

    @Override
    public HashMap<String, String> getValues(String fieldName) {
        // TODO Auto-generated method stub
        return null;
    }

}

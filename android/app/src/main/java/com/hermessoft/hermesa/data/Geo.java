package com.hermessoft.hermesa.data;


import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.hermessoft.hermesa.config.Config;

import java.util.concurrent.TimeUnit;

public class Geo {
    private static final long MAX_LOCATION_AGE = 60; //sec
    private static final long MIN_TIME = 20; //sec
    private static final long MIN_DISTANCE = 0; //m

    private Context context;
    private Config conf;
    private LocationManager locationManager;
    private boolean started;
    private Location lastLocation;
    private long lastTimeLocationChanged = 0;
    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            lastLocation = location;
            lastTimeLocationChanged = System.nanoTime();
            //((App)context.getApplicationContext()).showToast("Location changed");
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    public Geo(Context context, Config conf) {
        this.context = context;
        this.conf = conf;
        this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public void start() {
        if (!started) {
            locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, //NETWORK_PROVIDER,
                    MIN_TIME * 1000, MIN_DISTANCE, locationListener);
            started = true;
        }
    }

    public void stop() {
        if (started) {
            locationManager.removeUpdates(locationListener);
        }
        started = false;
    }

    public boolean isEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public boolean isRunning() {
        return started;
    }

    public Location getLastLocation() {
        Location result = lastLocation;
        long elapsedTime = TimeUnit.SECONDS.convert(System.nanoTime() - lastTimeLocationChanged,
                TimeUnit.NANOSECONDS);
        if (elapsedTime > MAX_LOCATION_AGE) {
            result = null;
        }
        return result;
    }

    public Location getLastKnownLocation() {
        return lastLocation;
    }
}

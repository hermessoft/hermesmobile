package com.hermessoft.hermesa.config;

import android.graphics.Color;
import android.view.Gravity;

public class UiStyle {
    public static final int TEXT_SIZE_SMALL = 1;
    // italic, bold +
    // italic
    public static final int TEXT_SIZE_MEDIUM = 2;
    public static final int TEXT_SIZE_LARGE = 3;
    private static final int[] TEXT_STYLE = {0, 1, 2, 3}; // normal, bold,
    private static final int[] COLORS = {0, Color.BLACK, Color.BLUE,
            Color.CYAN, Color.DKGRAY, Color.GRAY, Color.GREEN, Color.LTGRAY,
            Color.MAGENTA, Color.RED, Color.WHITE, Color.YELLOW};
    private static final int[] ALIGN = {0, Gravity.LEFT, Gravity.RIGHT,
            Gravity.CENTER};
    private int textSize;
    private int textStyle;
    private int alignh;
    //private int alignh;
    private int color;
    //private int colorbg;


    public UiStyle() {

    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = (textSize > 0 && textSize <= TEXT_SIZE_LARGE) ? textSize : 0;
    }

    public int getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(int textStyle) {
        this.textStyle = (textStyle > 0 && textStyle < TEXT_STYLE.length) ? TEXT_STYLE[textStyle] : 0;
    }

    public int getAlignH() {
        return alignh;
    }

    public void setAlignH(int align) {
        this.alignh = (align > 0 && align < ALIGN.length) ? ALIGN[align] : 0;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = (color > 0 && color < COLORS.length) ? COLORS[color] : 0;
    }

}

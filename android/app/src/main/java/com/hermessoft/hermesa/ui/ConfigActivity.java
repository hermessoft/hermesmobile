package com.hermessoft.hermesa.ui;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.R;

public class ConfigActivity extends PreferenceActivity {

    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference
                        .setSummary(index >= 0 ? listPreference.getEntries()[index]
                                : null);

            } else if (preference instanceof RingtonePreference) {
                // For ringtone preferences, look up the correct display value
                // using RingtoneManager.
                if (TextUtils.isEmpty(stringValue)) {
                    // Empty values correspond to 'silent' (no ringtone).
                    preference.setSummary(""); // R.string.pref_ringtone_silent);

                } else {
                    Ringtone ringtone = RingtoneManager.getRingtone(
                            preference.getContext(), Uri.parse(stringValue));

                    if (ringtone == null) {
                        // Clear the summary if there was a lookup error.
                        preference.setSummary(null);
                    } else {
                        // Set the summary to reflect the new ringtone display
                        // name.
                        String name = ringtone
                                .getTitle(preference.getContext());
                        preference.setSummary(name);
                    }
                }

            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference
                .setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(
                preference,
                PreferenceManager.getDefaultSharedPreferences(
                        preference.getContext()).getString(preference.getKey(),
                        ""));
    }

    protected App getApp() {
        return (App) getApplication();
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.addPreferencesFromResource(R.xml.pref_config);

        findPreference(getString(R.string.pref_info_devid)).setSummary(
                getApp().getConfig().getDeviceID());
        findPreference(getString(R.string.pref_info_appver)).setSummary(
                String.valueOf(App.APP_VERSION));
        findPreference(getString(R.string.pref_db_id)).setSummary(
                String.valueOf(getApp().getConfig().getDatabaseID()));

        findPreference(getString(R.string.pref_dev_user)).setEnabled(!getApp().isRegistered());
        findPreference(getString(R.string.pref_db_ver)).setEnabled(getApp().isRootAccess());

        if (findPreference(getString(R.string.pref_dev_user)).isEnabled()) {
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_dev_user)));
        } else {
            findPreference(getString(R.string.pref_dev_user)).setSummary(
                    getApp().getConfig().getDevUser());
        }

        if (findPreference(getString(R.string.pref_db_ver)).isEnabled()) {
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_db_ver)));
        } else {
            findPreference(getString(R.string.pref_db_ver)).setSummary(
                    String.valueOf(getApp().getConfig().getDatabaseVer()));
        }


        bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_sync_server_current)));
        bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_sync_server)));
        bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_sync_user)));

    }

    @SuppressWarnings("deprecation")
    @Override
    public void onResume() {
        super.onResume();
        findPreference(getString(R.string.pref_info_devid)).setSummary(
                getApp().getConfig().getDeviceID());
        findPreference(getString(R.string.pref_info_appver)).setSummary(
                String.valueOf(App.APP_VERSION));
        findPreference(getString(R.string.pref_db_id)).setSummary(
                String.valueOf(getApp().getConfig().getDatabaseID()));
        findPreference(getString(R.string.pref_db_ver)).setSummary(
                String.valueOf(getApp().getConfig().getDatabaseVer()));
    }

}

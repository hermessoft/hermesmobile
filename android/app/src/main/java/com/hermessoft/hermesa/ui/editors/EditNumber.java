package com.hermessoft.hermesa.ui.editors;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;


public class EditNumber extends EditText {

    public EditNumber(Context context) {
        super(context);
    }

    public EditNumber(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public EditNumber(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        if ((selStart != selEnd) || (selEnd != this.getText().length()))
            this.setSelection(this.getText().length());
    }
}

package com.hermessoft.hermesa.ui.cls;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FilterQueryProvider;
import android.widget.ListView;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.data.EdsAdapter;
import com.hermessoft.hermesa.data.EdsLoader;

import java.util.Locale;

public class UiFragmentList extends UiBaseFragment implements
        OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor>,
        FilterQueryProvider {

    public static final String UI_CLS = "fragment:list";

    ListView lvMain;
    EdsAdapter adapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        lvMain = (ListView) getView().findViewById(R.id.ListView);
        lvMain.setOnItemClickListener(this);
        fillData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ui_fragment_list, null);
        registerForContextMenu(view.findViewById(R.id.ListView));
        return view;
    }

    private void fillData() {

        try {
            adapter = new EdsAdapter(getActivity(), uiInfo.uiId);
            lvMain.setAdapter(adapter);
            getLoaderManager().initLoader(0, null, this);
            adapter.setFilterQueryProvider(this);
        } catch (Exception e) {
            app.catchError(e);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Loader<Cursor> result = null;
        try {
            result = new EdsLoader(getActivity(), uiInfo.uiId,
                    this.getUiValues());
        } catch (Exception e) {
            app.catchError(e);
        }
        return result;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
        try {

            // adapter.swapCursor(cursor);
            adapter.changeCursor(cursor);
        } catch (Exception e) {
            app.catchError(e);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursor) {
        adapter.changeCursor(null);
    }

    private void setItemSelectedData(int position) {
        getUiValues().putString("item_id",
                String.valueOf(adapter.getItemId(position)));
        Cursor cur = adapter.getCursor();
        for (int i = 0; i < cur.getColumnCount(); i++) {
            String col = cur.getColumnName(i);
            if (!col.equals("_id"))
                getUiValues().putString("item_" + col, cur.getString(i));
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        setItemSelectedData(position);
        super.onItemClick(parent, view, position, id);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo acmi = (AdapterContextMenuInfo) item
                .getMenuInfo();
        setItemSelectedData(acmi.position);
        return super.onContextItemSelected(item);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        AdapterContextMenuInfo acmi = (AdapterContextMenuInfo) menuInfo;
        setItemSelectedData(acmi.position);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().getLoader(0).forceLoad();
    }

    @Override
    public void onActionExecute(String act) {
        super.onActionExecute(act);
        if (act.equals("DEL")) {

            getLoaderManager().getLoader(0).forceLoad();
        }
    }

    @Override
    public Cursor runQuery(CharSequence constraint) {
        String s = (String) constraint;
        s = s.toUpperCase(Locale.getDefault());
        if (constraint.length() > 1) {
            getUiValues().putString("ftxt", "%" + s + "%");
        } else
            getUiValues().putString("ftxt", "");
        getLoaderManager().getLoader(0).forceLoad();
        // getSupportLoaderManager().restartLoader(0, null, this);
        return null;
    }

}

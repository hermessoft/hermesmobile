package com.hermessoft.hermesa.ui.cls;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.config.UiAction;
import com.hermessoft.hermesa.config.UiAction.OnActionExecuteListener;
import com.hermessoft.hermesa.config.UiAction.OnActionRequestListener;
import com.hermessoft.hermesa.config.UiCatalog;
import com.hermessoft.hermesa.config.UiCatalog.UiFactory;
import com.hermessoft.hermesa.config.UiInfo;
import com.hermessoft.hermesa.data.BarScan;
import com.hermessoft.hermesa.ui.ConfirmDialog;
import com.hermessoft.hermesa.ui.ConfirmDialog.OnConfirmDoneListener;

public class UiBaseActivity extends FragmentActivity
        implements OnConfirmDoneListener, OnActionRequestListener {

    public static final String PARAM_UI_TITLE = "ui_title";

    private static final String START_PARAM_UI_ID = "UI_ID";
    private static final String START_PARAM_ARGS = "UI_PARAMS";
    public static final UiFactory FACTORY = new UiFactory() {
        @Override
        public Object startUi(String cls, Class uiClass, Context context,
                              String ui, Bundle args, int options) {
            UiBaseActivity.start(uiClass, context, ui, args, options);
            return null;
        }
    };
    protected String uiId;
    protected App app;
    protected UiInfo uiInfo;
    private Bundle uiValues;
    private BroadcastReceiver mBarCodeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getUiValues().putString("barcode", intent.getStringExtra("barcode"));
            onActionRequest(uiId, "BARCODE");
        }
    };

    public static void start(Class uiClass, Context context, String ui, Bundle args, int options) {
        Intent intent = new Intent(context, uiClass);
        intent.putExtra(START_PARAM_UI_ID, ui);
        intent.putExtra(START_PARAM_ARGS, args);
        intent.addFlags(options);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app = (App) getApplication();

        uiValues = new Bundle();

        uiId = getIntent().getStringExtra(START_PARAM_UI_ID);
        Bundle startArgs = getIntent().getBundleExtra(START_PARAM_ARGS);
        if (startArgs != null) uiValues.putAll(startArgs);

        if (uiId == null || uiId.isEmpty())
            uiId = this.getClass().getName();
        uiInfo = app.getUiCatalog().getUiInfo(uiId);

        setTitle(uiInfo.title);
        UiAction titleAct = uiInfo.getAction("TITLE");
        if (titleAct != null) {
            Object actResult = titleAct.exec(this, getUiValues());
            if (actResult instanceof Bundle) {
                String uiTitle = ((Bundle) actResult).getString("title");
                if (uiTitle == null) uiTitle = ((Bundle) actResult).getString("name");
                if (uiTitle != null && !uiTitle.isEmpty()) setTitle(uiTitle);
            }
        }

        if (getTitle().toString().startsWith("-")) {
            setTitle("");
        }

    }

    protected Bundle getUiValues() {
        return uiValues;
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBarCodeReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(BarScan.SCAN_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mBarCodeReceiver, filter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        uiInfo.createContextMenu(this, menu, v, menuInfo, uiValues);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        UiAction a = uiInfo.getAction(item.getItemId());
        if (a != null) {
            onActionRequest(uiId, a.act);
            return true;
        } else {
            return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        uiInfo.createOptionsMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        uiInfo.prepareOptionsMenu(menu, uiValues);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        UiAction a = uiInfo.getAction(item.getItemId());
        if (a != null) {
            onActionRequest(uiId, a.act);
            return true;
        } else {
            //iMenu.optionsItemSelected(this, item, uiValues);
            return super.onOptionsItemSelected(item);
        }
    }

    public void onContextDefItemClick() {
        UiAction a = uiInfo.getDefaultContextMenuItem(uiValues);
        if (a != null) {
            onActionRequest(uiId, a.act);
        }
    }

    @Override
    public void onConfirmDone(String callerTag, String action) {
        execAction(callerTag, action);

    }

    public void reloadView() {
        onViewReload();
    }

    protected void execAction(String callerTag, String action) {
        if (uiId.equals(callerTag)) {
            UiAction a = uiInfo.getAction(action);
            if (a != null) {
                Object result = a.exec(this, uiValues);
                onActionExecuteAfter(action, result);
                if (a.checkOption(UiCatalog.ACTION_OPTION_VIEWRELOAD)) {
                    onViewReload();
                }
                onViewUpdate();
                if (a.checkOption(UiCatalog.ACTION_OPTION_VIEWCLOSE)) finish();

            }
        } else {
            Fragment frm = this.getSupportFragmentManager().findFragmentByTag(callerTag);
            if (frm != null) {
                ((OnActionExecuteListener) frm).onActionExecute(action);
            }
        }
    }

    protected void onActionExecuteAfter(String action, Object actionResult) {
        if (actionResult != null && actionResult instanceof Bundle) {
            getUiValues().putAll((Bundle) actionResult);
        }
    }

    protected void onViewReload() {

    }

    protected void onViewUpdate() {

    }

    public void onActionRequest(String callerTag, String action) {
        if (action.equals("DEL")) {
            confirmAction(callerTag, action, "Удалить выбранный элемент?");
        } else {
            execAction(callerTag, action);
        }
    }

    private void confirmAction(String callerTag, String action, String message) {
        ConfirmDialog dlg = ConfirmDialog.newInstance(callerTag, action, message);
        dlg.show(this.getSupportFragmentManager(), "confirm-action");
    }
}
package com.hermessoft.hermesa.ui.cls;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.config.UiAction;
import com.hermessoft.hermesa.config.UiAction.OnActionExecuteListener;
import com.hermessoft.hermesa.config.UiAction.OnActionRequestListener;
import com.hermessoft.hermesa.config.UiCatalog;
import com.hermessoft.hermesa.config.UiCatalog.UiFactory;
import com.hermessoft.hermesa.config.UiInfo;

public class UiBaseFragment extends Fragment implements OnItemClickListener, OnActionExecuteListener {
    private static final String START_PARAM_UI_ID = "UI_ID";
    private static final String START_PARAM_ARGS = "UI_PARAMS";
    public static final UiFactory FACTORY = new UiFactory() {
        @Override
        public Object startUi(String cls, Class uiClass, Context context,
                              String ui, Bundle args, int options) {
            return UiBaseFragment.start(uiClass, context, ui, args);
        }

    };
    protected App app;
    protected UiInfo uiInfo;
    private String uiId;
    private Bundle uiValues;
    private Bundle uiOuts;

    public static Object start(Class uiClass, Context context, String ui, Bundle args) {

        Fragment result = null;
        try {
            Bundle startParams = new Bundle();
            startParams.putString(START_PARAM_UI_ID, ui);
            startParams.putBundle(START_PARAM_ARGS, args);

            result = (Fragment) uiClass.newInstance();
            result.setArguments(startParams);

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (App) getActivity().getApplication();
        uiId = getArguments().getString(START_PARAM_UI_ID);
        uiInfo = app.getUiCatalog().getUiInfo(uiId);
        uiValues = new Bundle();

        Bundle startArgs = getArguments().getBundle(START_PARAM_ARGS);
        if (startArgs != null) uiValues.putAll(startArgs);
        setHasOptionsMenu(true);
    }

    protected Bundle getUiValues() {
        return uiValues;
    }


    private final void actionRequest(String act) {
        ((OnActionRequestListener) getActivity()).onActionRequest(getTag(), act);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        uiInfo.createContextMenu(getActivity(), menu, v, menuInfo, uiValues);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        UiAction a = uiInfo.getAction(item.getItemId());
        if (a != null) {
            actionRequest(a.act);
            return true;
        } else {
            return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        uiInfo.createOptionsMenu(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        uiInfo.prepareOptionsMenu(menu, uiValues);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        UiAction a = uiInfo.getAction(item.getItemId());
        if (a != null) {
            actionRequest(a.act);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
        UiAction a = uiInfo.getDefaultContextMenuItem(uiValues);
        if (a != null) {
            actionRequest(a.act);
        }
    }


    @Override
    public void onActionExecute(String act) {
        UiAction a = uiInfo.getAction(act);
        if (a != null) {
            a.exec(getActivity(), uiValues);
            onActionExecuteAfter(act);
            if (a.checkOption(UiCatalog.ACTION_OPTION_FRAGMENTRELOAD)) {
                onFragmentReload();
            }

            if (a.checkOption(UiCatalog.ACTION_OPTION_VIEWRELOAD)) {
                if (getActivity() instanceof UiBaseActivity) {
                    ((UiBaseActivity) getActivity()).reloadView();
                }
            }

            if (a.checkOption(UiCatalog.ACTION_OPTION_VIEWCLOSE)) getActivity().finish();

        }

    }

    protected void onActionExecuteAfter(String action) {

    }

    protected void onFragmentReload() {

    }
}

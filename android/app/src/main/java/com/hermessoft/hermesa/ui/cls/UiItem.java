package com.hermessoft.hermesa.ui.cls;

import android.database.Cursor;
import android.os.Bundle;
import android.view.ViewGroup;

import com.hermessoft.hermesa.R;

public class UiItem extends UiBaseActivity {

    public static final String UI_CLS = "activity:item";
    private ViewGroup rootView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.ui_item);
        rootView = (ViewGroup) findViewById(R.id.data);

        fillData();


    }

    private void fillData() {
        try {
            Cursor cursor = app.getDAL().edsQuery(uiInfo.uiId, getUiValues());
            if (cursor.moveToFirst()) {


                rootView.addView(app.getViewBuilder().buildCard(this, rootView, uiInfo.uiId,
                        cursor, getUiValues()));
            }

        } catch (Exception e) {
            app.catchError(e);
        }

    }
}

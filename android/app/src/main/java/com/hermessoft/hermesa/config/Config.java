package com.hermessoft.hermesa.config;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.data.SyncRunner;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.UUID;

public class Config {
    public static final String MSG_RELOAD = "MSG_CONFIG_RELOAD";
    private static final int ACCESS_LEVEL_ROOT = 5;
    private static final String CONF_SYN_SERVER = "SYNC.SERVER";
    private static final String CONF_HTTP_SERVER = "HTTP.SERVER";

    private Context context;
    private String deviceID;
    private HashMap<String, String> values = new HashMap<String, String>();
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SyncRunner.MSG_PULL_FINISH)) {
                LocalBroadcastManager.getInstance(context).sendBroadcast(
                        new Intent(MSG_RELOAD));
            }
        }
    };

    public Config(Context context) {
        this.context = context;
        IntentFilter filter = new IntentFilter();
        filter.addAction(SyncRunner.MSG_PULL_FINISH);
        LocalBroadcastManager.getInstance(context).registerReceiver(
                mMessageReceiver, filter);
    }

    public void clearValues() {
        values.clear();
    }

    public void setValue(String name, String val) {
        values.put(name, val);
    }

    public String getValue(String name) {
        String result = values.get(name);
        if (result == null) {
            result = "";
        }
        return result;
    }

    public String getSupportEmail() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getString(
                context.getString(R.string.pref_support_email),
                context.getString(R.string.pref_support_email_def));
    }


    private String getSyncServerDef() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String result = sharedPreferences.getString(
                context.getString(R.string.pref_sync_server),
                context.getString(R.string.pref_sync_server_def));
        if (result.isEmpty()) result = context.getString(R.string.pref_sync_server_def);
        return "http://" + result;
    }

    private String getSyncServer(int number) {
        String result;
        if (number > 1) {
            result = getValue(CONF_SYN_SERVER + String.valueOf(number));
        } else {
            result = getSyncServerDef();
        }

        if (result.isEmpty()) result = "";
        return result;
    }

    public String getSyncServer() {
        String result = getSyncServer(2);//getSyncServer(getSyncServerCurrent()); //TODO need uncomment for 10 ver app
        if (result.isEmpty()) result = getSyncServerDef();
        return result;
    }

    public String getHttpServer() {
        return getValue(CONF_HTTP_SERVER);
    }

    public int getSyncServerCurrent() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return Integer.valueOf(sharedPreferences.getString(
                context.getString(R.string.pref_sync_server_current), "1"));
    }

    public void setSyncServerCurrent(int number) {
        PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putString(context.getString(R.string.pref_sync_server_current),
                        String.valueOf(number)).commit();
    }

    public String getSyncUser() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String result = sharedPreferences.getString(
                context.getString(R.string.pref_sync_user), "");
        if (result.isEmpty())
            result = getDeviceID();
        return result;
    }

    public String getSyncPassword() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String result = sharedPreferences.getString(
                context.getString(R.string.pref_sync_psw), "");
        if (result.isEmpty())
            result = getDeviceID();
        return result;
    }

    public int getDatabaseID() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return Integer.valueOf(sharedPreferences.getString(
                context.getString(R.string.pref_db_id), "0"));
    }

    public void setDatabaseID(int db_id) {
        PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putString(context.getString(R.string.pref_db_id),
                        String.valueOf(db_id)).commit();
    }

    public int getDatabaseVer() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return Integer.valueOf(sharedPreferences.getString(
                context.getString(R.string.pref_db_ver), "0"));
    }

    public void setDatabaseVer(int db_ver) {
        PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putString(context.getString(R.string.pref_db_ver),
                        String.valueOf(db_ver)).commit();
    }

    public int getAccessLevel() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return Integer.valueOf(sharedPreferences.getString(
                context.getString(R.string.pref_access_level), "0"));
    }

    public void setAccessLevel(int level) {
        PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putString(context.getString(R.string.pref_access_level),
                        String.valueOf(level)).commit();
    }

    public boolean isRootAccess() {
        return getAccessLevel() == ACCESS_LEVEL_ROOT;
    }

    public String getDeviceID() {
        if (deviceID == null) {
            deviceID = new DeviceIdFactory(context).getDeviceid();
        }
        return deviceID;
    }

    public String getDevUser() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getString(
                context.getString(R.string.pref_dev_user),
                context.getString(R.string.pref_dev_user_def)).toUpperCase();
    }


    public class DeviceIdFactory {
        private static final String PREFS_FILE = "device_id.xml";
        private static final String PREFS_DEVICE_ID = "device_id";
        private UUID uuid;
        private String device_id;

        public DeviceIdFactory(Context context) {
            SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE,
                    0);
            device_id = prefs.getString(PREFS_DEVICE_ID, null);
            if (device_id == null) {
                final String androidId = Secure.getString(
                        context.getContentResolver(), Secure.ANDROID_ID);
                // Use the Android ID unless it's broken, in which case
                // fallback on deviceId,
                // unless it's not available, then fallback on a random
                // number which we store to a prefs file
                try {
                    if (!"9774d56d682e549c".equals(androidId)) {
                        uuid = UUID.nameUUIDFromBytes(androidId
                                .getBytes("utf8"));
                    } else {
                        final String deviceId = ((TelephonyManager) context
                                .getSystemService(Context.TELEPHONY_SERVICE))
                                .getDeviceId();
                        uuid = deviceId != null ? UUID
                                .nameUUIDFromBytes(deviceId.getBytes("utf8"))
                                : UUID.randomUUID();
                    }
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }
                // Write the value out to the prefs file
                device_id = uuid.toString().toUpperCase();
                prefs.edit().putString(PREFS_DEVICE_ID, device_id).commit();
            }

        }

        public String getDeviceid() {
            return device_id;
        }
    }
}

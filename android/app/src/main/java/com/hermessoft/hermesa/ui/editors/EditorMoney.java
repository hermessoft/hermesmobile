package com.hermessoft.hermesa.ui.editors;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiField;
import com.hermessoft.hermesa.config.UiField.FieldValueBinder;

import java.util.HashMap;

public class EditorMoney extends FrameLayout implements FieldValueBinder {

    private EditText viewExp;
    private EditText viewFract;

    public EditorMoney(Context context, UiField fieldInfo) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.editor_money, this);

        viewExp = (EditText) findViewById(R.id.viewExp);
        viewFract = (EditText) findViewById(R.id.viewFract);

    }

    @Override
    public void setValue(String fieldName, Cursor cursor) {
        viewExp.setText("");
        viewFract.setText("");

        double val;
        try {
            val = Double.parseDouble(cursor.getString(cursor.getColumnIndex(fieldName)));
        } catch (NumberFormatException nfe) {
            val = 0;
        }

        int iPart = (int) val;
        int fPart = (int) Math.round((val - iPart) * 100);
        if (fPart < 0) fPart = -fPart;
        if (iPart != 0) viewExp.setText(String.valueOf(iPart));
        if (fPart != 0) viewFract.setText(String.valueOf((int) fPart));

	/*
	double fPart = val % 1;
	int iPart = (int)(val - fPart);
	fPart = Math.round(fPart * 100);
	if (fPart < 0) fPart = -fPart;
	if (iPart != 0) viewExp.setText(String.valueOf(iPart));
	if (fPart != 0) viewFract.setText(String.valueOf((int)fPart));
	*/
    }

    @Override
    public String getValue(String fieldName) {
        String strExp = viewExp.getText().toString();
        String strFract = viewFract.getText().toString();
        int iPart, fPart;
        try {
            iPart = Integer.parseInt(strExp);
        } catch (NumberFormatException nfe) {
            iPart = 0;
        }

        try {
            fPart = Integer.parseInt(strFract);
        } catch (NumberFormatException nfe) {
            fPart = 0;
        }

        return String.valueOf(iPart) + '.' + ((fPart < 10) ? '0' : "") + String.valueOf(fPart);
    }

    @Override
    public HashMap<String, String> getValues(String fieldName) {
        // TODO Auto-generated method stub
        return null;
    }
}

package com.hermessoft.hermesa.config;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.config.UiAction.ExecuteCallback;
import com.hermessoft.hermesa.data.DAL;

import java.util.HashMap;

public class UiCatalog {
    public static final String ACTION_OPTION_VIEWRELOAD = "ViewReload";

    ;
    public static final String ACTION_OPTION_FRAGMENTRELOAD = "FragmentReload";
    public static final String ACTION_OPTION_VIEWCLOSE = "ViewClose";
    // TABLE CONF_UI
    private static final String SQL_CONF = "select * from conf";
    private static final String SQL_UI = "select * from conf_ui";
    private static final String COL_UI = "ui";
    private static final String COL_UI_CLS = "cls";
    private static final String COL_UI_TITLE = "title";
    private static final String COL_UI_OPTIONS = "options";
    private static final String COL_UI_ARGS = "args";
    private static final String COL_UI_EDS_SQL = "eds_sql";
    private static final String COL_UI_EDS_ACT = "eds_act";
    private static final String SQL_UIA = "select * from conf_uia order by ui, idx, act";
    private static final String COL_UIA_UI = "ui";
    private static final String COL_UIA_ACT = "act";
    private static final String COL_UIA_TITLE = "title";
    private static final String COL_UIA_MENU = "menu";
    private static final String COL_UIA_DEF = "def";
    private static final String COL_UIA_HANDLER = "handler";
    private static final String COL_UIA_PARAMS = "params";
    private static final String COL_UIA_CONDITION = "condition";
    private static final String COL_UIA_OPTIONS = "options";
    private static final String SQL_UIF = "select * from conf_uif";
    private static final String COL_UIF_UI = "ui";
    private static final String COL_UIF_FIELD = "field";
    private static final String COL_UIF_TITLE = "title";
    private static final String COL_UIF_VISIBLE = "visible";
    private static final String COL_UIF_BAND = "band";
    private static final String COL_UIF_EDITOR = "editor";
    private static final String COL_UIF_FORMAT = "format";
    private static final String COL_UIF_OPTIONS = "options";
    private static final String COL_UIF_TEXTSIZE = "textsize";
    private static final String COL_UIF_TEXTSTYLE = "textstyle";
    private static final String COL_UIF_ALIGNH = "alignh";
    //   private static final String COL_UIF_ALIGNV = "alignv";
    private static final String COL_UIF_COLOR = "color";
    private static HashMap<String, UiFactory> uiFactories = new HashMap<String, UiFactory>();
    //  private static final String COL_UIF_COLORBG = "colorbg";
    private static HashMap<String, Class> uiClasses = new HashMap<String, Class>();
    private Context context;
    private DAL dal;
    private Config conf;
    private HashMap<String, UiInfo> items = new HashMap<String, UiInfo>();
    private UiAction.ExecuteCallback actionExec = new ExecuteCallback() {

        @Override
        public Object exec(String handler, Context context, Bundle args, int options) {
            return getUiInfo(handler).exec(context, args, options);
        }

    };
    public UiCatalog(Context context, Config conf, DAL dal) {
        this.context = context;
        this.conf = conf;
        this.dal = dal;
        load();
    }

    public static void registerUiFactory(String cls, UiFactory factory, Class uiClass) {
        uiFactories.put(cls, factory);
        uiClasses.put(cls, uiClass);
    }

    public static UiFactory getUiFactory(String cls) {
        return uiFactories.get(cls);
    }

    public static Class getUiClass(String cls) {
        return uiClasses.get(cls);
    }

    private void load() {
        if (!dal.isReady()) return;

        for (String key : items.keySet()) {
            dal.removeEds(key);
        }

        items.clear();

        conf.clearValues();

        try {
            Cursor data;

            data = dal.sqlQuery(SQL_CONF);
            data.moveToFirst();
            while (!data.isAfterLast()) {
                conf.setValue(data.getString(data.getColumnIndex("name")),
                        data.getString(data.getColumnIndex("val")));
                data.moveToNext();
            }
            data.close();

            data = dal.sqlQuery(SQL_UI);
            data.moveToFirst();
            while (!data.isAfterLast()) {
                String uiId = data.getString(data.getColumnIndex(COL_UI));
                items.put(uiId,
                        new UiInfo(uiId,
                                data.getString(data.getColumnIndex(COL_UI_CLS)),
                                data.getString(data.getColumnIndex(COL_UI_TITLE)),
                                data.getString(data.getColumnIndex(COL_UI_OPTIONS)),
                                data.getString(data.getColumnIndex(COL_UI_ARGS)))

                );
                int eds_act = data.getInt(data.getColumnIndex(COL_UI_EDS_ACT));
                if (eds_act != 0) {
                    dal.addEds(uiId, eds_act, data.getString(data.getColumnIndex(COL_UI_EDS_SQL)));
                }
                data.moveToNext();
            }
            data.close();

            //actions
            int actionId = 0;
            data = dal.sqlQuery(SQL_UIA);
            data.moveToFirst();
            while (!data.isAfterLast()) {
                UiInfo uiInfo = getUiInfo(data.getString(data.getColumnIndex(COL_UIA_UI)));
                String act = data.getString(data.getColumnIndex(COL_UIA_ACT));
                uiInfo.addAction(act,
                        new UiAction(actionId, act,
                                data.getString(data.getColumnIndex(COL_UIA_TITLE)),
                                data.getInt(data.getColumnIndex(COL_UIA_MENU)),
                                data.getInt(data.getColumnIndex(COL_UIA_DEF)) == 1,
                                data.getString(data.getColumnIndex(COL_UIA_HANDLER)),
                                data.getString(data.getColumnIndex(COL_UIA_PARAMS)),
                                data.getString(data.getColumnIndex(COL_UIA_CONDITION)),
                                data.getString(data.getColumnIndex(COL_UIA_OPTIONS)),
                                actionExec)

                );
                data.moveToNext();
                actionId++;
            }
            data.close();

            //fields
            data = dal.sqlQuery(SQL_UIF);
            data.moveToFirst();
            while (!data.isAfterLast()) {
                UiInfo uiInfo = getUiInfo(data.getString(data.getColumnIndex(COL_UIF_UI)));
                String fname = data.getString(data.getColumnIndex(COL_UIF_FIELD));
                UiField field = uiInfo.getField(fname);
                field.title = data.getString(data.getColumnIndex(COL_UIF_TITLE));

                field.visible = data.getInt(data.getColumnIndex(COL_UIF_VISIBLE)) == 1;
                field.band = data.getString(data.getColumnIndex(COL_UIF_BAND));
                field.editor = data.getInt(data.getColumnIndex(COL_UIF_EDITOR));
                field.fmt = data.getString(data.getColumnIndex(COL_UIF_FORMAT));
                field.setOptions(data.getString(data.getColumnIndex(COL_UIF_OPTIONS)));
                UiStyle fStyle = field.getStyle();
                fStyle.setTextSize(data.getInt(data.getColumnIndex(COL_UIF_TEXTSIZE)));
                fStyle.setTextStyle(data.getInt(data.getColumnIndex(COL_UIF_TEXTSTYLE)));
                fStyle.setAlignH(data.getInt(data.getColumnIndex(COL_UIF_ALIGNH)));
                fStyle.setColor(data.getInt(data.getColumnIndex(COL_UIF_COLOR)));

                data.moveToNext();
            }
            data.close();
        } catch (Exception e) {
            App.sendError(context, e.toString());
        }
    }

    public void reload() {
        load();
    }

    public boolean isUiExist(String ui) {
        return items.containsKey(ui);
    }

    public UiInfo getUiInfo(String ui) {
        UiInfo result = items.get(ui);
        if (result == null) {
            result = new UiInfo(ui);
            items.put(ui, result);
        }
        return result;
    }

    public static enum MenuKind {mkNone, mkOptions, mkContext}

    public interface UiFactory {
        Object startUi(String cls, Class uiClass, Context context, String ui, Bundle args, int options) throws Exception;
    }
}

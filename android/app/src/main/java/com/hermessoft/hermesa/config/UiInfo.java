package com.hermessoft.hermesa.config;

import android.content.Context;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.config.UiCatalog.MenuKind;
import com.hermessoft.hermesa.config.UiCatalog.UiFactory;
import com.hermessoft.hermesa.util.Utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class UiInfo {
    private static final String ACT_NEXT = "NEXT";

    public final String uiId;
    public final String cls;
    public final String title;
    private HashSet<String> uiArgs = new HashSet<String>();
    private HashMap<String, String> options = new HashMap<String, String>();
    private LinkedHashMap<String, UiAction> actions = new LinkedHashMap<String, UiAction>();
    private HashMap<String, UiField> fields = new HashMap<String, UiField>();

    public UiInfo(String uiId) {
        this.uiId = uiId;
        this.cls = "";
        this.title = "";
    }

    public UiInfo(String uiId, String cls, String title, String options, String args) {
        this.uiId = uiId;
        this.cls = cls;
        this.title = title;
        Utils.parseOptionsText(options, this.options);
        if (args != null) {
            String[] strArr = args.split(",");
            for (String p : strArr)
                uiArgs.add(p);
        }
    }

    public Object exec(Context context, Bundle args, int options) {
        Object result = null;
        Bundle startArgs = null;
        if (args != null) {
            startArgs = new Bundle();
            if (uiArgs.contains("*")) {
                for (String p : args.keySet()) {
                    startArgs.putString(p, args.getString(p));
                }
            } else {
                for (String p : uiArgs) {
                    if (args.containsKey(p)) {
                        startArgs.putString(p, args.getString(p));
                    }
                }
            }
        }

        UiFactory factory = UiCatalog.getUiFactory(cls);
        if (factory != null) {

            try {
                result = factory.startUi(cls, UiCatalog.getUiClass(cls),
                        context, uiId, startArgs, 0);

                //copy input args to output
                if (result instanceof Bundle) {
                    for (String key : startArgs.keySet()) {
                        if (!((Bundle) result).containsKey(key)) {
                            ((Bundle) result).putString(key, startArgs.getString(key));
                        }
                    }
                }

                UiAction nextAct = getAction(ACT_NEXT);
                if (nextAct != null) {
                    if (result instanceof Bundle) {
                        if (nextAct.checkCondition((Bundle) result)) {
                            result = nextAct.exec(context, (Bundle) result);
                        }
                    } else {
                        result = nextAct.exec(context, null);
                    }
                }

                if (result instanceof Bundle) {
                    for (UiAction act : getActions(ACT_NEXT)) {
                        if (act.checkCondition((Bundle) result)) {
                            result = act.exec(context, (Bundle) result);
                            break;
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                App.sendError(context, e.toString());
            }
        }

        return result;
    }

    public String getOption(String key) {
        String result = "";
        if (options.containsKey(key)) result = options.get(key);
        return result;
    }

    public void addAction(String Id, UiAction action) {
        actions.put(Id, action);
    }

    public UiAction getAction(String act) {
        return actions.get(act);
    }

    public UiAction getAction(int Id) {
        UiAction result = null;
        for (UiAction a : actions.values()) {
            if (a.Id == Id) {
                result = a;
                break;
            }
        }
        return result;
    }

    public List<UiAction> getActions(String act) {
        LinkedList<UiAction> result = new LinkedList<UiAction>();
        String actPref = act + '.';
        for (UiAction a : actions.values()) {
            if (a.act.startsWith(actPref))
                result.add(a);
        }
        return result;
    }

    public List<UiAction> getMenu() {
        LinkedList<UiAction> result = new LinkedList<UiAction>();
        for (UiAction a : actions.values()) {
            if (a.menuKind == MenuKind.mkOptions) {
                result.add(a);
            }
        }
        return result;
    }

    public List<UiAction> getContextMenu() {
        LinkedList<UiAction> result = new LinkedList<UiAction>();
        for (UiAction a : actions.values()) {
            if (a.menuKind == MenuKind.mkContext) {
                result.add(a);
            }
        }
        return result;
    }

    public UiAction getDefaultContextMenuItem(Bundle params) {
        UiAction result = null;
        for (UiAction a : getContextMenu()) {
            if (a.def && a.checkCondition(params)) {
                result = a;
                break;
            }
        }
        return result;
    }

    public void createContextMenu(Context context, ContextMenu menu, View v,
                                  ContextMenuInfo menuInfo, Bundle params) {

        for (UiAction a : getContextMenu()) {
            if (!a.checkCondition(params))
                continue;
            menu.add(0, a.Id, 0, a.title);
        }
    }

    public void createOptionsMenu(Menu menu) {
        for (UiAction a : getMenu()) {
            MenuItem mi = menu.add(0, a.Id, 0, a.title);
            if (a.def) {
                mi.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                if (a.ico_id != 0) {
                    mi.setIcon(a.ico_id);
                }
            }
        }
    }

    public void prepareOptionsMenu(Menu menu, Bundle params) {
        for (UiAction a : getMenu()) {
            MenuItem mi = menu.findItem(a.Id);
            if (mi != null) {
                mi.setVisible(a.checkCondition(params));
            }
        }
    }

    public UiField getField(String name) {
        UiField result = fields.get(name);
        if (result == null) {
            result = new UiField(uiId, name);
            fields.put(name, result);
        }
        return result;
    }
}

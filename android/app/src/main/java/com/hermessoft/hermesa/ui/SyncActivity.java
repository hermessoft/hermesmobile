package com.hermessoft.hermesa.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.data.SyncRunner;

public class SyncActivity extends Activity {

    private TextView tvLog;
    private ScrollView svLog;
    private Button btPull;
    private Button btPush;
    private App app;
    // handler for received Intents for the "my-event" event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SyncRunner.MSG_PROGRESS)) {
                tvLog.append(intent.getStringExtra(SyncRunner.MSG_EXTRA_MESSAGE));
                scrollLog();
            } else if (intent.getAction().equals(SyncRunner.MSG_START)) {
                setProgressBarIndeterminateVisibility(true);
                setButtonState(false);
            } else if (intent.getAction().equals(SyncRunner.MSG_FINISH)) {
                setProgressBarIndeterminateVisibility(false);
                setButtonState(true);
                Toast.makeText(context,
                        context.getString(R.string.sync_toast_finish),
                        Toast.LENGTH_SHORT).show();
            } else if (intent.getAction().equals(SyncRunner.MSG_ERROR)) {
                setProgressBarIndeterminateVisibility(false);
                setButtonState(true);
            }
        }
    };

    private void scrollLog() {
        svLog.post(new Runnable() {
            @Override
            public void run() {
                svLog.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void setButtonState(boolean enabled) {
        btPull.setEnabled(enabled);
        btPush.setEnabled(enabled);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.sync);
        app = (App) getApplication();

        svLog = (ScrollView) findViewById(R.id.scrollViewPullLog);
        tvLog = (TextView) findViewById(R.id.tvSyncPullLog);

        if (app.getSyncRunner().isRunning()) {
            tvLog.setText(app.getSyncRunner().getLog());
            setProgressBarIndeterminateVisibility(true);
        } else
            tvLog.setText(app.getSyncRunner().getInfo());

        btPull = (Button) findViewById(R.id.cmdPullStart);
        btPull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvLog.setText("");
                app.getSyncRunner().startPull();
            }
        });

        btPush = (Button) findViewById(R.id.cmdPushStart);
        btPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvLog.setText("");
                app.getSyncRunner().startPush();
            }
        });

        setButtonState(!app.getSyncRunner().isRunning());
        scrollLog();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Register mMessageReceiver to receive messages.
        IntentFilter filter = new IntentFilter();
        filter.addAction(SyncRunner.MSG_START);
        filter.addAction(SyncRunner.MSG_FINISH);
        filter.addAction(SyncRunner.MSG_ERROR);
        filter.addAction(SyncRunner.MSG_PROGRESS);
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, filter);
    }

    @Override
    protected void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sync, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // call activity.invalidateOptionsMenu() when need rebuild menu!!!
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.sync_action_setup:
                startActivity(new Intent(this, ConfigActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}

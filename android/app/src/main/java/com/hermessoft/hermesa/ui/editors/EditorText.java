package com.hermessoft.hermesa.ui.editors;

import android.content.Context;
import android.database.Cursor;
import android.widget.EditText;

import com.hermessoft.hermesa.config.UiField;
import com.hermessoft.hermesa.config.UiField.FieldValueBinder;

import java.util.HashMap;

public class EditorText extends EditText implements FieldValueBinder {

    private final String mFmt;

    public EditorText(Context context, UiField fieldInfo) {
        super(context);
        setSingleLine();
        mFmt = fieldInfo.fmt;
    }

    @Override
    public void setValue(String fieldName, Cursor cursor) {
        String val = cursor.getString(cursor.getColumnIndex(fieldName));
        setText(UiField.format(val, mFmt));
    }

    @Override
    public String getValue(String fieldName) {
        return getText().toString();
    }

    @Override
    public HashMap<String, String> getValues(String fieldName) {
        // TODO Auto-generated method stub
        return null;
    }

}

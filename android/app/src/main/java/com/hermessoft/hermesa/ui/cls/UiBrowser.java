package com.hermessoft.hermesa.ui.cls;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.webkit.HttpAuthHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiAction;
import com.hermessoft.hermesa.config.UiInfo;

import java.util.List;

public class UiBrowser extends UiBaseActivity {
    public static final String UI_CLS = "activity:browser";

    private WebView webView = null;
    private String url = "";
    private boolean clearHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.ui_browser);

        webView = (WebView) findViewById(R.id.webview);
        webView.setWebViewClient(new BrowserWebViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100) {
                    UiBrowser.this.setProgressBarIndeterminateVisibility(true);
                } else {
                    UiBrowser.this.setProgressBarIndeterminateVisibility(false);
                }
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        //webSettings.setJavaScriptEnabled(true);


        if (savedInstanceState == null) {
            loadDefParams();
            buildUrl();
            webView.loadUrl(url);
        } else
            webView.restoreState(savedInstanceState);
    }

    private void buildUrl() {
        url = "";
        if (getUiValues().containsKey("url")) {
            url = getUiValues().getString("url");
            getUiValues().remove("url");
        }
        if (url.equals("")) url = uiInfo.getOption("url");
        if (url.equals("")) url = uiId;

        url = "/" + url;

        if (!getUiValues().isEmpty()) {
            StringBuffer buffer = new StringBuffer();

            for (String k : getUiValues().keySet())
                buffer.append(k + "=" + getUiValues().getString(k) + "&"); //URLEncoder.encode("xxxxxxx"));

            if (buffer.length() != 0)
                url = url + "?" + buffer.toString();
        }

        url = app.getConfig().getHttpServer() + url;
    }

    private void loadDefParams() {
        UiAction act;
        act = uiInfo.getAction("DEF");
        if (act != null) {
            Object data = act.exec(this, getUiValues());
            if (data != null) {
                if (data instanceof Cursor) {
                    Cursor cur = (Cursor) data;
                    if (cur.moveToFirst()) {
                        for (int i = 0; i < cur.getColumnCount(); i++) {
                            getUiValues().putString(cur.getColumnName(i), cur.getString(i));
                        }
                    }
                    cur.close();
                } else if (data instanceof Bundle) {
                    getUiValues().putAll((Bundle) data);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onViewReload() {
        buildUrl();
        clearHistory = true;
        webView.loadUrl(url);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        webView.saveState(outState);
    }

    private class BrowserWebViewClient extends WebViewClient {
        @Override
        public void onReceivedHttpAuthRequest(WebView view,
                                              HttpAuthHandler handler, String host, String realm) {

            handler.proceed(app.getConfig().getDeviceID(), app.getConfig().getDeviceID());

        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            app.showToast(description);

            //  Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {

            UiBrowser.this.setTitle(view.getTitle());
            if (clearHistory) {
                clearHistory = false;
                webView.clearHistory();
            }
            super.onPageFinished(view, url);
        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            boolean result = false;

            List<String> url_seg = Uri.parse(url).getPathSegments();
            if (!url_seg.isEmpty()) {
                String uiId = url_seg.get(0);
                if (app.getUiCatalog().isUiExist(uiId)) {
                    UiInfo uiInfo = app.getUiCatalog().getUiInfo(uiId);
                    Bundle args = new Bundle();
                    for (String k : Uri.parse(url).getQueryParameterNames()) {
                        args.putString(k, Uri.parse(url).getQueryParameter(k));
                    }
                    uiInfo.exec(UiBrowser.this, args, 0);
                    result = true;
                }
            }

            return result;
            /*if(Uri.parse(url).getHost().endsWith("html5rocks.com")) {
                return false;
            }
             
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            view.getContext().startActivity(intent);
            return true;*/
        }
    }

}

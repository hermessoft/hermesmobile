package com.hermessoft.hermesa.ui.editors;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiAction;
import com.hermessoft.hermesa.config.UiField;
import com.hermessoft.hermesa.config.UiField.FieldValueBinder;
import com.hermessoft.hermesa.config.UiInfo;
import com.hermessoft.hermesa.data.EdsAdapterLookup;

import java.util.HashMap;

public class EditorLookup extends FrameLayout implements FieldValueBinder {

    private Spinner spinner;
    private EdsAdapterLookup adapter;

    public EditorLookup(Context context, UiField fieldInfo, Bundle args) throws Exception {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.editor_lookup, this);

        UiInfo uiInfo = ((App) context.getApplicationContext()).getUiCatalog().getUiInfo(fieldInfo.uiId);
        UiAction act = uiInfo.getAction("LOOKUP." + fieldInfo.name.toUpperCase());
        if (act != null) {
            Object data = act.exec(context, args);
            if (data != null && data instanceof Cursor) {
                adapter = new EdsAdapterLookup(context, act.handler, (Cursor) data);
                spinner = (Spinner) findViewById(R.id.spinner);
                spinner.setAdapter(adapter);
                spinner.setPrompt(act.title);
            }
        } else {
            throw new Exception(String.format("Action LOOKUP.%s not found", fieldInfo.name));
        }
    }

    public void setValue(String value) {
        int pos = adapter.getPositionById(Integer.valueOf(value));
        if (pos != -1)
            spinner.setSelection(pos);
    }


    public String getValue(String fieldname) {
        return String.valueOf(spinner.getSelectedItemId());
    }

    @Override
    public void setValue(String fieldName, Cursor cursor) {
        String val = cursor.getString(cursor.getColumnIndex(fieldName));
        int pos = adapter.getPositionById(Integer.valueOf(val));
        if (pos != -1)
            spinner.setSelection(pos);

    }

    @Override
    public HashMap<String, String> getValues(String fieldName) {
        // TODO Auto-generated method stub
        return null;
    }
}

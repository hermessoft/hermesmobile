package com.hermessoft.hermesa.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Compress {
    private static final int BUFFER = 2048;


    public static void zipDirectory(String dirPath, String zipFile) throws IOException {

        BufferedInputStream origin = null;
        FileOutputStream dest = new FileOutputStream(zipFile);
        ZipOutputStream out = new ZipOutputStream(
                new BufferedOutputStream(dest));
        byte data[] = new byte[BUFFER];

        File dir = new File(dirPath);
        if (dir.isDirectory()) {
            String[] files = dir.list();
            for (String fname : files) {
                FileInputStream fi = new FileInputStream(dirPath + "/" + fname);
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(fname);
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
                fi.close();
            }
        }
        out.close();
    }
}

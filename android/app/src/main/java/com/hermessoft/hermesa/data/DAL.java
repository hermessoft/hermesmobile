package com.hermessoft.hermesa.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;

import com.hermessoft.hermesa.config.Config;
import com.hermessoft.hermesa.util.Utils;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class DAL {
    private static final String DATABASE_NAME = "hermesa";

    private Context context;
    private Config conf;
    private DBHelper hdb;
    private SQLiteDatabase db;
    private String db_name;
    private HashMap<String, Eds> edsList = new HashMap<String, Eds>();

    public DAL(Context context, Config conf) {
        this.context = context;
        this.conf = conf;
    }

    private SQLiteDatabase getDB() throws Exception {
        if (db == null) {
            if (conf.getDatabaseID() == 0)
                throw new Exception("Application not regitered");
            db_name = DATABASE_NAME + "_"
                    + String.valueOf(conf.getDatabaseID()) + ".db";
            hdb = new DBHelper(context, db_name);
            db = hdb.getWritableDatabase();
        }
        return db;
    }

    public boolean isReady() {
        return (conf.getDatabaseVer() != 0);
    }

    public void beginTransaction() throws Exception {
        getDB().beginTransaction();
    }

    public void setTransactionSuccessful() throws Exception {
        getDB().setTransactionSuccessful();
    }

    public void endTransaction() throws Exception {
        getDB().endTransaction();
    }

    public void upgradeDB(int db_ver, String[] ddl)
            throws XmlPullParserException, IOException, Exception {
        if (db_ver < conf.getDatabaseVer())
            return;

        SQLiteDatabase db = getDB();
        db.beginTransaction();
        try {
            for (String item : ddl) {
                for (String stmt : Utils.xmlToList(item)) {
                    try {
                        db.execSQL(stmt);
                    } catch (SQLiteException exception) {
                        if (!exception.getMessage().toUpperCase().contains("DUPLICATE COLUMN NAME")) {
                            throw new SQLiteException(exception.getMessage());
                        }
                    }

                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        conf.setDatabaseVer(db_ver);
    }

    public void replace(String tabl, ContentValues values) throws Exception {
        getDB().replaceOrThrow(tabl, null, values);
    }

    public void erase(String tabl, ContentValues values) throws Exception {
        if (values.size() == 0)
            throw new Exception("Erase's values is empty");

        StringBuilder where = new StringBuilder();
        String[] args = new String[values.size()];
        for (String key : values.keySet()) {
            if (where.length() == 0)
                where.append(key + "=?");
            else
                where.append(" and " + key + "=?");
        }
        getDB().delete(tabl, where.toString(), args);
    }

    public void eraseAll(String tabl) throws Exception {
        SQLiteDatabase db = getDB();
        db.delete(tabl, null, null);
    }

    public void sqlExec(String sql) throws Exception {
        getDB().execSQL(sql);
    }

    public Cursor sqlQuery(String sql) throws Exception {
        return getDB().rawQuery(sql, null);
    }

    public Set<String> getTableFields(String table_name) throws Exception {
        Cursor cur = getDB().rawQuery("PRAGMA table_info(" + table_name + ")", null);
        cur.moveToFirst();

        HashSet<String> result = new HashSet<String>();
        do {
            result.add(cur.getString(1));
        } while (cur.moveToNext());
        cur.close();
        return result;
    }

    public void removeEds(String key) {
        edsList.remove(key);
    }

    public void addEds(String key, int act, String sql) throws Exception {
        Eds eds = new Eds(key, act, sql);
        edsList.put(key, eds);
    }

    private Eds getEds(String edsId) throws Exception {
        Eds result = edsList.get(edsId);
        if (result == null) {
            throw new Exception("EDS " + edsId + " not found");
        }
        return result;
    }

    public Bundle edsExec(String edsId, Bundle params) throws Exception {
        return getEds(edsId).exec(getDB(), params);
    }

    public Cursor edsQuery(String edsId, Bundle params) throws Exception {
        return getEds(edsId).query(getDB(), params);
    }

}

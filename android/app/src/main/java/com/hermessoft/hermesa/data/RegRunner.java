package com.hermessoft.hermesa.data;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.R;

import java.util.HashMap;
import java.util.Map;

public class RegRunner {

    public static final String MSG_START = "MSG_REG_START";
    public static final String MSG_FINISH = "MSG_REG_FINISH";
    public static final String MSG_ERROR = "MSG_REG_ERROR";
    public static final String MSG_PROGRESS = "MSG_REG_PROGRESS";
    public static final String MSG_EXTRA_MESSAGE = "MESSAGE";

    private static final String SYNC_PARAM_APP_VER = "app_ver";
    private static final String SYNC_PARAM_DEV_ID = "dev_id";
    private static final String SYNC_PARAM_USER = "usr";
    private static final String SYNC_PARAM_DB_ID = "db_id";
    private static final String RPC_REG_CHECK = "MFW.RegDeviceCheck";
    private static final String RPC_REG_SAVE = "MFW.RegDeviceSave";

    private Context context;
    private String log;
    private String last_error = "";
    private RegTask task;

    public RegRunner(Context context) {
        this.context = context;
    }

    private App getApp() {
        return (App) context.getApplicationContext();
    }

    private void progress(String msg) {
        log = log + "\n" + msg;
        Intent intent = new Intent(MSG_PROGRESS);
        intent.putExtra(MSG_EXTRA_MESSAGE, msg);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private void progress(int resId) {
        progress(context.getString(resId));
    }

    public boolean start() {
        if (task == null || task.getStatus().equals(AsyncTask.Status.FINISHED)) {
            task = new RegTask();
            task.execute();
            return true;
        }
        return false;
    }

    public void cancel() {

    }

    public boolean isRunning() {
        return (task != null && task.getStatus().equals(
                AsyncTask.Status.RUNNING));
    }

    public String getLog() {
        return log;
    }

    public String getInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nDEVICE ID:");
        sb.append("\n" + getApp().getConfig().getDeviceID());
        sb.append("\n");
        sb.append("\nDEVICE USER:");
        sb.append("\n" + getApp().getConfig().getDevUser());
        sb.append("\n");
        sb.append("\nSERVER:");
        sb.append("\n" + getApp().getConfig().getSyncServer());
        sb.append("\n");
        sb.append("\nSERVER USER:");
        sb.append("\n" + getApp().getConfig().getSyncUser());
        if (!last_error.isEmpty()) {
            sb.append("\n");
            sb.append("\nERROR:");
            sb.append("\n" + last_error);
        }
        return sb.toString();
    }

    class RegTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            log = "";
            LocalBroadcastManager.getInstance(context).sendBroadcast(
                    new Intent(MSG_START));
            progress(R.string.reg_log_start);

            RpcClient xmlrpc = new RpcClient(getApp().getConfig()
                    .getSyncServer(), getApp().getConfig().getSyncUser(),
                    getApp().getConfig().getSyncPassword());

            try {
                int db_id = 0;
                final HashMap<String, Object> rpc_params = new HashMap<String, Object>();
                rpc_params.put(SYNC_PARAM_DEV_ID, getApp().getConfig()
                        .getDeviceID());
                rpc_params.put(SYNC_PARAM_USER, getApp().getConfig()
                        .getDevUser());
                rpc_params.put(SYNC_PARAM_APP_VER, App.APP_VERSION);

                final Object[] data = (Object[]) xmlrpc.call(RPC_REG_CHECK,
                        rpc_params);
                if (data.length > 0) {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> response = (Map<String, Object>) data[0];
                    if (response.get("DB_ID") instanceof Integer) {
                        db_id = (Integer) response.get("DB_ID");
                        rpc_params.put(SYNC_PARAM_DB_ID, db_id);
                        xmlrpc.call(RPC_REG_SAVE, rpc_params);
                    }
                }

                if (db_id != 0) {
                    getApp().getConfig().setDatabaseID(db_id);
                    progress(R.string.reg_log_finish);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(
                            new Intent(MSG_FINISH));
                } else {
                    progress(R.string.reg_log_finish_error);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(
                            new Intent(MSG_ERROR));
                }
            } catch (Exception e) {
                e.printStackTrace();
                last_error = e.getMessage();
                progress(e.getMessage());
                LocalBroadcastManager.getInstance(context).sendBroadcast(
                        new Intent(MSG_ERROR));
            }
            return null;
        }
    }
}

package com.hermessoft.hermesa;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.hermessoft.hermesa.config.Config;
import com.hermessoft.hermesa.config.UiCatalog;
import com.hermessoft.hermesa.data.BarScan;
import com.hermessoft.hermesa.data.DAL;
import com.hermessoft.hermesa.data.Geo;
import com.hermessoft.hermesa.data.RegRunner;
import com.hermessoft.hermesa.data.SyncRunner;
import com.hermessoft.hermesa.ui.ViewBuilder;
import com.hermessoft.hermesa.ui.cls.UiBaseActivity;
import com.hermessoft.hermesa.ui.cls.UiBaseFragment;
import com.hermessoft.hermesa.ui.cls.UiBrowser;
import com.hermessoft.hermesa.ui.cls.UiDialogItem;
import com.hermessoft.hermesa.ui.cls.UiEds;
import com.hermessoft.hermesa.ui.cls.UiFragmentItem;
import com.hermessoft.hermesa.ui.cls.UiFragmentList;
import com.hermessoft.hermesa.ui.cls.UiGeo;
import com.hermessoft.hermesa.ui.cls.UiItem;
import com.hermessoft.hermesa.ui.cls.UiItemEx;
import com.hermessoft.hermesa.ui.cls.UiList;
import com.hermessoft.hermesa.ui.cls.UiPager;
import com.hermessoft.hermesa.ui.cls.UiPickList;
import com.hermessoft.hermesa.util.Compress;
import com.hermessoft.hermesa.util.ExceptionHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class App extends Application {

    public static final String NEW_APK_FILE = "hermesa.apk";
    public static final int APP_VERSION = 9;

    public static final String MSG_ERROR = "MSG_ERROR";
    public static final String MSG_ERROR_EXTRA_INFO = "ERROR_INFO";
    private static final String ERROR_DUMP_FOLDER = "errors";

    private Config config;
    private SyncRunner syncRunner;
    private RegRunner regRunner;
    private DAL dal;
    private ViewBuilder viewBuilder;
    private UiCatalog uiCatalog;
    private ExceptionHandler exceptionHandler;
    private Geo geo;
    private BarScan barscan;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(SyncRunner.MSG_APP_UPDATE)) {
                doAppUpdate(intent.getStringExtra(SyncRunner.MSG_EXTRA_MESSAGE));

            } else if (intent.getAction().equals(RegRunner.MSG_FINISH)) {
                Toast.makeText(getApplicationContext(),
                        R.string.reg_toast_finish, Toast.LENGTH_SHORT).show();

            } else if (intent.getAction().equals(SyncRunner.MSG_PULL_FINISH)) {
                getUiCatalog().reload();

            } else if (intent.getAction().equals(MSG_ERROR)) {
                String error_info = intent.getExtras().getString(
                        MSG_ERROR_EXTRA_INFO);
                App.this.catchError(error_info);

            }
        }
    };

    public static void sendError(Context context, String msg) {
        Intent intent = new Intent(MSG_ERROR);
        intent.putExtra(MSG_ERROR_EXTRA_INFO, msg);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        exceptionHandler = new ExceptionHandler(getErrorDumpPath());
        Thread.setDefaultUncaughtExceptionHandler(exceptionHandler);
        IntentFilter filter = new IntentFilter();
        filter.addAction(MSG_ERROR);
        filter.addAction(SyncRunner.MSG_APP_UPDATE);
        filter.addAction(RegRunner.MSG_FINISH);
        filter.addAction(SyncRunner.MSG_PULL_FINISH);
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, filter);
        registerUiFactory();
        getGeo().start();
        getBarScan().start();
    }

    private void doAppUpdate(String newApkFile) {
        File apkFile = new File(newApkFile);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(Uri.fromFile(apkFile),
                "application/vnd.android.package-archive");
        startActivity(intent);
    }

    public String getErrorDumpPath() {
        return getFilesDir() + "/" + ERROR_DUMP_FOLDER;
    }

    public boolean isErrorDumpPresent() {
        boolean result = false;
        File dumpdir = new File(getErrorDumpPath());
        if (dumpdir.isDirectory()) {
            String[] files = dumpdir.list();
            result = files != null && files.length > 0;
        }

        return result;
    }

    public void clearErrorDump() {
        File dumpdir = new File(getErrorDumpPath());
        if (dumpdir.isDirectory()) {
            String[] files = dumpdir.list();
            for (String fname : files) {
                File dump = new File(getErrorDumpPath(), fname);
                dump.delete();
            }
        }
    }

    public String getErrorDump() throws IOException {
        StringBuilder sb = new StringBuilder();
        File dumpdir = new File(getErrorDumpPath());
        if (dumpdir.isDirectory()) {
            String[] files = dumpdir.list();
            for (String fname : files) {
                File dump = new File(getErrorDumpPath(), fname);
                if (dump.isFile()) {
                    sb.append("\n");
                    sb.append("<<<<<  " + dump.getName() + "  >>>>>");
                    FileInputStream fis = new FileInputStream(dump);
                    BufferedReader bfr = new BufferedReader(
                            new InputStreamReader(fis));

                    String receiveString = "";
                    while ((receiveString = bfr.readLine()) != null) {
                        sb.append("\n");
                        sb.append(receiveString);
                    }
                    fis.close();
                }
            }
        }

        return sb.toString();
    }

    public String getErrorDumpZip() {
        String result = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/hermesa_errordump.zip";
        try {
            Compress.zipDirectory(getErrorDumpPath(), result);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
        }
        return result;
    }

    public void catchError(Exception e) {
        exceptionHandler.makeDump(Thread.currentThread(), e);
        Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
    }

    public void catchError(String message) {
        exceptionHandler.makeDump(message);
        showToast(message);
    }

    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public Config getConfig() {
        if (config == null) {
            config = new Config(this.getApplicationContext());
        }
        return config;
    }

    public DAL getDAL() {
        if (dal == null) {
            dal = new DAL(this.getApplicationContext(), getConfig());
        }
        return dal;
    }

    public Geo getGeo() {
        if (geo == null) {
            geo = new Geo(this.getApplicationContext(), getConfig());
        }
        return geo;
    }

    public BarScan getBarScan() {
        if (barscan == null) {
            barscan = new BarScan(this.getApplicationContext(), getConfig());
        }
        return barscan;
    }

    public boolean isRegistered() {
        return getConfig().getDatabaseID() != 0;
    }

    public boolean isRootAccess() {
        return getConfig().isRootAccess();
    }

    public RegRunner getRegRunner() {
        if (regRunner == null) {
            regRunner = new RegRunner(this);
        }
        return regRunner;
    }

    public SyncRunner getSyncRunner() {
        if (syncRunner == null) {
            syncRunner = new SyncRunner(this, getConfig(), getDAL());
        }
        return syncRunner;
    }

    public ViewBuilder getViewBuilder() {
        if (viewBuilder == null) {
            viewBuilder = new ViewBuilder(this);
        }
        return viewBuilder;
    }

    public UiCatalog getUiCatalog() {
        if (uiCatalog == null) {
            uiCatalog = new UiCatalog(this, getConfig(), getDAL());
        }
        return uiCatalog;
    }


    private void registerUiFactory() {
        UiCatalog.registerUiFactory(UiList.UI_CLS, UiBaseActivity.FACTORY, UiList.class);
        UiCatalog.registerUiFactory(UiPickList.UI_CLS, UiBaseActivity.FACTORY, UiPickList.class);
        UiCatalog.registerUiFactory(UiItem.UI_CLS, UiBaseActivity.FACTORY, UiItem.class);
        UiCatalog.registerUiFactory(UiDialogItem.UI_CLS, UiBaseActivity.FACTORY, UiDialogItem.class);
        UiCatalog.registerUiFactory(UiPager.UI_CLS, UiBaseActivity.FACTORY, UiPager.class);
        UiCatalog.registerUiFactory(UiItemEx.UI_CLS, UiBaseActivity.FACTORY, UiItemEx.class);
        UiCatalog.registerUiFactory(UiBrowser.UI_CLS, UiBaseActivity.FACTORY, UiBrowser.class);

        UiCatalog.registerUiFactory(UiFragmentItem.UI_CLS, UiBaseFragment.FACTORY, UiFragmentItem.class);
        UiCatalog.registerUiFactory(UiFragmentList.UI_CLS, UiBaseFragment.FACTORY, UiFragmentList.class);

        UiCatalog.registerUiFactory(UiEds.UI_CLS_EXEC, UiEds.FACTORY, null);
        UiCatalog.registerUiFactory(UiEds.UI_CLS_QUERY, UiEds.FACTORY, null);

        UiCatalog.registerUiFactory(UiGeo.UI_CLS, UiGeo.FACTORY, null);
    }
}

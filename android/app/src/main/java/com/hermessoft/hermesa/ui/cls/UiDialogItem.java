package com.hermessoft.hermesa.ui.cls;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiAction;
import com.hermessoft.hermesa.ui.ViewBuilder;

import java.util.HashMap;

public class UiDialogItem extends UiBaseActivity {

    public static final String UI_CLS = "activity:dialog";
    private ViewGroup rootView = null;
    private HashMap<String, String> outs = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (((String) getTitle()).isEmpty()) requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.ui_dialog_item);

        ((Button) findViewById(R.id.btnSave))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onSave();
                    }
                });

        ((Button) findViewById(R.id.btnCancel))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        rootView = (ViewGroup) findViewById(R.id.data);

        fillData();
    }

    private void fillData() {
        try {
            Cursor cursor = app.getDAL().edsQuery(uiInfo.uiId, getUiValues());
            if (cursor.moveToFirst()) {

                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    outs.put(cursor.getColumnName(i), cursor.getString(i));
                }

                rootView.addView(app.getViewBuilder().buildCard(this, rootView, uiInfo.uiId,
                        cursor, getUiValues()));
            }

        } catch (Exception e) {
            app.catchError(e);
        }

    }

    private void onSave() {
        UiAction a = uiInfo.getAction("SAVE");
        if (a != null) {
            Bundle args = new Bundle();
            args.putAll(getUiValues());


            ViewBuilder.getViewValues(rootView, outs);
            for (String key : outs.keySet()) {
                args.putString("data_" + key, outs.get(key));
            }
            a.exec(this, args);
        }
        finish();
    }


}

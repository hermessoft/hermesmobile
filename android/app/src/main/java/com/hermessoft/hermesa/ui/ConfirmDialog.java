package com.hermessoft.hermesa.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.hermessoft.hermesa.R;

public class ConfirmDialog extends DialogFragment implements DialogInterface.OnClickListener {
    private static final String ARG_MESSAGE = "message";
    private static final String ARG_CALLER = "caller";
    private static final String ARG_ACTION = "action";

    public static ConfirmDialog newInstance(String callerTag, String action, String message) {
        ConfirmDialog result = new ConfirmDialog();
        Bundle args = new Bundle();
        args.putString(ARG_CALLER, callerTag);
        args.putString(ARG_ACTION, action);
        args.putString(ARG_MESSAGE, message);
        result.setArguments(args);
        return result;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                // .setTitle(R.string.confirm)
                .setPositiveButton(R.string.yes, this)
                .setNegativeButton(R.string.no, this)
                //.setNeutralButton(R.string.cancel, this)
                .setMessage(getArguments().getString(ARG_MESSAGE));
        return adb.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        OnConfirmDoneListener listener = (OnConfirmDoneListener) getActivity();
        if (which == Dialog.BUTTON_POSITIVE) {
            listener.onConfirmDone(getArguments().getString(ARG_CALLER), getArguments().getString(ARG_ACTION));
        }
    }

    public interface OnConfirmDoneListener {
        public void onConfirmDone(String callerTag, String action);
    }
}

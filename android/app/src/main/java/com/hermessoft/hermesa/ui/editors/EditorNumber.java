package com.hermessoft.hermesa.ui.editors;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiField;
import com.hermessoft.hermesa.config.UiField.FieldValueBinder;

import java.util.HashMap;

public class EditorNumber extends FrameLayout implements FieldValueBinder {

    private EditText viewValue;

    public EditorNumber(Context context, UiField fieldInfo) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.editor_number, this);
        viewValue = (EditText) findViewById(R.id.viewValue);

        viewValue.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                if (viewValue.getText().toString().equals("0")) viewValue.setText("");
                viewValue.setSelection(viewValue.getText().length());

            }
        });

        ((Button) findViewById(R.id.btnDec))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        changeValue(false);

                    }
                });

        ((Button) findViewById(R.id.btnInc))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        changeValue(true);

                    }
                });
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private void changeValue(boolean doIncrement) {
        String strValue = viewValue.getText().toString();
        double val = 0;
        if (strValue != null && !strValue.equals("") && isNumeric(strValue))
            val = Double.valueOf(strValue);

        if (doIncrement) {
            val++;
        } else {
            val--;
        }

        if (val < 0)
            val = 0;

        viewValue.setText(String.valueOf(val));
        viewValue.setSelection(viewValue.getText().length());
    }

    @Override
    public void setValue(String fieldName, Cursor cursor) {
        String val = cursor.getString(cursor.getColumnIndex(fieldName));
        viewValue.setText(val);
    }

    @Override
    public String getValue(String fieldName) {
        return viewValue.getText().toString();
    }

    @Override
    public HashMap<String, String> getValues(String fieldName) {
        // TODO Auto-generated method stub
        return null;
    }
}

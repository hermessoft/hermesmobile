package com.hermessoft.hermesa.ui;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.data.Eds;
import com.hermessoft.hermesa.data.EdsLoader;

public class MainActivity extends FragmentActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final String EDS_NAV = "MainActivity";
    private static final String SQL_NAV = "select ui _id, title from conf_ui where options like '%MainNav%' order by title";
    private static final String COL_NAV_TITLE = "title";
    private static long back_pressed;
    private App app;
    private CursorAdapter mAdapter;
    private ListView mNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        app = (App) getApplication();
        mNav = (ListView) findViewById(R.id.main_nav);

        mNav.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Cursor cursor = (Cursor) mAdapter.getItem(position);
                String nav_uiId = cursor.getString(cursor.getColumnIndex("_id"));
                app.getUiCatalog().getUiInfo(nav_uiId)
                        .exec(MainActivity.this, null, 0);
            }

        });

        try {
            app.getDAL().addEds(EDS_NAV, Eds.ACT_QUERY, SQL_NAV);
        } catch (Exception e) {
            app.catchError(e);
        }

        fillNav();
    }

    public void fillNav() {

        if (!app.getDAL().isReady())
            return;

        if (mAdapter == null) {
            getSupportLoaderManager().initLoader(0, null, this);
            try {
                mAdapter = new NavAdapter(this);
                mNav.setAdapter(mAdapter);
            } catch (Exception e) {
                app.catchError(e);
            }
        } else
            getSupportLoaderManager().getLoader(0).forceLoad();
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis())
            super.onBackPressed();
        else
            Toast.makeText(getBaseContext(), R.string.main_exit_confirm,
                    Toast.LENGTH_SHORT).show();
        back_pressed = System.currentTimeMillis();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // call activity.invalidateOptionsMenu() when need rebuild menu!!!
        menu.findItem(R.id.main_action_edump).setVisible(
                app.isErrorDumpPresent());
        boolean isRegistered = app.isRegistered();
        menu.findItem(R.id.main_action_reg).setVisible(!isRegistered);
        menu.findItem(R.id.main_action_sync).setVisible(isRegistered);
        menu.findItem(R.id.main_action_pref).setVisible(isRegistered);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.main_action_conf:
                startActivity(new Intent(this, ConfigActivity.class));
                break;
            case R.id.main_action_pref:
                // startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.main_action_sync:
                startActivity(new Intent(this, SyncActivity.class));
                break;
            case R.id.main_action_reg:
                startActivity(new Intent(this, RegActivity.class));
                break;
            case R.id.main_action_exit:
                finish();
                break;
            case R.id.main_action_edump:
                startActivity(new Intent(this, ErrorDumpActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        invalidateOptionsMenu();
        fillNav();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bandle) {
        return new EdsLoader(this, EDS_NAV, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        try {
            mAdapter.swapCursor(cursor);
        } catch (Exception e) {
            app.catchError(e);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    private class NavAdapter extends CursorAdapter {

        private LayoutInflater inflater;

        public NavAdapter(Context context) {
            super(context, null, true);
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView tvTitle = (TextView) view
                    .findViewById(/* R.id.main_navi_title */android.R.id.text1);
            tvTitle.setText(cursor.getString(cursor
                    .getColumnIndex(COL_NAV_TITLE)));
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View result = inflater.inflate(
                    /* R.layout.main_nav */android.R.layout.simple_list_item_1,
                    parent, false);
            return result;
        }
    }


}

package com.hermessoft.hermesa.data;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import com.hermessoft.hermesa.App;

public class EdsAdapterLookup extends CursorAdapter {
    private App app;
    private String uiId;

    public EdsAdapterLookup(Context context, String uiId, Cursor cursor) {
        super(context, null, true);
        app = (App) context.getApplicationContext();
        this.uiId = uiId;
        this.changeCursor(cursor);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        app.getViewBuilder().bindListRow(view, context, uiId, cursor);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        View result = null;
        try {
            result = app.getViewBuilder().buildListRow(context, parent, uiId, cursor);
        } catch (Exception e) {
            app.catchError(e);
        }
        return result;
    }

    @Override
    public void changeCursor(Cursor cursor) {
        super.changeCursor(cursor);
    }

    public int getPositionById(long id) {
        final int count = getCount();

        for (int pos = 0; pos < count; pos++) {
            if (id == getItemId(pos)) {
                return pos;
            }
        }
        return -1;
    }

}

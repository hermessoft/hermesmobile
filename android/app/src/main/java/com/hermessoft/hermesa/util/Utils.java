package com.hermessoft.hermesa.util;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Utils {

    public static List<String> xmlToList(String xml)
            throws XmlPullParserException, IOException {

        List<String> result = new ArrayList<String>();
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(new StringReader(xml));
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.TEXT) {
                result.add(xpp.getText());
            }
            eventType = xpp.next();
        }
        return result;
    }
/*
    public static Bundle assignParams(Set<String> params, Bundle source,
	    Map<String, String> mapRule) {
	Bundle result = new Bundle();
	if (source == null) return result;
	
	String pv;
	String val;
	for (String p : params) {

	    if (mapRule != null && mapRule.containsKey(p)) {
		pv = mapRule.get(p);
	    } else {
		pv = p;
	    }

	    if (source.containsKey(pv)) {
		val = source.getString(pv);
	    } else {
		val = "";
	    }

	    result.putString(p, val);
	}
	return result;
    }*/

    public static void parseParamsText(String source, Map<String, String> result) {
        if (source != null && !source.isEmpty()) {
            String[] terms = source.split(";");
            for (String term : terms) {
                String[] pair = term.split("=");
                if (pair.length == 2)
                    result.put(pair[0], pair[1]);
            }
        }
    }

    public static void parseOptionsText(String source, Map<String, String> result) {
        if (source != null && !source.isEmpty()) {
            String[] terms = source.split(";");
            for (String term : terms) {
                String[] pair = term.split("=");
                if (pair.length == 2)
                    result.put(pair[0], pair[1]);
                else
                    result.put(pair[0], "1");
            }
        }
    }


}

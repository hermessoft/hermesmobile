package com.hermessoft.hermesa.config;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiCatalog.MenuKind;
import com.hermessoft.hermesa.util.Utils;

import java.util.HashMap;

public class UiAction {
    public final int Id;
    public final String act;
    public final String title;
    public final MenuKind menuKind;
    public final boolean def;
    public final int ico_id;
    public String handler;
    private HashMap<String, String> mapParams = new HashMap<String, String>();
    private HashMap<String, String> mapCondition = new HashMap<String, String>();
    private HashMap<String, String> mapOptions = new HashMap<String, String>();
    private ExecuteCallback execCallback;

    public UiAction(int Id, String act, String title, int menu, boolean def,
                    String handler, String params, String condition, String options, ExecuteCallback execCallback) {
        this.Id = Id;
        this.act = act;
        this.title = title;
        this.def = def;

        switch (menu) {
            case 1:
                menuKind = MenuKind.mkOptions;
                break;
            case 2:
                menuKind = MenuKind.mkContext;
                break;
            default:
                menuKind = MenuKind.mkNone;
                break;
        }

        if (this.act.equals("NEW"))
            ico_id = R.drawable.ic_new;
        else if (this.act.equals("EDIT"))
            ico_id = R.drawable.ic_edit;
        else
            ico_id = 0;

        this.handler = handler;

        Utils.parseParamsText(params, this.mapParams);
        Utils.parseParamsText(condition, this.mapCondition);
        Utils.parseOptionsText(options, this.mapOptions);

        this.execCallback = execCallback;
    }

    private Bundle assignParams(Bundle source) {
        Bundle result = new Bundle();
        if (source == null)
            return result;

        String pv;
        String val;
        for (String p : mapParams.keySet()) {
            pv = mapParams.get(p);

            if (pv.startsWith("[") && pv.endsWith("]")) {
                val = (pv.length() > 2) ? pv.substring(1, pv.length() - 1) : "";
            } else {
                val = (source.containsKey(pv)) ? source.getString(pv) : "";
            }
            result.putString(p, val);
        }
        return result;
    }

    public Object exec(Context context, Bundle args) {
        Log.d("MY", "ACTION EXEC" + "\n action: " + act + "\n handler: " + handler + "\n params: " + args.toString());
        Bundle startArgs = assignParams(args);
        Log.d("MY", "\n start params: " + startArgs.toString());

        int options = 0;
        if (checkOption(UiCatalog.ACTION_OPTION_VIEWCLOSE)) {
            options = Intent.FLAG_ACTIVITY_CLEAR_TOP;
        }

        String current_handler = handler;
        if (current_handler.equals("") && startArgs.containsKey("handler")) {
            current_handler = startArgs.getString("handler");
            startArgs.remove("handler");
        }
        Object result = null;
        if (!current_handler.equals("")) {
            result = execCallback.exec(current_handler, context, startArgs, options);
        } else {
            result = new Bundle(startArgs);
        }
        return result;
    }

    public boolean checkCondition(Bundle args) {
        boolean result = true;
        String pv;  //val for check
        String av; //arg value

        for (String p : mapCondition.keySet()) {

            if (!args.containsKey(p)) {
                result = false;
                break;
            }

            av = args.getString(p);
            if (av == null) av = "";

            pv = mapCondition.get(p);

            if (pv.equals("Empty")) {

                result = av.isEmpty();

            } else if (pv.equals("NotEmpty")) {

                result = !av.isEmpty();

            } else if (pv.startsWith("[") && pv.endsWith("]")) {

                pv = (pv.length() > 2) ? pv.substring(1, pv.length() - 1) : "";

                String[] vals = pv.split(",");
                result = false;
                for (String v : vals)
                    if (v.equals(av)) {
                        result = true;
                        break;
                    }

            } else {
                pv = (args.containsKey(pv)) ? args.getString(pv) : pv;
                result = av.equals(pv);
            }

            if (!result) break;
        }
        return result;
    }

    public boolean checkOption(String option) {
        return mapOptions.containsKey(option);
    }

    public interface ExecuteCallback {
        public Object exec(String handler, Context context, Bundle args, int options);
    }


    public interface OnActionRequestListener {
        public void onActionRequest(String callerTag, String action);
    }

    public interface OnActionExecuteListener {
        public void onActionExecute(String action);
    }
}

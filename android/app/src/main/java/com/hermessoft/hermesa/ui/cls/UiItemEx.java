package com.hermessoft.hermesa.ui.cls;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiAction;

public class UiItemEx extends UiBaseActivity {

    public static final String UI_CLS = "activity:itemex";

    FragmentTransaction fTrans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_itemex);

        fillData();
    }

    private void fillData() {
        UiAction act = uiInfo.getAction("HEAD");
        if (act != null) {
            fTrans = this.getSupportFragmentManager().beginTransaction();
            Fragment head = (Fragment) act.exec(this, getUiValues());
            fTrans.replace(R.id.head, head, "HEAD");
            fTrans.commit();
        }

        act = uiInfo.getAction("DETAIL");
        if (act != null) {
            if (!act.title.startsWith("-")) {
                TextView detailTitle = (TextView) findViewById(R.id.detailTitle);
                detailTitle.setText(act.title);
                detailTitle.setVisibility(View.VISIBLE);
            }

            fTrans = this.getSupportFragmentManager().beginTransaction();
            Fragment head = (Fragment) act.exec(this, getUiValues());
            fTrans.replace(R.id.detail, head, "DETAIL");
            fTrans.commit();
        }

    }

}

package com.hermessoft.hermesa.ui.editors;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.config.UiAction;
import com.hermessoft.hermesa.config.UiField;
import com.hermessoft.hermesa.config.UiField.FieldValueBinder;
import com.hermessoft.hermesa.config.UiInfo;
import com.hermessoft.hermesa.data.EdsAdapterLookup;

import java.util.HashMap;

public class EditorQty extends FrameLayout implements FieldValueBinder {

    private static final String UNIT_SUFFIX = "_unt";

    private EditText viewValue;
    private EdsAdapterLookup adapterUnit;
    private Spinner spinnerUnit;
    private boolean useUnit = false;

    public EditorQty(Context context, UiField fieldInfo, Bundle args) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.editor_qty, this);
        viewValue = (EditText) findViewById(R.id.viewValue);

        viewValue.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                if (viewValue.getText().toString().equals("0")) viewValue.setText("");
                viewValue.setSelection(viewValue.getText().length());

            }
        });

        ((Button) findViewById(R.id.btnDec))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        changeValue(false);

                    }
                });

        ((Button) findViewById(R.id.btnInc))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        changeValue(true);

                    }
                });

        spinnerUnit = (Spinner) findViewById(R.id.spnUnit);
        UiInfo uiInfo = ((App) context.getApplicationContext()).getUiCatalog().getUiInfo(fieldInfo.uiId);
        UiAction act = uiInfo.getAction(fieldInfo.name.toUpperCase() + ".UNITS");
        if (act != null) {
            Object data = act.exec(context, args);
            if (data != null && data instanceof Cursor) {
                adapterUnit = new EdsAdapterLookup(context, act.handler, (Cursor) data);
                spinnerUnit.setAdapter(adapterUnit);
            }
        } else {
            spinnerUnit.setVisibility(GONE);
        }
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private void changeValue(boolean doIncrement) {
        String strValue = viewValue.getText().toString();
        int intValue = 0;
        if (strValue != null && !strValue.equals("") && isNumeric(strValue))
            intValue = Integer.valueOf(strValue);

        if (doIncrement) {
            intValue++;
        } else {
            intValue--;
        }

        if (intValue < 0)
            intValue = 0;

        viewValue.setText(String.valueOf(intValue));
        viewValue.setSelection(viewValue.getText().length());
    }


    public String getValue() {
        return viewValue.getText().toString();
    }

    public String getValueUnit() {
        return String.valueOf(spinnerUnit.getSelectedItemId());
    }

    public void setValueUnit(int value) {
        int pos = adapterUnit.getPositionById(Integer.valueOf(value));
        if (pos != -1)
            spinnerUnit.setSelection(pos);
    }

    @Override
    public void setValue(String fieldName, Cursor cursor) {
        String val = cursor.getString(cursor.getColumnIndex(fieldName));
        viewValue.setText(val);

        //unit
        String fieldNameUnit = fieldName + UNIT_SUFFIX;
        if (cursor.getColumnIndex(fieldNameUnit) != -1) {
            useUnit = true;
            int valUnit = cursor.getInt(cursor.getColumnIndex(fieldNameUnit));
            int pos = adapterUnit.getPositionById(valUnit);
            if (pos != -1) spinnerUnit.setSelection(pos);
        }

    }

    @Override
    public String getValue(String fieldName) {
        return null;
    }

    @Override
    public HashMap<String, String> getValues(String fieldName) {
        HashMap<String, String> result = new HashMap<String, String>();
        result.put(fieldName, viewValue.getText().toString());
        if (useUnit)
            result.put(fieldName + UNIT_SUFFIX, String.valueOf(spinnerUnit.getSelectedItemId()));
        return result;
    }
}


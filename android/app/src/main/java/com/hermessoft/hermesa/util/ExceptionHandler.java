package com.hermessoft.hermesa.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

    private final DateFormat formatter = new SimpleDateFormat("dd.MM.yy HH:mm");
    private final DateFormat fileFormatter = new SimpleDateFormat("dd-MM-yy");
    private final String dumpPath;
    private final Thread.UncaughtExceptionHandler previousHandler;
    private String versionName = "0";
    private int versionCode = 0;

    public ExceptionHandler(String dumpPath) {
        this.dumpPath = dumpPath;
        previousHandler = Thread.getDefaultUncaughtExceptionHandler();
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        makeDump(thread, throwable);

        if (previousHandler != null)
            previousHandler.uncaughtException(thread, throwable);
    }

    public void makeDump(String message) {
        final Date dumpDate = new Date(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append("\n")
                .append("********  " + formatter.format(dumpDate) + "  ********")
                .append("\n")
                .append(String.format("Version: %s (%d)\n", versionName,
                        versionCode))
                .append("\n")
                .append(message);
        saveDump(sb.toString());
    }

    public void makeDump(Thread thread, Throwable exception) {
        final Date dumpDate = new Date(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append("\n")
                .append("********  " + formatter.format(dumpDate) + "  ********")
                .append("\n")
                .append(String.format("Version: %s (%d)\n", versionName,
                        versionCode))
                .append(thread.toString())
                .append("\n");
        processThrowable(exception, sb);
        saveDump(sb.toString());
    }

    private void saveDump(String txt) {
        final Date dumpDate = new Date(System.currentTimeMillis());
        File dump = new File(dumpPath, String.format("errortrace-%s.txt",
                fileFormatter.format(dumpDate)));
        File dumpdir = dump.getParentFile();

        if (dumpdir.isDirectory() || dumpdir.mkdirs()) {

            FileWriter writer = null;
            try {
                writer = new FileWriter(dump, true);
                writer.write(txt);
            } catch (IOException e) {
                // ignore
            } finally {
                try {
                    if (writer != null)
                        writer.close();
                } catch (IOException e) {
                    // ignore
                }
            }

        }

    }

    private void processThrowable(Throwable exception, StringBuilder builder) {
        if (exception == null)
            return;
        StackTraceElement[] stackTraceElements = exception.getStackTrace();
        builder.append("Exception: ").append(exception.getClass().getName())
                .append("\n").append("Message: ")
                .append(exception.getMessage()).append("\nStacktrace:\n");
        for (StackTraceElement element : stackTraceElements) {
            builder.append("\t").append(element.toString()).append("\n");
        }
        processThrowable(exception.getCause(), builder);
    }
}


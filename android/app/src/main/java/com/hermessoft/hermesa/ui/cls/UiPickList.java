package com.hermessoft.hermesa.ui.cls;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.data.EdsAdapter;
import com.hermessoft.hermesa.data.EdsLoader;

public class UiPickList extends UiBaseActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String UI_CLS = "activity:picklist";
    ListView lvMain;
    EdsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (((String) getTitle()).isEmpty())
            requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.ui_picklist);


        lvMain = (ListView) findViewById(R.id.data);
        lvMain.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getUiValues().putString("item_id",
                        String.valueOf(adapter.getItemId(position)));
                Cursor cur = adapter.getCursor();
                for (int i = 0; i < cur.getColumnCount(); i++) {
                    String col = cur.getColumnName(i);
                    if (!col.equals("_id"))
                        getUiValues().putString("item_" + col, cur.getString(i));
                }

                finish();
            }
        });
        fillData();

    }

    private void fillData() {
        try {
            adapter = new EdsAdapter(this, uiInfo.uiId);
            lvMain.setAdapter(adapter);
            getSupportLoaderManager().initLoader(0, null, this);
        } catch (Exception e) {
            app.catchError(e);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Loader<Cursor> result = null;
        try {
            result = new EdsLoader(this, uiInfo.uiId,
                    this.getUiValues());
        } catch (Exception e) {
            app.catchError(e);
        }
        return result;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
        try {
            // adapter.swapCursor(cursor);
            adapter.changeCursor(cursor);
        } catch (Exception e) {
            app.catchError(e);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursor) {
        adapter.changeCursor(null);
    }


}

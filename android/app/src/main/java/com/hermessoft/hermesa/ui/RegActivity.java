package com.hermessoft.hermesa.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hermessoft.hermesa.App;
import com.hermessoft.hermesa.R;
import com.hermessoft.hermesa.data.RegRunner;

public class RegActivity extends Activity {

    private TextView tvLog;
    private ScrollView svLog;
    private Button btReg;
    private App app;
    // handler for received Intents for the "my-event" event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(RegRunner.MSG_PROGRESS)) {
                tvLog.append("\n"
                        + intent.getStringExtra(RegRunner.MSG_EXTRA_MESSAGE));
                scrollLog();
            } else if (intent.getAction().equals(RegRunner.MSG_START)) {
                setProgressBarIndeterminateVisibility(true);
                btReg.setEnabled(false);
            } else if (intent.getAction().equals(RegRunner.MSG_FINISH)) {
                setProgressBarIndeterminateVisibility(false);
                finish();
            } else if (intent.getAction().equals(RegRunner.MSG_ERROR)) {
                setProgressBarIndeterminateVisibility(false);
                btReg.setEnabled(true);
            }
        }
    };

    private void scrollLog() {
        svLog.post(new Runnable() {
            @Override
            public void run() {
                svLog.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.reg);
        app = (App) getApplication();

        svLog = (ScrollView) findViewById(R.id.scrollViewRegLog);
        tvLog = (TextView) findViewById(R.id.tvRegLog);

        if (app.getRegRunner().isRunning()) {
            tvLog.setText(app.getRegRunner().getLog());
            setProgressBarIndeterminateVisibility(true);
        } else
            tvLog.setText(app.getRegRunner().getInfo());

        btReg = (Button) findViewById(R.id.btRegitration);
        btReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvLog.setText("");
                app.getRegRunner().start();
            }
        });

        btReg.setEnabled(!app.getRegRunner().isRunning());
        scrollLog();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Register mMessageReceiver to receive messages.
        IntentFilter filter = new IntentFilter();
        filter.addAction(RegRunner.MSG_START);
        filter.addAction(RegRunner.MSG_FINISH);
        filter.addAction(RegRunner.MSG_ERROR);
        filter.addAction(RegRunner.MSG_PROGRESS);
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, filter);
    }

    @Override
    protected void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);
        super.onPause();
    }
}

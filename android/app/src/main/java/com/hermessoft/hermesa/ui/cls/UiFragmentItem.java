package com.hermessoft.hermesa.ui.cls;

import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hermessoft.hermesa.R;

public class UiFragmentItem extends UiBaseFragment {

    public static final String UI_CLS = "fragment:item";
    private ViewGroup rootView = null;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rootView = (ViewGroup) getView().findViewById(R.id.RootView);
        fillData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.ui_fragment_item, null);

    }

    private void fillData() {
        rootView.removeAllViews();
        try {
            Cursor cursor = app.getDAL().edsQuery(uiInfo.uiId, getUiValues());
            if (cursor.moveToFirst()) {

                rootView.addView(app.getViewBuilder().buildCard(this.getActivity(), rootView,
                        uiInfo.uiId, cursor, getUiValues()));
            }

        } catch (Exception e) {
            app.catchError(e);
        }
    }

    @Override
    protected void onFragmentReload() {
        fillData();
    }
}

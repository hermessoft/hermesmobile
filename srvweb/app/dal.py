#!/usr/bin/env python
#-*- coding: utf-8 -*-"
import fdb as fb
import dalutils
import datetime
import time
import config

META_SQL = 'select sql from mfw_web_ui where ui = ?'  
sqlformater = dalutils.SQLParams('named', 'qmark')

con = None
devices = {}
ui_cache = {}

####### fix fdb bug: non ascii simbols in exception message #################
def fix_exception_from_status(error, status, preamble=None):
    
    import ctypes
    #from fdb import ibase, fbcore
   
    msglist = []
    msg = ctypes.create_string_buffer(512)

    if preamble:
        msglist.append(preamble)
    sqlcode = fb.fbcore.api.isc_sqlcode(status)
    error_code = status[1]
    msglist.append('- SQLCODE: %i' % sqlcode)
    
    pvector = ctypes.cast(ctypes.addressof(status), fb.ibase.ISC_STATUS_PTR)
    while True:
        result = fb.fbcore.api.fb_interpret(msg, 512, pvector)
        if result != 0:
            if fb.fbcore.PYTHON_MAJOR_VER == 3:
                msglist.append('- ' + (msg.value).decode('utf_8'))
            else:    
                msglist.append('- ' + msg.value)
        else:
            break
     
    message = '\n'.join(msglist)
    if config.DAL_CHARSET:
        encoding = fb.ibase.charset_map[config.DAL_CHARSET.upper()]    
        message = message.decode(encoding).encode('utf_8', 'replace')

    return error(message)        

fb.fbcore.exception_from_status = fix_exception_from_status 
############################################################


def _exec_sql(sql, sql_params):      
    cur = _get_connection().cursor()    
    if sql_params:
        cur.execute(sql, sql_params)
    else:
        cur.execute(sql)
        
    con.commit(True) 
    if sql.lower().startswith('select'):
        return cur

def _exec_sql2(sql, params):
    if not sql:
        raise Exception('Empty sql')

    sql_params = None    
    if params:
        sql_parsed, sql_params = sqlformater.format(sql, params)          
    else:
        sql_parsed = sql
    return _exec_sql(sql_parsed, sql_params)
     

def _get_sql(ui, ver):
    data = _exec_sql('select sql from mfw_web_ui where ui = ? and ver = ?', [ui, ver]).fetchone()
    if not data:
        raise Exception('SQL for uri: %s %s' % [uri])        

    return data[0]
    

def _get_connection():
    global con
    if not con:
        con = fb.connect(dsn=config.DAL_DSN, user=config.DAL_USER, password=config.DAL_PASSWORD, 
            role=config.DAL_ROLE, charset=config.DAL_CHARSET)
    return con

def _fetchonemap(sql, params):
    result = _exec_sql(sql, params).fetchonemap()
    if not result:
        result = {}
    return result    
    
def login(dev_id):
    global devices
    if not dev_id in devices:
        data = _fetchonemap('select * from mfw_web_login(?)', [dev_id])         
        if data:
            devices[dev_id] = {}
            devices[dev_id].update(data)
        else:
            raise Exception('Device %s not found' % dev_id)
            
    return devices[dev_id]

def get_ui_info(ui, ver):
    global ui_cache
    ui_key = str(ui) + str(ver)
    if ui_key in ui_cache:
        result = ui_cache[ui_key]
    else:    
        result = UiInfo(ui, ver)
    if not config.DEBUG:
        ui_cache[ui_key] = result

    return result    

def get_ui_data(ui, ver, params):
    return _exec_sql2(_get_sql(ui, ver), params).fetchallmap()

class UiInfo():
    def __init__(self, ui, ver):
        self.ui = ui
        self.ver = ver        
          
        #load props 
        props = _exec_sql('select * from mfw_web_ui where ver = ? and ui = ?', [self.ver, self.ui]).fetchonemap()
        if not props:
            raise Exception('Page not found %s' % ui)
        self.title = props['TITLE']
        self.cls = props['CLS']
        self.options = parseOptionsText(props['OPTIONS'])

        self.hvisible = checkOption(self.options.get('hvisible'))
        self.band = self.options.get('band')

        self.act = props['ACT'] if props['ACT'] else ''
        self.act_params = props['ACT_PARAMS'] if props['ACT_PARAMS'] else ''

        #load fields
        self.fields = {}        
        mdata = _exec_sql('select * from mfw_web_uif where ver = ? and ui = ?', [self.ver, self.ui]).fetchallmap()
        for row in mdata:            
            field = UiField(row['FIELD'], row)
            self.fields[field.name] = field
     
        #load childs
        self.childs = []        
        mdata = _exec_sql('select ui from mfw_web_ui where ver = ? and ui starting ?', [self.ver, self.ui + '.']).fetchallmap()
        for row in mdata:            
            self.childs.append(row['UI'])
            
    def getField(self, name):
        f = self.fields.get(name)
        if not f:
            f = UiField(name, None)
            self.fields[name] = f
        return f    

class UiField():
    def __init__(self, name, metadata):        
        self.name = name
        self.title = name
        self.band = ''
        self.visible = True        
        self.hide_empty = False        
        self.fmt = u'{}'
        self.editor = 0
        self.css = ''
        self.text_style = 0
        self.text_size = 0
        self.color = 0
        self.alignh = 0
        self.act = ''
        self.act_params = ''

        if metadata:
            self.name = name
            self.title = metadata['TITLE']
            self.band = metadata['BAND'] if metadata['BAND'] else ''
            self.visible = metadata['VISIBLE'] > 0            
            self.hide_empty = metadata['VISIBLE'] == 2            
            self.fmt = metadata['FORMAT'].replace('%s', '{}') if metadata['FORMAT'] else u'{}'
            self.editor = metadata['EDITOR']
            self.css = metadata['CSS'] if metadata['CSS'] else ''
            self.text_size = metadata['TEXTSIZE']                        
            self.text_style = metadata['TEXTSTYLE']            
            self.color = metadata['COLOR']            
            self.alignh = metadata['ALIGNH']
            self.act = metadata['ACT'] if metadata['ACT'] else ''
            self.act_params = metadata['ACT_PARAMS'] if metadata['ACT_PARAMS'] else ''

def parseOptionsText(txt):
    result = {}
    if txt:
        nodes = txt.split(';')
        if nodes[0]:
            for node in nodes:
                k, v = node.split('=')
                result[k] = v
    return result

   
def checkOption(val):
    return val and val == '1' 


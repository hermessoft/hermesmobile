
from flask import Flask

app = Flask(__name__)
app.config.from_object('config')

 
from app import routes
app.jinja_env.line_statement_prefix = '%'

#if not app.debug:
import logging
from logging.handlers import RotatingFileHandler
file_handler = RotatingFileHandler(app.config['LOG_FILE'], 'a', 1 * 1024 * 1024, 10)
file_handler.setFormatter(logging.Formatter('\n%(asctime)s %(levelname)s\n%(message)s\n[in %(pathname)s:%(lineno)d]'))
app.logger.setLevel(logging.DEBUG)
file_handler.setLevel(logging.DEBUG)
app.logger.addHandler(file_handler)
app.logger.info('startup')
#-*- coding: utf-8 -*-"

def _cell(wi, fname, val, width):
    field = wi.info.getField(fname)
    css = []
    css.append('col-xs-%s' %  width)
    css.append(field.css)

    if field.alignh == 2:
        css.append('text-right')
    elif field.alignh == 3:  
        css.append('text-center')   
            
    css.append('text-size-' + str(field.text_size))

    if field.text_style != 0:
        css.append('text-style-' + str(field.text_style))

    if field.color != 0:
        css.append('text-color-' + str(field.color))
            
    html = '<div class="%s">' % ' '.join(css)

    content = field.fmt.format(val)

    if field.editor == 11:
        html = html + '<span class="badge1">' + content + '</span>'
        #html = html + content
    else:
        html = html + content

    html = html + '</div>'
    return html

def _get_row_lines(wi, row, band_fields=False):
    lines = []
    line = None
    line_band = ''
    for fname in row:
        field = wi.info.getField(fname)          

        if band_fields and (not field.band.startswith('band')):
            continue

        if (not band_fields) and field.band.startswith('band'):
            continue
        
        if not field.visible:
            continue          
        if ((not row[fname]) or (row[fname] == 0)) and field.hide_empty:
            continue

        field_band = field.band
        if (not field_band) or (field_band != line_band):
            line_band = field_band
            if line:
                lines.append(line)
            line = []                    
        line.append(fname)  
    if line:
        lines.append(line)
    return lines

def _row(wi, row):    
    html = ''    
    lines = _get_row_lines(wi, row) 
    for l in lines:
        html = html + '<div class="row">'            
        for fname in l:                
            html = html + _cell(wi, fname, row[fname], int(12 / len(l)))
        html = html + '</div>'
    return html

def _row_band(wi, band, row):
    html = ''
    lines = _get_row_lines(wi, row, True)
    if len(lines) != 0:
        for l in lines:
            html = html + '<div class="row">'            
            for fname in l:                
                html = html + _cell(wi, fname, row[fname], int(12 / len(l)))
            html = html + '</div>'
    else:
        html = html + '<div class="row">'            
        html = html + _cell(wi, wi.info.band, row[wi.info.band], 12)
        html = html + '</div>'
    return html

def _list_item(wi, row):
    if wi.info.act:
        html = '<a href="{}"'.format(wi.url_for(row)) +  ' class="list-group-item">'
    else:
        html = '<div class="list-group-item">'          

    html = html + _row(wi, row) 

    if wi.info.act:
        html = html + '</a>'
    else:
        html = html +  '</div>'          

    return html

def _list_band(wi, band):
    html = '<div class="list-group-item list-group-item-info">' 
    html = html + _row_band(wi, band, wi.bandrows[band][0])
    html = html + '</div>'
    return html    

def _list_body(wi):
    html = '<div class="list-group">'
    if wi.info.band:
        for band in wi.bands:
            '''
            html = html + '<div class="list-group-item list-group-item-info">' 
            html = html + u'<h4>{}</h4>'.format(band)
            html = html + '</div>'
            '''
            html = html + _list_band(wi, band)
            for row in wi.bandrows[band]:
                html = html + _list_item(wi, row) 
    else:
        for row in wi.d:
            html = html + _list_item(wi, row)

    html = html + '<div>'
    return html


def list(wi):  
    html = '<div class="panel panel-default">'

    if 'head' in wi.childs:
        html = html + '<div class="panel-heading">'
        for row in wi.childs['head'].d:
            html = html + _row(wi.childs['head'], row)            
        html = html + '</div>'
        
    html = html + _list_body(wi)

    html = html + '</div>'
    return html
    


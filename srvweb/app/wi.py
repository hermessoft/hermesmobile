#-*- coding: utf-8 -*-"
from flask import request, url_for
import dal
from app import app
import config
import wi_view as view

class WorkItem:
    def __init__(self, ui):
        auth = request.authorization
        if auth:
            self.dev_id = auth.username
        else:
            self.dev_id = 'anonymous'

        self.ui = ui        

        if config.DEBUG:
            app.logger.debug('REQUEST DATA' +  '\ndev:\t' + self.dev_id + '\nurl:\t' + ui + '\nargs:\t' + str(request.args))
        
        self.params = {}      

        login_info = dal.login(self.dev_id)
        self.ver = login_info['VER'] 

        if request.args:
            for arg in request.args:
                self.params[arg] = request.args[arg]

        self.params['db_id'] = login_info['DB_ID']
        self.params['ver'] = login_info['VER']        
        self.params['emp_id'] = login_info['EMP_ID']        


        if config.DEBUG:
            app.logger.debug('WORKITEM PARAMS\n' + str(self.params))
         
        self.info = dal.get_ui_info(self.ui, self.ver)        
        self.tmpl = self.info.cls + '.html' 

        self._load_data()
        self._load_childs() 


    def _load_data(self):
        self.d = dal.get_ui_data(self.ui, self.ver, self.params)        
        self.data = self.d

        self.bands = [] 
        self.bandrows = {}       
        fband = self.info.band     
        if fband:
            aux_k = ''
            aux_a = []
            for row in self.d:
                if row[fband] != aux_k:
                    aux_k = row[fband]
                    aux_a = []
                    self.bands.append(aux_k)
                    self.bandrows[aux_k] = aux_a
                aux_a.append(row)  

    def _load_childs(self):
        self.childs = {}
        for c_ui in self.info.childs:
            c_ui_key = c_ui.split('.')[1]
            self.childs[c_ui_key] = WorkItem(c_ui)

    def _url_params_map(self, map_rule, data = None):
        result = {}
        terms = map_rule.split(';')
        if terms[0]:
            for term in terms:
                p, v = term.split('=')
                p = p.lower()
                if v.startswith('['):
                    result[p] = v.strip('[]')
                elif data and v in data:
                    result[p] = data[v]     
                elif v.lower() in self.params:
                    result[p] = self.params[v.lower()]    
                else:
                    result[p] = None
        return result

    def url_for(self, row=None):        
        if not self.info.act:
            return ''
        arg = self._url_params_map(self.info.act_params, row)
        arg['page'] = self.info.act
        return url_for('show', **arg)    

    def url_for_field(self, field, row=None):        
        if not field.act:
            return ''
        arg = self._url_params_map(field.act_params, row)
        arg['page'] = field.act
        return url_for('show', **arg)    

    def title(self):
        if 'title' in self.childs:    
            for r in self.childs['title'].data:
                for f in r:
                    return r[f]
        else:
            return self.info.title

    def html_list(self):
        return view.list(self)
    
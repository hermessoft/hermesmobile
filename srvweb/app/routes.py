#-*- coding: utf-8 -*-"
from flask import render_template, url_for, g, abort, request, Response
from app import app
from jinja2 import TemplateNotFound
from wi import WorkItem


@app.context_processor
def wi_processor():
    if g.wi:
        return dict(wi=g.wi)

@app.before_request
def before_request():
    pass #g.wi = WorkItem()


@app.route('/')
@app.route('/<page>')
def show(page='/'):
    auth = request.authorization
    if not auth:
        return authenticate()   
    g.wi = WorkItem(page)
    try:
        return render_template(g.wi.tmpl)
    except TemplateNotFound:
        abort(404)

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

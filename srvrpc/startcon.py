#!/usr/bin/env python
#-*- coding: utf-8 -*-"

import sys
import os
from server import XMLRPCServer
from log import Log
import cfg

srv = None

def start():
    global srv 
    srv = XMLRPCServer(addr=(cfg.server['address'], cfg.server['port']))
    Log.info('Start server on %s:%s' % (cfg.server['address'], cfg.server['port']))
    srv.serve_forever()

if __name__ == '__main__':      
    try:
        start()
    except KeyboardInterrupt:
        Log.info('^C received, shutting down server')
        srv.socket.close()

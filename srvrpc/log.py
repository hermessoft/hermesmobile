#!/usr/bin/env python
#-*- coding: utf-8 -*-"

import logging
from logging import handlers
from time import localtime, strftime
from sys import stdout, stderr
import cfg

Log = logging.getLogger()

if cfg.log['debug']:
    Log.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(filename)s[%(lineno)d] %(levelname)s %(message)s')
else:
    Log.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

hdlr = logging.StreamHandler()
hdlr.setFormatter(formatter)
Log.addHandler(hdlr)     

if cfg.log['file']:
    hdlr2 = handlers.RotatingFileHandler(cfg.log['file'], 'a', cfg.log['size'] * 1024, 1)    
    hdlr2.setFormatter(formatter)    
    Log.addHandler(hdlr2)    


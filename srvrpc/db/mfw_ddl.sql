UPDATE MFW_DDL SET 
    VER = 1,
    IDX = 1,
    DDL = '
<item> DROP TABLE IF EXISTS CONF_UI   </item>
<item>
CREATE TABLE CONF_UI (
 ui  text not null primary key,
 cls  text,
 title   text,
 options  text,
 args text,
 eds_act integer,
 eds_sql text
)
</item>

<item> DROP TABLE IF EXISTS CONF_UIA  </item>
<item>
CREATE TABLE CONF_UIA (
 ui text not null,
 act text not null,
 title text,
 menu integer,
 def integer,
 handler text,
 params text,
 condition text,
 idx integer,
 primary key (ui, act)
)
</item>

<item> DROP TABLE IF EXISTS CONF_UIF  </item>
<item>
CREATE TABLE CONF_UIF (
 ui text not null,
 field text not null,
 title text,
 band  text,
 visible integer,
 editor integer,
 format text,
 textsize integer,
 textstyle integer,
 color integer,
 colorbg integer,
 alignh integer,
 alignv integer,
 options text,
 primary key (ui, field)
)
</item>'
WHERE ;

UPDATE MFW_DDL SET 
    VER = 3,
    IDX = 1,
    DDL = '<item> ALTER TABLE CONF_UIA ADD COLUMN options text </item>

<item> DROP TABLE IF EXISTS PREF </item>
<item>
CREATE TABLE IF NOT EXISTS PREF (
 name text not null primary key,
 val text
)
</item>'
WHERE ;


COMMIT WORK;

UPDATE MFW_DDL SET 
    VER = 2,
    IDX = 2,
    DDL = '<item> ALTER TABLE VDOC ADD COLUMN dlv integer not null default 1 </item>'
WHERE ;

UPDATE MFW_DDL SET 
    VER = 1,
    IDX = 2,
    DDL = '
<item> DROP TABLE IF EXISTS ENUM </item>
<item>
CREATE TABLE IF NOT EXISTS ENUM (
 enum text,
 _id integer,
 name text,
 primary key (enum, _id)
)
</item>

<item> DROP TABLE IF EXISTS OLA </item>
<item>
CREATE TABLE IF NOT EXISTS OLA (
 _id integer not null primary key,
 name text,
 name_upper text,
 addr text,
 phone text,
 pltyp_id integer,
 debt float,
 debtd integer,
 debt1 float,
 debtd1 integer,
 debt2 float,
 debtd2 integer,
 status integer default 1
)
</item>

<item> DROP TABLE IF EXISTS VST </item>
<item>
CREATE TABLE IF NOT EXISTS VST (
 _id integer not null primary key autoincrement,
 dat date default (current_date),
 tms time default (time(''now'',''localtime'')),
 ola_id integer not null,
 status integer default 0
)
</item>

<item> CREATE INDEX IF NOT EXISTS vst_idx_ola ON VST(ola_id); </item>

<item> DROP TABLE IF EXISTS VACT </item>
<item>
CREATE TABLE IF NOT EXISTS VACT (
 _id integer not null primary key,
 name text not null
)
</item>

<item> DROP TABLE IF EXISTS VDOC </item>
<item>
CREATE TABLE IF NOT EXISTS VDOC (
 _id integer not null primary key autoincrement,
 dat date default (current_date),
 tms time default (time(''now'',''localtime'')),
 vst_id integer not null references vst(_id) on delete cascade,
 vact_id integer not null,
 af1 integer default 0,
 status integer default 0,
 rem text
)
</item>

<item> CREATE INDEX IF NOT EXISTS vdoc_idx_vst ON VDOC(vst_id); </item>

<item> DROP TABLE IF EXISTS VDOCI </item>
<item>
CREATE TABLE IF NOT EXISTS VDOCI (
 _id integer not null primary key autoincrement,
 doc_id integer not null references vdoc(_id) on delete cascade,
 sku_id integer not null,
 qty float,
 price float
)
</item>

<item> CREATE INDEX IF NOT EXISTS vdoci_idx_doc ON VDOCI(doc_id); </item>
<item> CREATE INDEX IF NOT EXISTS vdoci_idx_sku ON VDOCI(sku_id); </item>

<item> DROP TABLE IF EXISTS MG </item>
<item>
CREATE TABLE IF NOT EXISTS MG (
 _id integer not null primary key,
 name text,
 idx integer,
 status integer default 1
)
</item>

<item> DROP TABLE IF EXISTS SKU </item>
<item>
CREATE TABLE IF NOT EXISTS SKU (
 _id integer not null primary key,
 name text,
 name_upper text,
 idx integer,
 mg_id integer,
 grp_id integer,
 stock float,
 status integer default 1
)
</item>

<item> DROP TABLE IF EXISTS PL </item>
<item>
CREATE TABLE IF NOT EXISTS PL (
 sku_id integer,
 pltyp_id integer,
 price float,
 sync_id integer,
 primary key (sku_id, pltyp_id)
)
</item>

<item> DROP TABLE IF EXISTS PLX </item>
<item>
CREATE TABLE IF NOT EXISTS PLX (
 ola_id integer,
 skug_id integer,
 pltyp_id integer,
 primary key (ola_id, skug_id)
)
</item>

<item> DROP TABLE IF EXISTS OLA_SKU </item>
<item>
CREATE TABLE IF NOT EXISTS OLA_SKU (
 ola_id integer,
 sku_id integer,
 f1 integer,
 f2 integer,
 ldat date,
 lqty float,
 sync_id integer, 
 primary key (ola_id, sku_id)
)
</item>'
WHERE ;

UPDATE MFW_DDL SET 
    VER = 3,
    IDX = 2,
    DDL = '<item> DROP TABLE IF EXISTS SKUG </item>
<item>
CREATE TABLE IF NOT EXISTS SKUG (
 _id integer not null primary key,
 name text,
 mg_id integer,
 idx integer,
 status integer default 1
)
</item>

<item> ALTER TABLE SKU ADD COLUMN unt text </item>
<item> ALTER TABLE SKU ADD COLUMN unt2 text </item>
<item> ALTER TABLE SKU ADD COLUMN unt2x integer </item>
<item> ALTER TABLE VDOCI ADD COLUMN unt text </item>
<item> ALTER TABLE VDOCI ADD COLUMN qtyx_unt integer </item>
<item> ALTER TABLE VDOCI ADD COLUMN qtyx integer </item>

<item> DROP TRIGGER IF EXISTS VDOCI_TR_IA </item>
<item>
CREATE TRIGGER IF NOT EXISTS VDOCI_TR_IA
       AFTER INSERT ON VDOCI
       FOR EACH ROW
BEGIN  
   UPDATE VDOCI
   set qty = (select case
                       when new.qtyx_unt = 2 then new.qtyx * sku.unt2x
                       else new.qtyx
                     end
              from sku where sku._id = new.sku_id),
       unt = (select case
                       when new.qtyx_unt = 2 then sku.unt2
                       else sku.unt
                     end
              from sku where sku._id = new.sku_id)

   WHERE _id = new._id;
END;
</item>

<item> DROP TRIGGER IF EXISTS VDOCI_TR_IU </item>
<item>
CREATE TRIGGER IF NOT EXISTS VDOCI_TR_IU
       AFTER UPDATE OF qtyx, qtyx_unt ON VDOCI
       FOR EACH ROW
BEGIN  
   UPDATE VDOCI
   set qty = (select case
                       when new.qtyx_unt = 2 then new.qtyx * sku.unt2x
                       else new.qtyx
                     end
              from sku where sku._id = new.sku_id),
       unt = (select case
                       when new.qtyx_unt = 2 then sku.unt2
                       else sku.unt
                     end
              from sku where sku._id = new.sku_id)

   WHERE _id = new._id;
END;
</item>'
WHERE ;


COMMIT WORK;

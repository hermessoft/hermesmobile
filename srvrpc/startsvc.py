# -*- coding: utf-8 -*-
#! /usr/bin/env python
import win32serviceutil
import win32service
import win32event
import servicemanager
import threading

from server import XMLRPCServer
from log import Log
import cfg

srv = None
server_thread = None

def start():
    global srv 
    global server_thread
    srv = XMLRPCServer(addr=(cfg.server['address'], cfg.server['port']))
    Log.info('Start server on %s:%s' % (cfg.server['address'], cfg.server['port']))
    server_thread = threading.Thread(target=srv.serve_forever)
    server_thread.start()

def stop():
    #server_process.termitane()
    srv.socket.close()

class AppServerSvc (win32serviceutil.ServiceFramework):
    _svc_name_ = cfg.svc['name']
    _svc_display_name_ = cfg.svc['display']
    _svc_description_ = "xml-rpc service"
     
    def __init__(self,args):
        win32serviceutil.ServiceFramework.__init__(self,args)
        self.hWaitStop = win32event.CreateEvent(None,0,0,None)
        self.hWaitResume = win32event.CreateEvent(None, 0, 0, None)
        self.timeout = 10000 #Пауза между выполнением основного цикла службы в миллисекундах
        self.resumeTimeout = 1000
        self._paused = False
       
    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                                servicemanager.PYS_SERVICE_STOPPED,
                                (self._svc_name_, ''))
           
    def SvcPause(self):
        self.ReportServiceStatus(win32service.SERVICE_PAUSE_PENDING)
        self._paused = True
        self.ReportServiceStatus(win32service.SERVICE_PAUSED)
        servicemanager.LogInfoMsg("The %s service has paused." % (self._svc_name_, ))
       
    def SvcContinue(self):
        self.ReportServiceStatus(win32service.SERVICE_CONTINUE_PENDING)
        win32event.SetEvent(self.hWaitResume)
        self.ReportServiceStatus(win32service.SERVICE_RUNNING)
        servicemanager.LogInfoMsg("The %s service has resumed." % (self._svc_name_, ))
                   
     
    def SvcDoRun(self):
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                                servicemanager.PYS_SERVICE_STARTED,
                                (self._svc_name_,''))
        self.main()  
       
    #В этом методе реализовываем нашу службу    
    def main(self):
        #Здесь выполняем необходимые действия при старте службы
        #servicemanager.LogInfoMsg("Hello! I'm a Dummy Service.")
        start()
        while True:
            #Здесь должен находиться основной код сервиса
            #servicemanager.LogInfoMsg("I'm still here.")
               
            #Проверяем не поступила ли команда завершения работы службы
            rc = win32event.WaitForSingleObject(self.hWaitStop, self.timeout)
            if rc == win32event.WAIT_OBJECT_0:
                #Здесь выполняем необходимые действия при остановке службы                
                servicemanager.LogInfoMsg("Bye!")
                stop()                
                break
     
            #Здесь выполняем необходимые действия при приостановке службы
            if self._paused:
                servicemanager.LogInfoMsg("I'm paused... Keep waiting...")
                stop()
            #Приостановка работы службы                
            while self._paused:
                #Проверям не поступила ли команда возобновления работы службы
                rc = win32event.WaitForSingleObject(self.hWaitResume, self.resumeTimeout)
                if rc == win32event.WAIT_OBJECT_0:
                    self._paused = False
                    #Здесь выполняем необходимые действия при возобновлении работы службы
                    servicemanager.LogInfoMsg("Yeah! Let's continue!")
                    start()
                    break                  
     
if __name__ == '__main__':
    win32serviceutil.HandleCommandLine(AppServerSvc)

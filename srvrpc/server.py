#!/usr/bin/env python
#-*- coding: utf-8 -*-"

from SimpleXMLRPCServer import SimpleXMLRPCDispatcher, SimpleXMLRPCRequestHandler
from SocketServer import ThreadingTCPServer
from base64 import b64decode
from log import Log
import cfg
import rpc #Load RPC methods

try:
    import fcntl
except ImportError:
    fcntl = None


class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = (cfg.server['path'],)

    def __init__(self, request, client_address, server):
        Log.info('-' * 20)
        Log.info('CONNECT: %s' % client_address[0])        
        SimpleXMLRPCRequestHandler.__init__(self, request, client_address, server)

    def parse_request(self):
        if SimpleXMLRPCRequestHandler.parse_request(self):
            result = True
            if cfg.server['auth']:
                result = self._authenticate()
            if not result: 
                self.send_error(401, 'Authentication failed')
            return result

    def _authenticate(self):
        result = False
        auth_head = self.headers.get('Authorization', '')
        if auth_head:
            (auth_metod, encode) = auth_head.split()
            if auth_metod == 'Basic':
                (user, passwd) = b64decode(encode).split(':')
                if (user in cfg.users) and (cfg.users[user] == passwd):
                    Log.info('USER: %s' % user)
                    result = True
                else:
                    Log.warning('Auth fault: user %s passwd %s' % (user, passwd))
                    #Log.info('Client address: %s' % SimpleXMLRPCRequestHandler.address_string(self))
        return result

class XMLRPCInstance:
    def _dispatch(self, method, params):
        (method_pref, spam, method_name)  = method.partition('.')
        if not method_name:
            method_name = method_pref
        if method_pref in cfg.MethodCatalog:
            Log.info('CALL: %s' % method)
            Log.debug('PARAMS: %s' % params)
            return cfg.MethodCatalog[method_pref](method_name, params)
        else:
            Log.error('Method %s is not supported' % method)
            raise Exception('Method %s is not supported' % method)

class XMLRPCServer(ThreadingTCPServer, SimpleXMLRPCDispatcher):
    allow_reuse_address = True
    _send_traceback_header = False

    def __init__(self, addr, requestHandler=RequestHandler, logRequests=False, allow_none=True, encoding=None, bind_and_activate=True):
        self.logRequests = logRequests

        SimpleXMLRPCDispatcher.__init__(self, allow_none, encoding)
        ThreadingTCPServer.__init__(self, addr, requestHandler, bind_and_activate)
        self.register_instance(XMLRPCInstance())

        if fcntl is not None and hasattr(fcntl, 'FD_CLOEXEC'):
            flags = fcntl.fcntl(self.fileno(), fcntl.F_GETFD)
            flags |= fcntl.FD_CLOEXEC
            fcntl.fcntl(self.fileno(), fcntl.F_SETFD, flags)

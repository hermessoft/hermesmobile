#!/usr/bin/env python
#-*- coding: utf-8 -*-"

from log import Log
import cfg
from xmlrpclib import Binary

def ApkLoad(method, params):
	prm = params[0]
	Log.debug('Apk load begin')
	device_id = prm["dev_id"]
	apk_version = prm["app_ver"]
	apk_file = cfg.work_dir + cfg.apk['dir'] +'/' + str(apk_version) + '/' + cfg.apk['file']
	Log.debug('SEND APK:\t file: %s \t device: %s' % (apk_file, device_id))
	try:
		with open(apk_file, "rb") as handle:
			return Binary(handle.read())
	except Exception as e:
		Log.error('SEND APK ERROR:\t%s' % e)
		raise

cfg.RegisterMethod(ApkLoad, 'ApkLoad')

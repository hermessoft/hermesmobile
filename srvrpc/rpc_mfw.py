#!/usr/bin/env python
#-*- coding: utf-8 -*-"

import traceback
import threading
import time
import cfg 
from log import Log
import rpc_mfw_utl
#import fdb as fb
import kinterbasdb as fb
fb.init(type_conv=200)


con = None
utl_query = rpc_mfw_utl.SQLParams('named', 'qmark')

sql_cache = {}

_writeTPB = (
    fb.isc_tpb_write + #@UndefinedVariable
    fb.isc_tpb_consistency + #@UndefinedVariable
    fb.isc_tpb_nowait #@UndefinedVariable
)
_readTPB = (
    fb.isc_tpb_read + #@UndefinedVariable
    fb.isc_tpb_read_committed + #@UndefinedVariable
    fb.isc_tpb_rec_version +#@UndefinedVariable
    fb.isc_tpb_nowait #@UndefinedVariable
)

lcTimeOut = threading.Lock()
thTimeOut = threading.Thread()

def chTimeOut():
    global work_time, con
    while 1:
        lcTimeOut.acquire()        
        breakFlag = (time.time() - work_time) > (cfg.dal['timeout'] * 60) and con and not con.closed
        if breakFlag:
            con.close()
            Log.debug("DISCONNECT FROM DB")
        lcTimeOut.release()
        if breakFlag: break
        time.sleep(5)

def connect():
    global con, work_time, thTimeOut
    work_time = time.time();
    if not con or con.closed:
        try:
            lcTimeOut.acquire()
            con = fb.connect(dsn=cfg.dal['dsn'], user=cfg.dal['user'], password=cfg.dal['passwd'], charset=cfg.dal['charset'])
            lcTimeOut.release()
            if not thTimeOut.is_alive():
                thTimeOut = threading.Thread(target=chTimeOut, name="chTimeOut")
                thTimeOut.start()
        except Exception as e:
            if type(e) == fb.OperationalError:
                Log.error("Connect DataBase:\t%s" % e[1], True)
            else:
                Log.error("Connect:\t%s" % e, True)
            raise

def execSQL(sql, params):    
    Log.debug('EXEC SQL:')
    Log.debug(sql)  
    Log.debug('%s' % params)
    connect()
    result = con.cursor()
    try:
        if params:
            result.execute(sql, params)
        else:
            result.execute(sql)
        con.commit(True)        
        Log.debug('EXEC SQL finish.')
        return result
    except Exception as e:
        if type(e) == fb.ProgrammingError:
            Log.error("EXEC SQL ERROR:\t%s" % e[1])
        else:
            Log.error("EXEC SQL ERROR:\t%s" % e)
        raise

def cursor2ArrayMap(cursor):
    arr = []
    for row in cursor:
        i = 0
        map = {}
        for value in row:
            if value :
                if type(value) == unicode:
                    map[cursor.description[i][0]] = value.strip()
                else:
                    map[cursor.description[i][0]] = value
            else:
                map[cursor.description[i][0]] = value

            #обработка null
            if value is None: 
                map[cursor.description[i][0]] = ''

            i += 1                    
        arr.append(map)
    return arr


def getSqlText(method):
    
    if not method in sql_cache:
        cur = execSQL(cfg.dal['rpc_sql'], [method])              
        data = cur.fetchone()        
        if data and data[0]:
            sql_text = data[0]            
            sql_text = sql_text.lstrip()
            if cfg.dal['sql_cache']:
                sql_cache[method] = sql_text
    else:
        sql_text = sql_cache[method]

    return sql_text

def Execute(method, params):     
        
    
    sql_text = getSqlText(method)
    if not sql_text:
        raise Exception('No SQL for: %s' % method)

    do_fetch = sql_text.lower().startswith('select') 

    if params:
        if type(params[0]) is list:            
            for args in params[0]:
                sql_text_parsed, sql_params = utl_query.format(sql_text, args)          
                execSQL(sql_text_parsed, sql_params)            
        else:
            sql_text, sql_params = utl_query.format(sql_text, params[0])            
            cur = execSQL(sql_text, sql_params)            
    else:        
        cur = execSQL(sql_text, None)    

    if do_fetch and cur:
        result = cursor2ArrayMap(cur) 

    if do_fetch:
        return result
    else:    
        return 0

cfg.RegisterMethod(Execute, 'MFW')

